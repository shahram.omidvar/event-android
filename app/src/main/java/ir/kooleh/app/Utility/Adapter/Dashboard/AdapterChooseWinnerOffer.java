package ir.kooleh.app.Utility.Adapter.Dashboard;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Dashboard.InterfaceDashboard;
import ir.kooleh.app.Structure.Model.Dashboard.ModelOffersMyAd;
import ir.kooleh.app.Utility.Class.NumberUtils;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterChooseWinnerOffer extends RecyclerView.Adapter<AdapterChooseWinnerOffer.HolderChooseWinnerOffer>
{
    private Context context;
    private List<ModelOffersMyAd.ListOffer> offersMyAdList;

    private int lastCheckedPos = -1;
    private LinearLayout lastCheckedContainer;
    private ImageView lastCheckedImg;
    private String winnerID = "";

    private InterfaceDashboard.InterClassData.IntSendWinnerDataToActivity intSendWinnerDataToActivity;

    public AdapterChooseWinnerOffer(Context context)
    {
        this.context = context;
        intSendWinnerDataToActivity = (InterfaceDashboard.InterClassData.IntSendWinnerDataToActivity) context;
    }

    @NonNull
    @Override
    public HolderChooseWinnerOffer onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new HolderChooseWinnerOffer(
                LayoutInflater.from(context).inflate(R.layout.item_choose_winner_offer, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HolderChooseWinnerOffer holder, @SuppressLint("RecyclerView") int position)
    {
        ModelOffersMyAd.ListOffer modelOffersMyAd = offersMyAdList.get(position);
        Glide.with(context).load(modelOffersMyAd.getImage()).placeholder(R.drawable.ic_avatar).into(holder.imgProfile);
        holder.tvFullname.setText(modelOffersMyAd.getName());
        holder.tvPrice.setText(NumberUtils.digitDivider(String.valueOf(modelOffersMyAd.getPrice())) + " ریال");
//        holder.tvExplain.setText(modelOffersMyAd.getExplain());
        holder.tvDate.setText(String.format("%s", modelOffersMyAd.getDate()));

        if (!winnerID.equals(""))
        {
            holder.itemView.setEnabled(false);
        }

        if (winnerID.equals(modelOffersMyAd.getUserId()))
        {
            holder.containerItem.setBackgroundResource(R.color.blue_100);
            holder.imgChecked.setVisibility(View.VISIBLE);
        }

        holder.itemView.setOnClickListener(v ->
        {
            if (lastCheckedPos == position)
            {
                holder.containerItem.setBackgroundResource(R.color.transparent);
                holder.imgChecked.setVisibility(View.GONE);
                lastCheckedPos = -1;
                intSendWinnerDataToActivity.onResponseSendWinnerData("", "",
                        "یکی از پیشنهاد ها را انتخاب کنید", "");
            }
            else
            {
                if (lastCheckedPos != -1)
                {
                    lastCheckedContainer.setBackgroundResource(R.color.transparent);
                    lastCheckedImg.setVisibility(View.GONE);
                }
                holder.containerItem.setBackgroundResource(R.color.blue_100);
                holder.imgChecked.setVisibility(View.VISIBLE);
                lastCheckedContainer = holder.containerItem;
                lastCheckedImg = holder.imgChecked;
                lastCheckedPos = position;
                intSendWinnerDataToActivity.onResponseSendWinnerData(
                        modelOffersMyAd.getUserId(), modelOffersMyAd.getAuctionId(),
                        modelOffersMyAd.getName(), String.valueOf(modelOffersMyAd.getPrice()));
            }
        });

    }

    @Override
    public int getItemCount()
    {
        return offersMyAdList == null ? 0 : offersMyAdList.size();
    }


    public void setList(List<ModelOffersMyAd.ListOffer> offersMyAdList)
    {
        this.offersMyAdList = offersMyAdList;
        notifyDataSetChanged();
    }

    public void setList(List<ModelOffersMyAd.ListOffer> offersMyAdList, String winnerID)
    {
        this.offersMyAdList = offersMyAdList;
        this.winnerID = winnerID;
        notifyDataSetChanged();
    }

    public static class HolderChooseWinnerOffer extends RecyclerView.ViewHolder
    {
        private LinearLayout containerItem;
        private ImageView imgProfile;
        private CTextView tvFullname, tvPrice, tvDate;
        private ImageView imgChecked;

        public HolderChooseWinnerOffer(@NonNull View itemView)
        {
            super(itemView);
            containerItem = itemView.findViewById(R.id.container_item_choose_winner_offer);
            imgProfile = itemView.findViewById(R.id.img_user_choose_winner_offer);
            tvFullname = itemView.findViewById(R.id.tv_fullname_choose_winner_offer);
            tvPrice = itemView.findViewById(R.id.tv_price_choose_winner_offer);
            tvDate = itemView.findViewById(R.id.tv_date_choose_winner_offer);
            imgChecked = itemView.findViewById(R.id.img_check_choose_winner_offer);
        }
    }

}