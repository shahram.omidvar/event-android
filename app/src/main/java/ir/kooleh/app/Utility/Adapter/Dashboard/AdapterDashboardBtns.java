package ir.kooleh.app.Utility.Adapter.Dashboard;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Dashboard.ModelDashboard;
import ir.kooleh.app.Utility.Class.DashboardUtils;
import ir.kooleh.app.Utility.Class.UtilStrings;
import ir.kooleh.app.View.Ui.Activity.Profile.ProfileActivity;
import ir.kooleh.app.View.Ui.Activity.dashboard.MyAdsActivity;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterDashboardBtns extends RecyclerView.Adapter<AdapterDashboardBtns.HolderDashboardButtons>
{
    private final Activity activity;
    private List<ModelDashboard.Data.DashboardBtns> dashboardBtnsList;

    public AdapterDashboardBtns(Activity activity, List<ModelDashboard.Data.DashboardBtns> dashboardBtnsList)
    {
        this.activity = activity;
        this.dashboardBtnsList = dashboardBtnsList;
    }


    @NonNull
    @Override
    public AdapterDashboardBtns.HolderDashboardButtons onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new AdapterDashboardBtns.HolderDashboardButtons(
                LayoutInflater.from(activity).inflate(R.layout.item_dashboard_button,
                        parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull AdapterDashboardBtns.HolderDashboardButtons holder, int position)
    {
        holder.imgIcon.setImageResource(DashboardUtils.getButtonIconByAction(dashboardBtnsList.get(position).getAction()));
        holder.tvTitle.setText(UtilStrings.splitButtonNameFromNumber(dashboardBtnsList.get(position).getTitle()));
        holder.tvUnseenNumbers.setText(UtilStrings.splitNumberFromButtonName(dashboardBtnsList.get(position).getTitle()));


        holder.itemView.setOnClickListener(v ->
        {
            Intent intent = new Intent();
            switch (dashboardBtnsList.get(position).getAction())
            {
                case "profile":
                    intent.setClass(activity, ProfileActivity.class);
                    break;
                case "my_ads":
                    intent.setClass(activity, MyAdsActivity.class);
                    intent.putExtra("ad_type", "my_ads");
                    break;
                case "my_auctions":
                    intent.setClass(activity, MyAdsActivity.class);
                    intent.putExtra("ad_type", "my_auctions");
                    break;
                case "my_offers":
                    intent.setClass(activity, MyAdsActivity.class);
                    intent.putExtra("ad_type", "my_offers");
                    break;
            }
            activity.startActivityForResult(intent, 1);
        });

    }

    @Override
    public int getItemCount()
    {
        return dashboardBtnsList == null ? 0 : dashboardBtnsList.size();
    }


    public void setList(List<ModelDashboard.Data.DashboardBtns> dashboardBtnsList)
    {
        this.dashboardBtnsList = dashboardBtnsList;
        notifyDataSetChanged();
    }

    public static class HolderDashboardButtons extends RecyclerView.ViewHolder
    {
        private final ImageView imgIcon;
        private final CTextView tvTitle;
        private final CTextView tvUnseenNumbers;

        public HolderDashboardButtons(@NonNull View itemView)
        {
            super(itemView);
            imgIcon = itemView.findViewById(R.id.img_icon_dashboard_button);
            tvTitle = itemView.findViewById(R.id.tv_title_dashboard_button);
            tvUnseenNumbers = itemView.findViewById(R.id.tv_unseen_item_numbers);
        }
    }
}
