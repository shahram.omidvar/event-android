package ir.kooleh.app.Utility.Adapter.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Service.ModelIndexService;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.Utility.Class.OpenActivityOrFragmentFromServer;
import ir.kooleh.app.View.Ui.Activity.general.WebViewActivity;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterRecyclerViewFragmentServiceHorizontal extends RecyclerView.Adapter<AdapterRecyclerViewFragmentServiceHorizontal.ServiceHorizontalViewHolder>
{

    private final Activity activity;
    private List<ModelIndexService.ServiceHorizontal> serviceHorizontalList;
    private CustomToast customToast;

    public AdapterRecyclerViewFragmentServiceHorizontal(Activity activity)
    {
        this.activity = activity;
        customToast = new CustomToast(activity);
    }

    @NonNull
    @Override
    public ServiceHorizontalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ServiceHorizontalViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_row_list_service_horizontal, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceHorizontalViewHolder holder, int position)
    {

        ModelIndexService.ServiceHorizontal serviceHorizontal = serviceHorizontalList.get(position);

        if (serviceHorizontal.getImage() != null && serviceHorizontal.getImage().length() > 0)
            Glide.with(activity).load(serviceHorizontal.getImage())
                    .placeholder(R.drawable.img_name_logo_kooleh)
                    .dontAnimate()
                    .into(holder.ivService);


        if (serviceHorizontal.getName() == null) holder.tvService.setVisibility(View.GONE);
        else holder.tvService.setText(serviceHorizontal.getName());

        holder.itemView.setOnClickListener(v ->
        {
            if (serviceHorizontal.getAction().equals("web"))
            {
                Intent intent = new Intent(activity, WebViewActivity.class);
                intent.putExtra("value", serviceHorizontal.getValue());
                activity.startActivity(intent);

            }
            else if (serviceHorizontal.getAction().equals("browser"))
            {
                String uri = Uri.parse(serviceHorizontal.getValue())
                        .buildUpon()
                        .build().toString();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                activity.startActivity(browserIntent);
            }

            else if (serviceHorizontal.getAction().equals("activity"))
            {
                OpenActivityOrFragmentFromServer.openActivity(
                        activity,
                        serviceHorizontal.getValue());
            }

            else if (serviceHorizontal.getAction().equals("fragment"))
            {
                OpenActivityOrFragmentFromServer.openFragment(
                        activity,
                        serviceHorizontal.getValue(),
                        R.id.fragment_home);
            }
            else if (serviceHorizontal.getAction().equals("toast"))
            {
                customToast = new CustomToast(activity.getApplicationContext());
                customToast.showInfo(serviceHorizontal.getValue());
            }

        });

    }

    @Override
    public int getItemCount()
    {
        return serviceHorizontalList == null ? 0 : serviceHorizontalList.size();
    }

    static class ServiceHorizontalViewHolder extends RecyclerView.ViewHolder
    {
        private final ImageView ivService;
        private final CTextView tvService;

        public ServiceHorizontalViewHolder(@NonNull View itemView)
        {
            super(itemView);
            ivService = itemView.findViewById(R.id.iv_icon_smart_card_service_horizontal);
            tvService = itemView.findViewById(R.id.tv_title_service_horizontal);
            tvService.setSelected(true);
        }
    }

    public void addListService(List<ModelIndexService.ServiceHorizontal> serviceHorizontal)
    {
        this.serviceHorizontalList = serviceHorizontal;
        notifyDataSetChanged();
    }

}
