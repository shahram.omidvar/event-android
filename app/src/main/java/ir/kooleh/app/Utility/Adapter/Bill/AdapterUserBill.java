package ir.kooleh.app.Utility.Adapter.Bill;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Bill.InterfaceUserBill;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceTryAgainPaginate;
import ir.kooleh.app.Structure.Model.Bills.ModelUserBill;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterUserBill extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private final Activity activity;
    private List<ModelUserBill> userBillList = new ArrayList<>();
    private final String color;
    private final String icon;
    private final String type;
    private InterfaceUserBill.BillInquiry.IntClickBillItem intClickBillItem;

    private InterfaceTryAgainPaginate interfaceTryAgainPaginate;
    private boolean showbtnTryAgain = false;

    public AdapterUserBill(Activity activity, String type, String color, String icon)
    {
        this.activity = activity;
        this.type = type;
        this.color = color;
        this.icon = icon;
        intClickBillItem = (InterfaceUserBill.BillInquiry.IntClickBillItem) activity;
        if (activity instanceof InterfaceTryAgainPaginate)
        {
            this.interfaceTryAgainPaginate = (InterfaceTryAgainPaginate) activity;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        if (viewType == 2)
            return new HolderItemLastPaginate(LayoutInflater.from(activity).inflate(R.layout.item_last_paginate, parent, false));
        return new HolderUserBill(LayoutInflater.from(activity).inflate(R.layout.item_user_bill, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if (holder instanceof HolderUserBill)
        {
            HolderUserBill holderUserBill = (HolderUserBill) holder;
            ModelUserBill modelUserBill = userBillList.get(position);

            holderUserBill.tvSlug.setText(modelUserBill.getSlug());
            holderUserBill.tvValue.setText(modelUserBill.getValue());
            if (modelUserBill.getReceiptImage() != null)
            {
                Glide.with(activity).load(modelUserBill.getReceiptImage()).into(holderUserBill.ivReceiptImage);
            }

            holder.itemView.setOnClickListener(v -> {
                intClickBillItem.onResponseIntClickBillItem(modelUserBill.getValue());
            });
        }
        else
        {
            HolderItemLastPaginate holderUserPackagesBottom = (HolderItemLastPaginate) holder;
            if (showbtnTryAgain)
            {
                showbtnTryAgain = false;
                holderUserPackagesBottom.itemView.setVisibility(View.VISIBLE);
                holderUserPackagesBottom.itemView.setOnClickListener(v ->
                {
                    holderUserPackagesBottom.itemView.setVisibility(View.GONE);
                    interfaceTryAgainPaginate.onResponseTryAgainPaginate();
                });
            }
        }

    }

    @Override
    public int getItemCount()
    {
        return userBillList == null ? 0 : userBillList.size() + 1;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (position == this.getItemCount() - 1)
        {
            return 2;
        }
        return 1;
    }

    public void setList(List<ModelUserBill> userBillList)
    {
        this.userBillList = userBillList;
        notifyDataSetChanged();
    }

    public void addList(List<ModelUserBill> userBillList)
    {
        this.userBillList.addAll(userBillList);
        notifyDataSetChanged();
    }

    public void showBtnTryAgain()
    {
        this.showbtnTryAgain = true;
        notifyDataSetChanged();
    }



    public class HolderUserBill extends RecyclerView.ViewHolder
    {
        private final CTextView tvSlug;
        private final ImageView ivReceiptImage;
        private final CTextView tvValue;
        public HolderUserBill(@NonNull View itemView)
        {
            super(itemView);
            tvSlug = itemView.findViewById(R.id.tv_slug_item_user_bill);
            ivReceiptImage = itemView.findViewById(R.id.iv_receipt_image_item_user_bill);
            tvValue = itemView.findViewById(R.id.tv_value_item_user_bill);
        }
    }


    public class HolderItemLastPaginate extends RecyclerView.ViewHolder
    {
        public HolderItemLastPaginate(@NonNull View itemView)
        {
            super(itemView);
            itemView.setVisibility(View.GONE);
        }
    }

}
