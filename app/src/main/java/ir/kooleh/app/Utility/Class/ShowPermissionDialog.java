package ir.kooleh.app.Utility.Class;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import ir.kooleh.app.Structure.Enum.LanguageApp;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceGetPermissions;
import ir.kooleh.app.Structure.Enum.ListPermission;


public class ShowPermissionDialog extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback
{

    private ListPermission PERMISSION;
    private Activity activity;
    private InterfaceGetPermissions interfaceGetPermissions;
    public final int REQUEST_CODE_CAMERA = 10000;
    public final int REQUEST_CODE_MEDIA_ACCESS_CAMERA = 10001;
//    public final int REQUEST_CODE_MEDIA_ACCESS_GALLERY = 10002;
    public final int REQUEST_CODE_INTERNET = 10003;
    public final int REQUEST_CODE_RECORD_AUDIO = 10004;
    public final int REQUEST_CODE_SMS = 10005;
    public final int REQUEST_CODE_WRITE_STORAGE_CAMERA = 10006;
    public final int REQUEST_CODE_WRITE_STORAGE_GALLERY = 10007;
    public final int REQUEST_CODE_READ_CONTACTS = 10007;
    private String permissionMode = "";

    public ShowPermissionDialog() { }

    public ShowPermissionDialog(Activity activity, InterfaceGetPermissions interfaceGetPermissions)
    {
        this.interfaceGetPermissions = interfaceGetPermissions;
        this.activity = activity;
    }


    // درخواست یک مجوز از کاربر
    public void getPermissionFromUser(ListPermission selectPermission)
    {

        String permission = getNamePermission(selectPermission, LanguageApp.English);
        if (permission.equals("-1"))
        {
            interfaceGetPermissions.onResultRequestPermissions(false, "این مجوز پیدا نشد لطفا در انتخاب مجوز دقت کنید", -1);
            return;
        }

        PERMISSION = selectPermission;
        //در صورتی که تا حالا مجوز را نگرفتیم
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED)
        {

            // در صورتی که دفعه ای اول با مجوز موافقت نشد
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission))
                CreateDialog(selectPermission);

            else
                reqPermission(selectPermission);
        }
        else
        {
//            interfaceGetPermissions.onResultRequestPermissions(false,
//                    "مجوز از قبل دریافت شد است", 0);
        }
    }

    public void getPermissionFromUser(ListPermission selectPermission, String permissionMode)
    {
        this.permissionMode = permissionMode;
        String permission = getNamePermission(selectPermission, LanguageApp.English);
        if (permission.equals("-1"))
        {
            interfaceGetPermissions.onResultRequestPermissions(false, "این مجوز پیدا نشد لطفا در انتخاب مجوز دقت کنید", -1);
            return;
        }

        PERMISSION = selectPermission;
        //در صورتی که تا حالا مجوز را نگرفتیم
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED)
        {

            // در صورتی که دفعه ای اول با مجوز موافقت نشد
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission))
                CreateDialog(selectPermission);

            else
                reqPermission(selectPermission);
        }
        else
        {
            interfaceGetPermissions.onResultRequestPermissions(false,
                    "مجوز از قبل دریافت شد است", 0);
        }
    }


    private void CreateDialog(final ListPermission listPermission)
    {
        String namePersianPermission = getNamePermission(listPermission, LanguageApp.Persian);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("درخواست مجوز");
        builder.setMessage("برای  " + namePersianPermission + " ابتدا باید مجوز را تایید کنید");
        builder.setPositiveButton("موافقم", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                reqPermission(listPermission);
            }
        });
        builder.setNegativeButton("لغو", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
                interfaceGetPermissions.onResultRequestPermissions(false, "کاربر گرامی شما درخواست را لغو کردید !!!", -1);
            }
        });
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    private void reqPermission(ListPermission permission)
    {

        String namePermissions = getNamePermission(permission, LanguageApp.English);
        int codeRequest = getCodeRequestPermission(permission);

        if (namePermissions.equals("-1") || codeRequest == -1)
        {
            interfaceGetPermissions.onResultRequestPermissions(false, "این مجوز پیدا نشد لطفا در انتخاب مجوز دقت کنید", -1);
            return;
        }
        ActivityCompat.requestPermissions(activity, new String[]{namePermissions}, codeRequest);

    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {

        int reqCode = getCodeRequestPermission(PERMISSION);
        if (requestCode == reqCode)
        {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                interfaceGetPermissions.onResultRequestPermissions(true, "مجوز با موفقیت ثبت شد", reqCode);
            }
            else
            {
                interfaceGetPermissions.onResultRequestPermissions(false, "مجوز رد شد", reqCode);
            }

        }

        else
            interfaceGetPermissions.onResultRequestPermissions(false, "این کد مجوز یافت نشد", reqCode);

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }


    private String getNamePermission(ListPermission permission, LanguageApp language)
    {


        switch (language)
        {

            case English:
            {
                switch (permission)
                {
                    case CAMERA:
                        return Manifest.permission.CAMERA;

                    case INTERNET:
                        return Manifest.permission.INTERNET;

                    case RECORD_AUDIO:
                        return Manifest.permission.RECORD_AUDIO;
                    case RECEIVE_SMS:
                        return Manifest.permission.RECEIVE_SMS;
                    case WRITE_EXTERNAL_STORAGE:
                        return Manifest.permission.WRITE_EXTERNAL_STORAGE;
                    case READ_EXTERNAL_STORAGE:
                        return Manifest.permission.READ_EXTERNAL_STORAGE;

                    default:
                        return "-1";

                }
            }

            case Persian:
            {
                switch (permission)
                {
                    case CAMERA:
                        return "دوربین";

                    case INTERNET:
                        return "اینترنت";

                    case RECORD_AUDIO:
                        return "ضبط صدا";

                    case RECEIVE_SMS:
                        return "اس ام اس";
                    case WRITE_EXTERNAL_STORAGE:
                        return "نوشتن روی حافظه";
                    case READ_EXTERNAL_STORAGE:
                        return "خواندن از حافظه";
                    case ACCESS_MEDIA_LOCATION:
                        return "خواندن و نوشتن از حافظه";

                    default:
                        return "-1";


                }
            }

        }

        return "-1";


    }

    public int getCodeRequestPermission(ListPermission permission)
    {

        switch (permission)
        {
            case CAMERA:
                return REQUEST_CODE_CAMERA;

            case INTERNET:
                return REQUEST_CODE_INTERNET;

            case RECORD_AUDIO:
                return REQUEST_CODE_RECORD_AUDIO;
            case RECEIVE_SMS:
                return REQUEST_CODE_SMS;
            case WRITE_EXTERNAL_STORAGE:
                if (permissionMode.equals("camera"))
                    return REQUEST_CODE_WRITE_STORAGE_CAMERA;
                else if (permissionMode.equals("gallery"))
                    return REQUEST_CODE_WRITE_STORAGE_GALLERY;
            default:
                return -1;

        }
    }


}
