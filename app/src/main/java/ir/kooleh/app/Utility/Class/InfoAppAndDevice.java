package ir.kooleh.app.Utility.Class;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.widget.Toast;

import com.jaredrummler.android.device.DeviceName;

import ir.kooleh.app.Structure.Model.App.ModelInfoAppDevice;

public class InfoAppAndDevice
{

    private ModelInfoAppDevice modelInfoAppDevice;
    private Context context;

    public InfoAppAndDevice(Context context)
    {
        this.context = context;
        modelInfoAppDevice = new ModelInfoAppDevice();
        DeviceName.init(context);
    }

    public static String getVersion(Context context)
    {
        try
        {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionName;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static int getVersionCode(Context context)
    {
        try
        {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionCode;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            e.printStackTrace();
            return -1;
        }
    }

    public static String getModelDevice(Context context)
    {
        DeviceName.init(context);
        return DeviceName.getDeviceName();
    }

    public static String getAndroidId()
    {
        return Settings.Secure.ANDROID_ID;
    }



    public ModelInfoAppDevice getInfoDeviceAndApp()
    {
        try
        {

            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            modelInfoAppDevice.setVersion(pInfo.versionName);
            modelInfoAppDevice.setVersionCode(pInfo.versionCode);
            modelInfoAppDevice.setModel(DeviceName.getDeviceName());

        }
        catch (Exception ex)
        {
            modelInfoAppDevice.setManufacturer("no find");
            modelInfoAppDevice.setMarketName("no find");
            modelInfoAppDevice.setModel("no find");
            modelInfoAppDevice.setCodename("no find");
            Toast.makeText(context, "خطا اطلاعات گوشی شما یافت نشد !!! ", Toast.LENGTH_LONG).show();
        }

        return modelInfoAppDevice;
    }


}

