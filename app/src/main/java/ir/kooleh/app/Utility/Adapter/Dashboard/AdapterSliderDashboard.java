package ir.kooleh.app.Utility.Adapter.Dashboard;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Service.ModelIndexService;
import ir.kooleh.app.Utility.Class.OpenActivityOrFragmentFromServer;
import ir.kooleh.app.View.Ui.Activity.general.WebViewActivity;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterSliderDashboard extends RecyclerView.Adapter<AdapterSliderDashboard.HolderSliderAdDetails>
{

    private final Activity activity;
    private List<ModelIndexService.Slider> listSliderService;

    public AdapterSliderDashboard(Activity activity)
    {
        this.activity = activity;
    }

    @NonNull
    @Override
    public HolderSliderAdDetails onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new HolderSliderAdDetails(LayoutInflater.from(activity).inflate(R.layout.item_row_service_slider, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull AdapterSliderDashboard.HolderSliderAdDetails holder, int position)
    {
        ModelIndexService.Slider slider = listSliderService.get(position);

        if (slider.getImage() != null && slider.getImage().length() > 0)
            Glide.with(activity).load(slider.getImage())
                    .placeholder(R.drawable.img_name_logo_kooleh)
                    .into(holder.ivServiceSlider);

        holder.tvServiceSlider.setText(slider.getText());

        if (slider.getText() == null || slider.getText().equals(""))
            holder.tvServiceSlider.setVisibility(View.GONE);
        else
            holder.tvServiceSlider.setText(slider.getText());

        holder.rlSliderService.setOnClickListener(v ->
        {
            if (slider.getAction() != null)
            {
                switch (slider.getAction())
                {
                    case "web":
                        Intent intent = new Intent(activity, WebViewActivity.class);
                        intent.putExtra("value", slider.getValue());
                        activity.startActivity(intent);
//                        OpenActivityOrFragmentFromServer.openWeb(context, slider.getValue());
                        break;
                    case "browser":
                        String uri = Uri.parse(slider.getValue())
                                .buildUpon()
                                .build().toString();
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        activity.startActivity(browserIntent);
                        break;
                    case "activity":
                        OpenActivityOrFragmentFromServer.openActivity(activity, slider.getValue());
                        break;
                    case "fragment":
                        OpenActivityOrFragmentFromServer.openFragment(activity, slider.getValue(), R.id.fragment_home);
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return listSliderService == null ? 0 : listSliderService.size();
    }

    public void setListSlider(List<ModelIndexService.Slider> listSliderService)
    {
        this.listSliderService = listSliderService;
        notifyDataSetChanged();
    }



    static class HolderSliderAdDetails extends RecyclerView.ViewHolder
    {
        private final RelativeLayout rlSliderService;
        private final ImageView ivServiceSlider;
        private final CTextView tvServiceSlider;

        public HolderSliderAdDetails(View itemView)
        {
            super(itemView);
            rlSliderService = itemView.findViewById(R.id.rl_item_row_slider_fragment_service);
            ivServiceSlider = itemView.findViewById(R.id.iv_row_service_slider);
            tvServiceSlider = itemView.findViewById(R.id.tv_row_service_slider);
        }
    }
}
