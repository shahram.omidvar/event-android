package ir.kooleh.app.Utility.Adapter.Dashboard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Dashboard.ModelMostViewedDashboard;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterOfferFeed extends RecyclerView.Adapter<AdapterOfferFeed.HolderMostViewedDashboard>
{

    private List<ModelMostViewedDashboard> mostViewedDashboardList = new ArrayList<>();
    private Context context;

    public AdapterOfferFeed(Context context)
    {
        this.context = context;
    }

    @NonNull
    @Override
    public HolderMostViewedDashboard onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new HolderMostViewedDashboard(LayoutInflater.from(context).inflate(R.layout.item_most_viewed_dashboard, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HolderMostViewedDashboard holder, int position)
    {
        ModelMostViewedDashboard modelMostViewedDashboard = mostViewedDashboardList.get(position);
        holder.ivAd.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        holder.ivAd.setImageResource(R.drawable.img_name_logo_kooleh);

        if (modelMostViewedDashboard.getImage() != null  && modelMostViewedDashboard.getImage().length() > 0)
        {
            holder.ivAd.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Glide.with(context).load(modelMostViewedDashboard.getImage()).placeholder(R.drawable.img_name_logo_kooleh).into(holder.ivAd);
        }
        holder.tvTitle.setText(modelMostViewedDashboard.getTitle());
        holder.tvExplain.setText(modelMostViewedDashboard.getExplain());
    }

    @Override
    public int getItemCount()
    {
        return mostViewedDashboardList == null ? 0 : mostViewedDashboardList.size();
    }


    public void setList(List<ModelMostViewedDashboard> mostViewedDashboardList)
    {
        this.mostViewedDashboardList = mostViewedDashboardList;
        notifyDataSetChanged();
    }

    public class HolderMostViewedDashboard extends RecyclerView.ViewHolder
    {
        private ImageView ivAd;
        private CTextView tvTitle, tvExplain;
        public HolderMostViewedDashboard(@NonNull View itemView)
        {
            super(itemView);
            ivAd = itemView.findViewById(R.id.iv_most_viewed_dashboard);
            tvTitle = itemView.findViewById(R.id.tv_title_most_viewed);
            tvExplain = itemView.findViewById(R.id.tv_explain_most_viewed);
        }
    }

}
