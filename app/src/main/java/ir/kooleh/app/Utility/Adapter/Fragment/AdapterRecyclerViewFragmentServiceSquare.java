package ir.kooleh.app.Utility.Adapter.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Service.ModelIndexService;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.Utility.Class.OpenActivityOrFragmentFromServer;
import ir.kooleh.app.View.Ui.Activity.Tools.CompassActivity;
import ir.kooleh.app.View.Ui.Activity.general.WebViewActivity;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterRecyclerViewFragmentServiceSquare extends RecyclerView.Adapter<AdapterRecyclerViewFragmentServiceSquare.ServiceSquareViewHolder>
{

    private final Activity activity;
    private List<ModelIndexService.ServiceSquare> serviceSquareList;
    private CustomToast customToast;

    public AdapterRecyclerViewFragmentServiceSquare(Activity activity)
    {
        this.activity = activity;
        customToast = new CustomToast(activity);
    }

    @NonNull
    @Override
    public ServiceSquareViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ServiceSquareViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_row_list_service_square, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceSquareViewHolder holder, int position)
    {

        ModelIndexService.ServiceSquare serviceSquare = serviceSquareList.get(position);

        if (serviceSquare.getImage() != null && serviceSquare.getImage().length() > 0)
            Glide.with(activity).load(serviceSquare.getImage())
                    .placeholder(R.drawable.img_name_logo_kooleh)
                    .into(holder.ivServices);

        holder.tvServices.setText(serviceSquare.getName());

        if (serviceSquare.getName() == null) holder.tvServices.setVisibility(View.GONE);
        else holder.tvServices.setText(serviceSquare.getName());

        holder.rlService.setOnClickListener(v ->
        {
            if (serviceSquare.getAction().equals("web"))
            {
                Intent intent = new Intent(activity, WebViewActivity.class);
                intent.putExtra("value", serviceSquare.getValue());
                activity.startActivity(intent);
            }
            else if (serviceSquare.getAction().equals("browser"))
            {
                String uri = Uri.parse(serviceSquare.getValue())
                        .buildUpon()
                        .build().toString();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                activity.startActivity(browserIntent);
            }

            else if (serviceSquare.getAction().equals("activity"))
            {
                activity.startActivity(new Intent(activity, CompassActivity.class));
//                OpenActivityOrFragmentFromServer.openActivity(
//                        activity,
//                        serviceSquare.getValue());
            }

            else if (serviceSquare.getAction().equals("fragment"))
            {
                OpenActivityOrFragmentFromServer.openFragment(
                        activity,
                        serviceSquare.getValue(),
                        R.id.fragment_home);
            }
            else if (serviceSquare.getAction().equals("toast"))
            {
                customToast = new CustomToast(activity.getApplicationContext());
                customToast.showInfo(serviceSquare.getValue());
            }

        });

    }

    @Override
    public int getItemCount()
    {
        return serviceSquareList == null ? 0 : serviceSquareList.size();
    }

    static class ServiceSquareViewHolder extends RecyclerView.ViewHolder
    {

        private final LinearLayout rlService;
        private final ImageView ivServices;
        private final CTextView tvServices;

        public ServiceSquareViewHolder(@NonNull View itemView)
        {
            super(itemView);
            rlService = itemView.findViewById(R.id.rl_item_row_rv_service);
            ivServices = itemView.findViewById(R.id.iv_item_rv_services);
            tvServices = itemView.findViewById(R.id.tv_item_rv_services);
            tvServices.setSelected(true);
        }
    }

    public void addListService(List<ModelIndexService.ServiceSquare> serviceSquareList)
    {
        this.serviceSquareList = serviceSquareList;
        notifyDataSetChanged();
    }

}
