package ir.kooleh.app.Utility.Class;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.Structure.Model.Dashboard.ModelProfileDetails;

public class UtilStrings
{

    public static List<ModelProfileDetails> getProfileDetailsList(String infoOne, String infoTwo)
    {
        List<ModelProfileDetails> profileDetailsList = new ArrayList<>();
        String[] infoOneList = infoOne.split("\r\n|\r|\n");
        String[] infoTwoList = infoTwo.split("\r\n|\r|\n");
        for (String s : infoOneList)
        {
            ModelProfileDetails modelProfileDetails = new ModelProfileDetails();
            if (checkForValidSplit(s, ":"))
            {
                modelProfileDetails.setLabel(s.split(":")[0].trim());
                modelProfileDetails.setValue(s.split(":")[1].trim());
            }
            else
            {
                modelProfileDetails.setLabel(s);
                modelProfileDetails.setValue("");
            }
            profileDetailsList.add(modelProfileDetails);
        }

        if (!infoTwo.equals(""))
        {
            for (String s : infoTwoList)
            {
                ModelProfileDetails modelProfileDetails = new ModelProfileDetails();
                if (checkForValidSplit(s, ":"))
                {
                    modelProfileDetails.setLabel(s.split(":")[0].trim());
                    modelProfileDetails.setValue(s.split(":")[1].trim());
                }
                else
                {
                    modelProfileDetails.setLabel(s);
                    modelProfileDetails.setValue("");
                }
                profileDetailsList.add(modelProfileDetails);
            }
        }

        return profileDetailsList;
    }


    public static boolean checkForValidSplit(String infoOne, String strSplit)
    {
        return infoOne.contains(strSplit);
    }


    public static String splitButtonNameFromNumber(String strName)
    {
        return strName.replaceAll("\\d","");

    }

    public static String splitNumberFromButtonName(String strName)
    {
        return strName.replaceAll("\\D+","");
    }
}
