package ir.kooleh.app.Utility.Adapter.Search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Search.InterfaceSearch;
import ir.kooleh.app.Structure.Model.Search.ModelSearchItem;

public class AdapterSearchModelSelectItem extends RecyclerView.Adapter<AdapterSearchModelSelectItem.HolderSearchModel>
{

    private Context context;
    private List<ModelSearchItem.Result> listSearchItem;
    private InterfaceSearch.SelectItem interfaceSearch;

    public AdapterSearchModelSelectItem(Context context, InterfaceSearch.SelectItem interfaceSearch)
    {

        this.context = context;
        this.interfaceSearch = interfaceSearch;
    }

    @NonNull
    @Override
    public HolderSearchModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new HolderSearchModel(
                LayoutInflater.from(context).inflate(R.layout.item_row_select_item,
                        parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HolderSearchModel holder, int position)
    {
        final ModelSearchItem.Result item = listSearchItem.get(position);
        holder.tvTiltSearchItem.setText(item.getTitle());

        if (item.getImage() != null && !item.getImage().equals(""))
            Glide.with(context).load(item.getImage().toString()).placeholder(R.drawable.img_name_logo_kooleh).
                    into(holder.ivSearchItem);

        holder.itemView.setOnClickListener(view -> interfaceSearch.onClickListSearchSelectItem(item));
    }

    @Override
    public int getItemCount()
    {
        return listSearchItem == null ? 0 : listSearchItem.size();
    }


    public void setListSearchItem(List<ModelSearchItem.Result> listSearchItem)
    {
        this.listSearchItem = listSearchItem;
        notifyDataSetChanged();
    }

    static class HolderSearchModel extends RecyclerView.ViewHolder
    {
        private TextView tvTiltSearchItem;
        private ImageView ivSearchItem;

        public HolderSearchModel(@NonNull View itemView)
        {
            super(itemView);
            tvTiltSearchItem = itemView.findViewById(R.id.tv_item_row_select_item);
            ivSearchItem = itemView.findViewById(R.id.iv_item_row_select_item);

        }
    }
}
