package ir.kooleh.app.Utility.Class;

import ir.kooleh.app.Structure.Enum.EnumBtnAdDetails;

public class AdDetailsUtils
{

    public static String getBtnAction(String strAction, String strValue)
    {
        switch (strAction)
        {
            case EnumBtnAdDetails.ActionType.API:
                return getApiActionValue(strValue);
            case EnumBtnAdDetails.ActionType.DIALOG:
            case EnumBtnAdDetails.ActionType.CALL:
            case EnumBtnAdDetails.ActionType.OPEN_ACTIVITY:
                return getCallAndDialogActionValue(strAction , strValue);

        }
        return EnumBtnAdDetails.NONE;
    }



    private static String getApiActionValue(String strValue)
    {
        String superValueType = strValue.split("#")[0];
        String valueType = strValue.split("#")[1];
        String valueTypeValue = valueType.split("=")[1];
        return superValueType + "#" + valueTypeValue;
    }

    private static String getCallAndDialogActionValue(String strAction, String strValue)
    {
        return strAction + "#" + strValue;
    }


}
