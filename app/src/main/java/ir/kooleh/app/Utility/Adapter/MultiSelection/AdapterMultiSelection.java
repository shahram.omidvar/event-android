package ir.kooleh.app.Utility.Adapter.MultiSelection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.kooleh.app.R;

import ir.kooleh.app.Structure.Interface.MultiSelection.InterfaceMultiSelection;

import ir.kooleh.app.Structure.Model.MultiSelection.ModelMultiSelection;


public class AdapterMultiSelection extends RecyclerView.Adapter<AdapterMultiSelection.HolderAlertShowList>
{

    private final Context context;
    private List<ModelMultiSelection> listMultiSelection;
    private final InterfaceMultiSelection.Adapter interfaceMultiSelection;

    public AdapterMultiSelection(Context context, InterfaceMultiSelection.Adapter interfaceMultiSelection)
    {
        this.context = context;
        this.interfaceMultiSelection = interfaceMultiSelection;
    }


    @NonNull
    @Override
    public HolderAlertShowList onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new HolderAlertShowList(
                LayoutInflater.from(context).inflate(R.layout.item_row_multi_selection,
                        parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HolderAlertShowList holder, int position)
    {
        final ModelMultiSelection multiSelection = listMultiSelection.get(position);
        holder.tvTitleRowMultiSelection.setText(String.format("%s", multiSelection.getTitle()));
        if (!multiSelection.getImage().toString().equals(""))
        {
            Glide.with(context).load(multiSelection.getImage().toString()).placeholder(R.drawable.img_name_logo_kooleh)
                    .into(holder.ivIconRowMultiSelection);
        }

        holder.itemView.setOnClickListener(view -> interfaceMultiSelection.onClickItemMultiSelection(multiSelection));
    }

    @Override
    public int getItemCount()
    {
        return listMultiSelection == null ? 0 : listMultiSelection.size();
    }


    public void setListAlert(List<ModelMultiSelection> listMultiSelection)
    {
        this.listMultiSelection = listMultiSelection;
        notifyDataSetChanged();
    }

    static class HolderAlertShowList extends RecyclerView.ViewHolder
    {
        private final TextView tvTitleRowMultiSelection;
        private final ImageView ivIconRowMultiSelection;

        public HolderAlertShowList(@NonNull View itemView)
        {
            super(itemView);
            tvTitleRowMultiSelection = itemView.findViewById(R.id.tv_title_row_multi_selection);
            ivIconRowMultiSelection = itemView.findViewById(R.id.iv_icon_row_multi_selection);

        }
    }
}
