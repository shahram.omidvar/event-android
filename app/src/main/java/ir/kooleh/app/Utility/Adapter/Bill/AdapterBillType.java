package ir.kooleh.app.Utility.Adapter.Bill;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Bills.ModelBillCategories;
import ir.kooleh.app.View.Ui.Activity.Bill.BillInquiryActivity;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterBillType extends RecyclerView.Adapter<AdapterBillType.HolderBillType>
{

    private Activity activity;
    private List<ModelBillCategories.Child> billTypeList;

    public AdapterBillType(Activity activity, List<ModelBillCategories.Child> billTypeList)
    {
        this.activity = activity;
        this.billTypeList = billTypeList;
    }

    @NonNull
    @Override
    public HolderBillType onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new HolderBillType(LayoutInflater.from(activity).inflate(R.layout.item_bill_category_sub_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HolderBillType holder, int position)
    {
        ModelBillCategories.Child modelBillType = billTypeList.get(position);
        holder.tvTitle.setText(modelBillType.getfName());

        Glide.with(activity).load(modelBillType.getImage()).placeholder(R.drawable.img_name_logo_kooleh).into(holder.icon);
        holder.containerImage.setCardBackgroundColor(Color.parseColor(modelBillType.getBackColor()));

        holder.containerImage.setOnClickListener(v -> {
            Intent intent = new Intent(activity, BillInquiryActivity.class);
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            intent.putExtra(BillInquiryActivity.KEY_OBJECT_BILL_TYPE, gson.toJson(modelBillType));
            activity.startActivity(intent);
        });
    }

    @Override
    public int getItemCount()
    {
        return billTypeList == null ? 0 : billTypeList.size();
    }


    public class HolderBillType extends RecyclerView.ViewHolder
    {
        private CardView containerImage;
        private ImageView icon;
        private CTextView tvTitle;
        public HolderBillType(@NonNull View itemView)
        {
            super(itemView);
            containerImage = itemView.findViewById(R.id.container_icon_bill_type);
            icon = itemView.findViewById(R.id.iv_icon_bill_type);
            tvTitle = itemView.findViewById(R.id.tv_title_bill_type);
            tvTitle.setSelected(true);
        }
    }

}
