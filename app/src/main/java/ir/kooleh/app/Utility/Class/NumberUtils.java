package ir.kooleh.app.Utility.Class;

import java.util.Locale;

public class NumberUtils
{

    public static String[] numberArraySequenceGenerator(int startNumber, int endNumber)
    {
        String[] arr = new String[endNumber - startNumber];
        for (int i = startNumber; i < endNumber + startNumber; i++)
        {
            arr[i - startNumber] = String.format(Locale.ENGLISH, "%02d", startNumber + i);
        }
        return arr;
    }

    public static String[] englishToPersianNumberConvertor(String[] englishNumbers)
    {
        char[] arabicChars = {'۰','۱','۲','۳','۴','۵','۶','۷','۸','۹'};
        String[] persianNumbers = new String[englishNumbers.length];

        for (int i = 0; i < englishNumbers.length; i++)
        {
            StringBuilder builder = new StringBuilder();
            for (int j = 0; j < englishNumbers[i].length(); j++)
            {
                if (Character.isDigit(englishNumbers[i].charAt(j)))
                {
                    builder.append(arabicChars[(int) (englishNumbers[i].charAt(j)) - 48]);
                }
                else
                {
                    builder.append(englishNumbers[i].charAt(i));
                }
            }
            persianNumbers[i] = String.valueOf(builder);
        }
        return persianNumbers;
    }

    public static String digitDivider(String s) {
        StringBuilder answer = new StringBuilder();
        char[] c = new StringBuilder(s).reverse().toString().toCharArray();
        if (c.length > 3) {
            for (int i = 0; i + 3 < c.length; i += 3) {
                answer.append(c[i]).append(c[i + 1]).append(c[i + 2]).append(",");
                if (i + 3 == c.length - 1) {
                    answer.append(c[i + 3]);
                }
                if (i + 4 == c.length - 1) {
                    answer.append(c[i + 3]).append(c[i + 4]);
                }
                if (i + 5 == c.length - 1) {
                    answer.append(c[i + 3]).append(c[i + 4]).append(c[i + 5]);
                }
            }
        } else {
            return s;
        }

        return new StringBuilder(answer.toString()).reverse().toString();
    }



    public static boolean isNumeric(String s) {
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");
    }

}
