package ir.kooleh.app.Utility.Adapter.Wallet;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceRecyclerViewSelectGateway;
import ir.kooleh.app.Structure.Model.Wallet.ModelGateway;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterGateways extends RecyclerView.Adapter<AdapterGateways.HolderGateway>
{

    private Context context;
    private List<ModelGateway> gatewayList;
    private InterfaceRecyclerViewSelectGateway interfaceCustomDialogSelectGateway;
    private int lastSelectedItemPosition = -1;

    public AdapterGateways(InterfaceRecyclerViewSelectGateway interfaceCustomDialogSelectGateway, Context context, List<ModelGateway> gatewayList)
    {
        this.context = context;
        this.interfaceCustomDialogSelectGateway = interfaceCustomDialogSelectGateway;
    }

    @NonNull
    @Override
    public HolderGateway onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new HolderGateway(LayoutInflater.from(context).inflate(R.layout.item_gateway, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HolderGateway holder, @SuppressLint("RecyclerView") int position)
    {
        ModelGateway modelGateway = gatewayList.get(position);
        holder.tvTitle.setText(modelGateway.getFaName());
        if (modelGateway.isSelected())
        {
            if (!modelGateway.getBackColor().equals(""))
            {
                holder.containerImage.setCardBackgroundColor(Color.parseColor(modelGateway.getBackColor()));
            }
            else
            {
                holder.containerImage.setCardBackgroundColor(context.getResources().getColor(R.color.primary));
            }
        }
        else
        {
            holder.containerImage.setCardBackgroundColor(context.getResources().getColor(R.color.gray_50));
        }

        Glide.with(context).load(modelGateway.getImage()).placeholder(R.drawable.img_name_logo_kooleh).into(holder.icon);

        holder.containerImage.setOnClickListener(v -> {
            if (!modelGateway.isSelected())
            {
                gatewayList.get(position).setSelected(true);
                if (lastSelectedItemPosition != -1)
                {
                    gatewayList.get(lastSelectedItemPosition).setSelected(false);
                }
                lastSelectedItemPosition = position;
                interfaceCustomDialogSelectGateway.onResponseSelectGateway(modelGateway.getName());
            }
            else
            {
                gatewayList.get(position).setSelected(false);
                lastSelectedItemPosition = -1;
                interfaceCustomDialogSelectGateway.onResponseSelectGateway("");
            }
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount()
    {
        return gatewayList == null ? 0 : gatewayList.size();
    }


    public void setList(List<ModelGateway> gatewayList)
    {
        this.gatewayList = gatewayList;
        notifyDataSetChanged();
    }


    public class HolderGateway extends RecyclerView.ViewHolder
    {
        private CardView containerImage;
        private ImageView icon;
        private CTextView tvTitle;
        public HolderGateway(@NonNull View itemView)
        {
            super(itemView);
            containerImage = itemView.findViewById(R.id.container_icon_bill_type);
            icon = itemView.findViewById(R.id.iv_icon_bill_type);
            tvTitle = itemView.findViewById(R.id.tv_title_bill_type);
            tvTitle.setSelected(true);
        }
    }
}
