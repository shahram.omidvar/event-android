package ir.kooleh.app.Utility.Adapter.Dashboard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Dashboard.ModelProfileDetails;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterProfileDetails extends RecyclerView.Adapter<AdapterProfileDetails.HolderProfileDetails>
{

    private List<ModelProfileDetails> profileDetailsList;
    private Context context;

    public AdapterProfileDetails(Context context)
    {
        this.context = context;
    }

    @NonNull
    @Override
    public HolderProfileDetails onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new HolderProfileDetails(
                LayoutInflater.from(context).inflate(R.layout.item_profile_details, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HolderProfileDetails holder, int position)
    {
//        holder.tvLabel.setText(profileDetailsList.get(position).getLabel());
//        holder.tvValue.setText(profileDetailsList.get(position).getValue());
    }

    @Override
    public int getItemCount()
    {
        return profileDetailsList == null ? 7 : profileDetailsList.size();
    }


    public void setList(List<ModelProfileDetails> profileDetailsList)
    {
        this.profileDetailsList = profileDetailsList;
        notifyDataSetChanged();
    }

    public static class HolderProfileDetails extends RecyclerView.ViewHolder
    {
        CTextView tvLabel, tvValue;


        public HolderProfileDetails(@NonNull View itemView)
        {
            super(itemView);
//            tvLabel = itemView.findViewById(R.id.tv_label_profile_details);
//            tvValue = itemView.findViewById(R.id.tv_value_profile_details);
//            tvValue.setSelected(true);
        }
    }
}
