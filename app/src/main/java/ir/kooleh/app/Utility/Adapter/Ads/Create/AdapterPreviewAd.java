package ir.kooleh.app.Utility.Adapter.Ads.Create;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Ads.ModelPreviewAd;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterPreviewAd extends RecyclerView.Adapter<AdapterPreviewAd.HolderPreviewAd>
{
    private Activity activity;
    private List<ModelPreviewAd> previewAdList;

    public AdapterPreviewAd(Activity activity)
    {
        this.activity = activity;
    }

    @NonNull
    @Override
    public HolderPreviewAd onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new HolderPreviewAd(LayoutInflater.from(activity).inflate(R.layout.item_preview_ad, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HolderPreviewAd holder, int position)
    {
        holder.tvField.setText(previewAdList.get(position).getField());
        holder.tvValue.setText(previewAdList.get(position).getValue());
    }

    @Override
    public int getItemCount()
    {
        return previewAdList == null ? 0 : previewAdList.size();
    }


    public void setList(List<ModelPreviewAd> previewAdList)
    {
        this.previewAdList = previewAdList;
    }

    public class HolderPreviewAd extends RecyclerView.ViewHolder
    {
        private CTextView tvField, tvValue;
        public HolderPreviewAd(@NonNull View itemView)
        {
            super(itemView);
            tvField = itemView.findViewById(R.id.tv_field_preview_ad);
            tvValue = itemView.findViewById(R.id.tv_value_preview_ad);
        }
    }
}
