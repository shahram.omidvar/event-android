package ir.kooleh.app.Utility.Adapter.Ads.Details;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Ads.ModelItemAd;

public class AdapterAdDetails extends RecyclerView.Adapter<AdapterAdDetails.AdsDetailsSpecialHolder>
{
    private List<ModelItemAd> listItemAds;
    private Context context;

    public AdapterAdDetails(Context context)
    {

        this.context = context;
    }


    @NonNull
    @Override
    public AdsDetailsSpecialHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {

        return new AdsDetailsSpecialHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_ad_details, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull AdsDetailsSpecialHolder holder, int position)
    {
        if (position % 2 == 0)
        {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.gray_50));
        }
        else
        {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        if (listItemAds != null)
        {
            holder.tvItemRight.setText(listItemAds.get(position).getRightText());
            holder.tvItemLeft.setText(listItemAds.get(position).getLeftText());
        }

    }

    @Override
    public int getItemCount()
    {

        return listItemAds == null ? 0 : listItemAds.size();

    }


    public void setListItemAds(List<ModelItemAd> listItemAds)
    {
        this.listItemAds = listItemAds;
        notifyDataSetChanged();
    }

    static class AdsDetailsSpecialHolder extends RecyclerView.ViewHolder
    {

        TextView tvItemRight, tvItemLeft;

        public AdsDetailsSpecialHolder(@NonNull View itemView)
        {
            super(itemView);
            tvItemRight = itemView.findViewById(R.id.tv_item_right_item_ads);
            tvItemLeft = itemView.findViewById(R.id.tv_item_left_item_ads);
        }
    }
}


