package ir.kooleh.app.Utility.Adapter.Bill;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Bills.ModelBillInquiry;

public class AdapterBillDetails extends RecyclerView.Adapter<AdapterBillDetails.AdsDetailsSpecialHolder>
{
    private List<ModelBillInquiry.Base> billInquiryList;
    private Context context;

    public AdapterBillDetails(Context context)
    {

        this.context = context;
    }


    @NonNull
    @Override
    public AdsDetailsSpecialHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {

        return new AdsDetailsSpecialHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_ad_details, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull AdsDetailsSpecialHolder holder, int position)
    {
        if (position % 2 == 0)
        {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.gray_50));
        }
        else
        {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        if (billInquiryList != null)
        {
            holder.tvItemRight.setText(billInquiryList.get(position).getLeft());
            holder.tvItemLeft.setText(billInquiryList.get(position).getRight());
        }

    }


    @Override
    public int getItemCount()
    {
        return billInquiryList == null ? 0 : billInquiryList.size();
    }


    public void setList(List<ModelBillInquiry.Base> billInquiryList)
    {
        this.billInquiryList = billInquiryList;
        notifyDataSetChanged();
    }

    static class AdsDetailsSpecialHolder extends RecyclerView.ViewHolder
    {

        TextView tvItemRight, tvItemLeft;

        public AdsDetailsSpecialHolder(@NonNull View itemView)
        {
            super(itemView);
            tvItemRight = itemView.findViewById(R.id.tv_item_right_item_ads);
            tvItemLeft = itemView.findViewById(R.id.tv_item_left_item_ads);
        }
    }
}


