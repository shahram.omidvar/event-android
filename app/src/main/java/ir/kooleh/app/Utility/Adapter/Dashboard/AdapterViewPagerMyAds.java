package ir.kooleh.app.Utility.Adapter.Dashboard;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import ir.kooleh.app.View.Ui.Fragment.FragmentMyAdAdStep;

public class AdapterViewPagerMyAds extends FragmentStateAdapter
{
    private final int viewPagerSize;
    public static final String POSITION = "position";
    public static final String AD_TYPE_VALUE = "ad_type";
    public static final String SCROLL_TO_END = "scrollToEnd";
    private int adType;
    private boolean scrollToEnd;

    public AdapterViewPagerMyAds(@NonNull FragmentActivity fragmentActivity, int viewPagerSize, int adType, boolean scrollToEnd)
    {
        super(fragmentActivity);
        this.viewPagerSize = viewPagerSize;
        this.adType = adType;
        this.scrollToEnd = scrollToEnd;
    }

    @NonNull @Override public Fragment createFragment(int position) {
        FragmentMyAdAdStep fragmentMyAdStep = new FragmentMyAdAdStep();
        Bundle bundle = new Bundle();
        bundle.putInt(POSITION, position);
        bundle.putInt(AD_TYPE_VALUE, adType);
        bundle.putBoolean(SCROLL_TO_END, scrollToEnd);
        fragmentMyAdStep.setArguments(bundle);
        return fragmentMyAdStep;
    }
    @Override public int getItemCount() {
        return viewPagerSize;
    }


}
