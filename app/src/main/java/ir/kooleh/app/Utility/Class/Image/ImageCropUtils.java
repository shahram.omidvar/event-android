package ir.kooleh.app.Utility.Class.Image;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;

import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import ir.kooleh.app.Structure.Model.Image.ModelListImage;

public class ImageCropUtils
{
    public static void setCroppedImage(Intent data, ModelListImage modelListImage)
    {
        Uri uri = UCrop.getOutput(data);
        File file = new File(uri.getPath());
        InputStream inputStream = null;
        try
        {
            inputStream = new FileInputStream(file);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte[] byteArray = baos.toByteArray();
        modelListImage.setModel("camara");
        modelListImage.setBitmap(bitmap);
        modelListImage.setBase64(Base64.encodeToString(byteArray, Base64.DEFAULT));
    }



    public static File createImageFile() {
        String imageFileName = "JPEG_" + System.currentTimeMillis() + "_";
        File storageDir = new File(
                Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DCIM
                ), "kooleh"
        );

        if (! storageDir.exists()){
            storageDir.mkdir();
        }
        File file = null;
        try
        {
            file = File.createTempFile(
                    imageFileName, ".jpg", storageDir
            );
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return file;
    }


    public static boolean deleteImage(String path)
    {
        File fdelete = new File(path);
        boolean check =  fdelete.exists() && fdelete.delete();
        return check;
    }
}