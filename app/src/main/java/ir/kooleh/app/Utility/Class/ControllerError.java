package ir.kooleh.app.Utility.Class;

import android.content.Context;
import android.content.Intent;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;


import ir.kooleh.app.R;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;
import ir.kooleh.app.View.Ui.Activity.Auth.CheckExistsUserActivity;

public class ControllerError
{
    private final Context context;
    private final UserShared usernameShared;

    public ControllerError(Context context)
    {
        this.context = context;
        usernameShared = new UserShared(context);

    }


    public  String checkVolleyError(VolleyError error)
    {
        String message = null;
        NetworkResponse networkResponse = error.networkResponse;
        if (networkResponse != null)
        {
            switch (networkResponse.statusCode)
            {
                case 401:
                    logout();
                    message = " لطفا دوباره وارد شوید";
                    break;
                case 403:
                    message = "این  URL  درست ست نشده است !!!";
                    break;
                case 404:
                    message = "این URL پیدا نشد";
                    break;

                case 422:
                    message = " خطا در ارسال پارمتر های مورد نیاز به سرور   لطفا با پشتیبانی تماس بگیرید";
                    break;

                case 500:
                    message = " خطای سمت سرور رخ داد لطفا دوباره تلاش کنید در صورت مشاهده دوباره خطا لطفا با پشتیبانی تماس بگیرید";
                    break;

                default:
                    message = "خطایی ناشناخته";
            }

            message += "  ( " + networkResponse.statusCode + " ) ";
            //  Toast.makeText(context, message, Toast.LENGTH_LONG).show();

        }

        if (message == null)
//            message = "  Bad URL ( " + 403 + " ) ";
            message = "خطا در اتصال به سرور. لطفا اینترنت خود را چک کنید.";

        return message;
    }

    public String checkJSONException(String TAG, JSONException error)
    {
        return Controller.context.getString(R.string.report_to_support) + error.getMessage();
    }


    public String checkException(String TAG, Exception error)
    {
        return Controller.context.getString(R.string.report_to_support) + error.getMessage();
    }

    public String responseMessageError(JSONObject jsonObject)
    {
        String message = "";
        try
        {
            message = jsonObject.getString("message");
        }
        catch (JSONException e)
        {
            message = e.getMessage();

        }
        return message;
    }


    //در صورتی که Api  توکن منقضی شده باشد
    private void logout()
    {
        usernameShared.deletePreferences("con");
        Intent i = new Intent(context, CheckExistsUserActivity.class);
        context.startActivity(i);
    }
}
