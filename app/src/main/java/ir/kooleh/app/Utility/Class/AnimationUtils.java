package ir.kooleh.app.Utility.Class;

import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.view.View;
import android.view.animation.AlphaAnimation;

public class AnimationUtils
{

    public static void objectAnimatorOfObject(View view, String propertyName, TypeEvaluator evaluator, int duration, Object... values)
    {
        final ObjectAnimator objectAnimatorOfObject = ObjectAnimator.ofObject(view,
                propertyName,
                evaluator,
                values);
        objectAnimatorOfObject.setDuration(duration);
        objectAnimatorOfObject.start();
    }

    public static void objectAnimatorOfFloat(View view, String propertyName, int duration, float value)
    {
        ObjectAnimator objectAnimatorOfFloat = ObjectAnimator.ofFloat(view, propertyName,  value);
        objectAnimatorOfFloat.setDuration(duration);
        objectAnimatorOfFloat.start();
    }

    public static void alphaAnimation(View view, int duration, float startingAlpha, float endingAlpha, int visibility)
    {
        view.setVisibility(visibility);
        AlphaAnimation alphaAnimation = new AlphaAnimation(startingAlpha, endingAlpha);
        alphaAnimation.setDuration(duration);
        view.startAnimation(alphaAnimation);
    }
}
