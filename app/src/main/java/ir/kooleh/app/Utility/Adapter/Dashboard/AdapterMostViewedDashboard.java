package ir.kooleh.app.Utility.Adapter.Dashboard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Bills.ModelListSuggestion;
import ir.kooleh.app.Structure.Model.Dashboard.ModelMostViewedDashboard;
import ir.kooleh.app.View.Ui.Activity.Tools.ServiceActivity;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterMostViewedDashboard extends RecyclerView.Adapter<AdapterMostViewedDashboard.HolderMostViewedDashboard>
{

    private List<ModelListSuggestion.Suggestion> suggestionList;
    private Activity activity;

    public AdapterMostViewedDashboard(Activity activity, List<ModelListSuggestion.Suggestion> suggestionList)
    {
        this.activity = activity;
        this.suggestionList = suggestionList;
    }

    @NonNull
    @Override
    public HolderMostViewedDashboard onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new HolderMostViewedDashboard(LayoutInflater.from(activity).inflate(R.layout.item_most_viewed_dashboard, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HolderMostViewedDashboard holder, int position)
    {
        ModelListSuggestion.Suggestion suggestion = suggestionList.get(position);
        holder.ivAd.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        holder.ivAd.setImageResource(R.drawable.img_name_logo_kooleh);

        if (suggestion.getImage() != null  && suggestion.getImage().length() > 0)
        {
            holder.ivAd.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Glide.with(activity).load(suggestion.getImage()).placeholder(R.drawable.img_name_logo_kooleh).into(holder.ivAd);
        }
        holder.tvTitle.setText(suggestion.getTitle());
        holder.tvExplain.setText(suggestion.getStory());
        holder.tvMessageImage.setText(suggestion.getMessageImage());

        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(activity, ServiceActivity.class);
            intent.putExtra(ServiceActivity.SERVICE_ID, suggestion.getId().toString());
            activity.startActivity(intent);
        });
    }

    @Override
    public int getItemCount()
    {
        return suggestionList == null ? 0 : suggestionList.size();
    }


    public void setList(List<ModelListSuggestion.Suggestion> suggestionList)
    {
        this.suggestionList = suggestionList;
        notifyDataSetChanged();
    }

    public class HolderMostViewedDashboard extends RecyclerView.ViewHolder
    {
        private ImageView ivAd;
        private CTextView tvTitle, tvExplain, tvMessageImage;
        public HolderMostViewedDashboard(@NonNull View itemView)
        {
            super(itemView);
            ivAd = itemView.findViewById(R.id.iv_most_viewed_dashboard);
            tvMessageImage = itemView.findViewById(R.id.tv_message_image_most_viewed);
            tvTitle = itemView.findViewById(R.id.tv_title_most_viewed);
            tvExplain = itemView.findViewById(R.id.tv_explain_most_viewed);
        }
    }

}
