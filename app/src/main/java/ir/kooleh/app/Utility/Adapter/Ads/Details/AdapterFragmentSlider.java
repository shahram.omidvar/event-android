package ir.kooleh.app.Utility.Adapter.Ads.Details;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Slider.ModelSlider;

public class AdapterFragmentSlider extends RecyclerView.Adapter<AdapterFragmentSlider.HolderFragmentSlider>
{

    List<ModelSlider> listSlider;
    private final Activity activity;

    public AdapterFragmentSlider(Activity activity, List<ModelSlider> listSlider)
    {
        this.activity = activity;
        this.listSlider = listSlider;
    }

    @NonNull
    @Override
    public HolderFragmentSlider onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new HolderFragmentSlider(LayoutInflater.from(activity).inflate(R.layout.item_slider_photoview, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull AdapterFragmentSlider.HolderFragmentSlider holder, int position)
    {
        Glide.with(activity).load(listSlider.get(position).getImage()).placeholder(R.drawable.img_name_logo_kooleh).into(holder.photoView);
    }

    @Override
    public int getItemCount()
    {
        return listSlider == null ? 0 : listSlider.size();
    }

    static class HolderFragmentSlider extends RecyclerView.ViewHolder
    {
        private PhotoView photoView;
        public HolderFragmentSlider(@NonNull View itemView)
        {
            super(itemView);
            photoView = itemView.findViewById(R.id.photoview);
        }
    }
}
