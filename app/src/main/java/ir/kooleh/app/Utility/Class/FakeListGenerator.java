package ir.kooleh.app.Utility.Class;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Ads.ModelAuctionOffer;

public class FakeListGenerator
{
    public static List<ModelAuctionOffer> getFakeListAuctionOffers(Context context)
    {
        List<ModelAuctionOffer> auctionOfferList = new ArrayList<>();
        for (int i = 0; i < 5; i++)
        {
            ModelAuctionOffer modelAuctionOffer = new ModelAuctionOffer();
            modelAuctionOffer.setName("متین");
            modelAuctionOffer.setExplain(context.getResources().getString(R.string.lorem_large));
            auctionOfferList.add(modelAuctionOffer);
        }
        return auctionOfferList;
    }

//    public static List<ModelViewValue> getFakeListAdTypes(String[] types)
//    {
//        List<ModelViewValue> modelViewValueList = new ArrayList<>();
//        for (int i = 0; i < types.length; i++)
//        {
//            ModelViewValue modelViewValue = new ModelViewValue();
//            modelViewValue.setFaName(types[i]);
//            modelViewValue.setId(1);
//            modelViewValueList.add(modelViewValue);
//        }
//        return modelViewValueList;
//    }
}
