package ir.kooleh.app.Utility.Class;

public class InterClassDataPassKeys
{

    public static class FragmentMessageHandler
    {
        public static final String FRAGMENT_TYPE = "home_fragment_type";
        public static final String MESSAGE_TYPE = "message_type";
    }
}
