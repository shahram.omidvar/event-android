package ir.kooleh.app.Utility.Adapter.Dashboard;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import ir.kooleh.app.View.Ui.Fragment.FragmentMyAdAdStep;
import ir.kooleh.app.View.Ui.Fragment.FragmentTripsTours;

public class AdapterViewPagerTripsTours extends FragmentStateAdapter
{
    private int viewPagerSize;
    public static final String POSITION = "position";
    public static final String KEYONE = "keyOne";
    public static final String KEYTWO = "keyTwo";
    String keyOne, keyTwo;

    public AdapterViewPagerTripsTours(@NonNull FragmentActivity fragmentActivity, int viewPagerSize, String keyOne, String keyTwo)
    {
        super(fragmentActivity);
        this.keyOne = keyOne;
        this.keyTwo = keyTwo;
        this.viewPagerSize = viewPagerSize;
    }

    @NonNull @Override public Fragment createFragment(int position) {
        FragmentTripsTours fragmentTripsTours = new FragmentTripsTours();
        Bundle bundle = new Bundle();
        bundle.putInt(POSITION, position);
        bundle.putString(KEYONE, keyOne);
        bundle.putString(KEYTWO, keyTwo);
        fragmentTripsTours.setArguments(bundle);
        return fragmentTripsTours;
    }
    @Override public int getItemCount() {
        return viewPagerSize;
    }

}
