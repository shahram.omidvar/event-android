package ir.kooleh.app.Utility.Adapter.View;

import android.app.FragmentManager;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;


import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;


import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Enum.EnTypeDateTime;
import ir.kooleh.app.Structure.Interface.Dialog.InterfaceDateTimePicker;
import ir.kooleh.app.Structure.Interface.View.InterfacePickers;
import ir.kooleh.app.Structure.ListView.InterfaceView;
import ir.kooleh.app.Structure.Model.View.ModelOption;
import ir.kooleh.app.Structure.Model.View.ModelView;
import ir.kooleh.app.Structure.Model.View.ModelViewValue;
import ir.kooleh.app.Structure.StaticValue.SvInputTypeValue;
import ir.kooleh.app.Structure.StaticValue.SvOptionView;
import ir.kooleh.app.View.Ui.Dialog.CustomDialogTimePicker;
import ir.kooleh.app.View.Ui.Dialog.DateAndTimePickerDialog;
import ir.kooleh.app.View.Layout.AlertDialogView;


public class AdapterViewButton extends RecyclerView.Adapter<AdapterViewButton.ListViewButtonViewHolder>
        implements InterfaceView.AlertDialog.AlertDialogView, InterfaceDateTimePicker,
        InterfacePickers.IntTimePickerDialogResult
{
    private static List<ModelView.Button> listViewButton;
    private Context context;
    private AlertDialogView alertDialogView;
    private final DateAndTimePickerDialog dateAndTimePickerDialog;

    public AdapterViewButton(Context context, FragmentManager fragmentManager, InterfaceView.Button.AdapterButton iAdapterButton)
    {
        this.context = context;
        dateAndTimePickerDialog = new DateAndTimePickerDialog(this, fragmentManager);
    }


    @NonNull
    @Override
    public ListViewButtonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ListViewButtonViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_row_view_button, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewButtonViewHolder holder, int position)
    {

        ModelView.Button modelButton = listViewButton.get(position);
        holder.textInputLayoutBtn.setHint(modelButton.getFaName());

        if (modelButton.getRequired())
            holder.etButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_next_red, 0, 0, 0);
        else
            holder.etButton.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_next_gray, 0, 0, 0);


        holder.etButton.setOnClickListener(v ->
        {
            onClickItemViewButton(holder, modelButton);
        });


        // set other option

        List<ModelOption> listOption = modelButton.getListOption();

        for (ModelOption option : listOption)
        {

            // set Helper text
            if (option.getNameOption().equals(SvOptionView.HELPER_TEXT))
                holder.textInputLayoutBtn.setHelperText(option.getValue());


                // set padding

            else if (option.getNameOption().equals(SvOptionView.PADDING))
            {
                holder.etButton.setPadding
                        (
                                Integer.parseInt(option.getValue()),
                                Integer.parseInt(option.getValue()),
                                Integer.parseInt(option.getValue()),
                                Integer.parseInt(option.getValue())
                        );
            }


            else if (option.getNameOption().equals(SvOptionView.PADDING_HORIZONTAL))
            {
                holder.etButton.setPadding
                        (
                                Integer.parseInt(option.getValue()),
                                0,
                                Integer.parseInt(option.getValue()),
                                0
                        );
            }


            else if (option.getNameOption().equals(SvOptionView.PADDING_VERTICAL))
            {
                holder.etButton.setPadding
                        (
                                0,
                                Integer.parseInt(option.getValue()),
                                0,
                                Integer.parseInt(option.getValue())
                        );
            }


        }


    }


    @Override
    public int getItemCount()
    {

        return listViewButton == null ? 0 : listViewButton.size();

    }


    public void setListEditTextView(List<ModelView.Button> listViewButton)
    {
        AdapterViewButton.listViewButton = listViewButton;
        notifyDataSetChanged();
    }


    // view holder

    static class ListViewButtonViewHolder extends RecyclerView.ViewHolder
    {

        TextInputLayout textInputLayoutBtn;
        EditText etButton;

        public ListViewButtonViewHolder(@NonNull View itemView)
        {
            super(itemView);
            textInputLayoutBtn = itemView.findViewById(R.id.txt_input_view_button);
            etButton = itemView.findViewById(R.id.et_view_button);

        }
    }


    // other


    //---------------

    ListViewButtonViewHolder holderSelect;

    private void onClickItemViewButton(ListViewButtonViewHolder holder, ModelView.Button modelButton)
    {
        holderSelect = holder;

        if (modelButton.getInputType().equals(SvInputTypeValue.DATE_PICKER))
            dateAndTimePickerDialog.showDate();

        else if (modelButton.getInputType().equals(SvInputTypeValue.TIME_PICKER))
        {
            CustomDialogTimePicker customDialogTimePicker = new CustomDialogTimePicker();
            customDialogTimePicker.make(context, this, listViewButton.get(holder.getAdapterPosition()).getUserInput());
            customDialogTimePicker.show();
        }

        else
        {
            alertDialogView = new AlertDialogView(this);
            alertDialogView.Create(context, modelButton.getFaName(), modelButton.getListValue());
            alertDialogView.show();
        }

    }


    @Override
    public void onResponseDateTime(EnTypeDateTime enTypeDateTime, String value)
    {
        if (holderSelect == null)
            return;

        if (enTypeDateTime == EnTypeDateTime.DATE && value != null)
        {
            holderSelect.etButton.setText(value);
            listViewButton.get(holderSelect.getAdapterPosition()).setUserInput(value);
            listViewButton.get(holderSelect.getAdapterPosition()).setValue(value);
        }

        else if (enTypeDateTime == EnTypeDateTime.TIME && value != null)
        {

        }

    }


    @Override
    public void onReponseTimePickerDialog(String strTime)
    {
        holderSelect.etButton.setText(strTime);
        listViewButton.get(holderSelect.getAdapterPosition()).setUserInput(strTime);
        listViewButton.get(holderSelect.getAdapterPosition()).setValue(strTime);
    }


    @Override
    public void onResponseAlertDialogView(ModelViewValue value, int requestCode, String choosenSequence)
    {

        if (value != null && holderSelect != null)
        {
            holderSelect.textInputLayoutBtn.setHelperText(choosenSequence);
            holderSelect.etButton.setText(value.getFaName());

            listViewButton.get(holderSelect.getAdapterPosition()).setUserInput(value.getId() + "");
            listViewButton.get(holderSelect.getAdapterPosition()).setValue(value.getFaName());

        }
    }


    public String checkRequiredData()
    {
        for (ModelView.Button btn : listViewButton)
        {
            if (btn.getRequired())
            {
                if (btn.getUserInput() == null
                        || btn.getUserInput().trim().equals(""))
                    return "خطا لطفا " + btn.getFaName() + " را وارد کنید ";

            }

        }
        return "";
    }

}


