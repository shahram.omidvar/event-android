package ir.kooleh.app.Utility.Adapter.Ads.Auction;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Ads.ModelAuctionOffer;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class AdapterAuctionOffers extends RecyclerView.Adapter<AdapterAuctionOffers.HolderAuctionOffers>
{
        private final Context context;
        private List<ModelAuctionOffer> auctionOfferList;

        public AdapterAuctionOffers(Context context, List<ModelAuctionOffer> auctionOfferList)
        {
            this.context = context;
            this.auctionOfferList = auctionOfferList;
        }


        @NonNull
        @Override
        public HolderAuctionOffers onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            return new AdapterAuctionOffers.HolderAuctionOffers(
                    LayoutInflater.from(context).inflate(R.layout.item_row_auction_offer,
                            parent, false));
        }


    @Override
        public void onBindViewHolder(@NonNull AdapterAuctionOffers.HolderAuctionOffers holder, int position)
        {
            final ModelAuctionOffer modelAuctionOffer = auctionOfferList.get(position);

            if (modelAuctionOffer.getImage() != null && !modelAuctionOffer.getImage().equals(""))
                Glide.with(context).load(modelAuctionOffer.getImage()).placeholder(R.drawable.ic_avatar).into(holder.imgUser);
            holder.tvFullname.setText(modelAuctionOffer.getName());
            holder.tvExplain.setText(modelAuctionOffer.getExplain());
            holder.tvDate.setText(modelAuctionOffer.getDate());
        }

        @Override
        public int getItemCount()
        {
            return auctionOfferList == null ? 0 : auctionOfferList.size();
        }


        public void setList(List<ModelAuctionOffer> auctionOfferList)
        {
            this.auctionOfferList = auctionOfferList;
            notifyDataSetChanged();
        }

        public static class HolderAuctionOffers extends RecyclerView.ViewHolder
        {
            private final ImageView imgUser;
            private final CTextView tvFullname;
            private final CTextView tvExplain;
            private final  CTextView tvDate;

            public HolderAuctionOffers(@NonNull View itemView)
            {
                super(itemView);
                imgUser = itemView.findViewById(R.id.img_user_auction_offer);
                tvFullname = itemView.findViewById(R.id.tv_username_auction_offer);
                tvExplain = itemView.findViewById(R.id.tv_explain_auction_offer);
                tvDate = itemView.findViewById(R.id.tv_date_auction_offer);
            }
        }
}
