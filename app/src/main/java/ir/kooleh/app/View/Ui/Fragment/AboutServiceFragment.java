package ir.kooleh.app.View.Ui.Fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import de.hdodenhof.circleimageview.CircleImageView;
import ir.kooleh.app.R;
import ir.kooleh.app.View.Ui.Activity.Tools.ComplimentItemsAboutAdapter;
import ir.kooleh.app.View.Ui.Activity.Tools.DividerItemDecoration;
import ir.kooleh.app.View.Ui.Activity.Tools.ServiceActivity;
import ir.kooleh.app.View.Ui.Activity.Tools.ServiceHomeAdapter;
import ir.kooleh.app.View.Ui.Activity.Tools.ServiceObject;
import ir.kooleh.app.View.Ui.Activity.Tools.TabAboutService;
import ir.kooleh.app.View.Ui.Activity.Tools.TagAboutAdapter;
import ir.kooleh.app.View.ViewCustom.CTextView;



public class AboutServiceFragment extends Fragment implements View.OnClickListener
{


    private String serviceId;
    private TabAboutService tabAboutService;

    private ComplimentItemsAboutAdapter complimentItemsAboutAdapter;
    private ServiceHomeAdapter relatedItemsAboutAdapter;
    private TagAboutAdapter tagAboutAdapter;

    private CTextView tvTitleAboutService;
    private ExpandableTextView expandTextViewDescription;
    private CTextView expandableText;
    private ImageButton expandCollapse;
    private CircleImageView ivImageCircleProfile;
    private CTextView tvNameOwnerService;
    private CTextView tvDesOwnerService;
    private CTextView tvDateCreatorService;
    private CTextView tvSimilarService;
    private RecyclerView rvComplimentService;
    private RecyclerView rvRelatedService;
    private RecyclerView rvTagService;


    public AboutServiceFragment()
    {
        // Required empty public constructor
    }


    public static AboutServiceFragment newInstance(String serviceId)
    {
        AboutServiceFragment fragment = new AboutServiceFragment();
        Bundle args = new Bundle();
        args.putString(serviceId, "1");
        fragment.setArguments(args);
        return fragment;
    }

    public void setTabAboutService(TabAboutService tabAboutService)
    {
        this.tabAboutService = tabAboutService;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            serviceId = getArguments().getString("1");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_about_service, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        tvTitleAboutService =  view.findViewById(R.id.tv_title_about_service);
        expandTextViewDescription = (ExpandableTextView) view.findViewById(R.id.expand_text_view_description);
        expandableText =  view.findViewById(R.id.expandable_text);
        expandCollapse = (ImageButton) view.findViewById(R.id.expand_collapse);
        ivImageCircleProfile =  view.findViewById(R.id.iv_image_circle_profile);
        tvNameOwnerService =  view.findViewById(R.id.tv_agnomen_creator_about_service);
        tvDesOwnerService =  view.findViewById(R.id.tv_name_creator_about_service);
        tvDateCreatorService =  view.findViewById(R.id.tv_date_creator_about_service);
        tvSimilarService =  view.findViewById(R.id.tv_title_similar_about_service);
        rvComplimentService = (RecyclerView) view.findViewById(R.id.rv_compliments_about_service);
        rvRelatedService = (RecyclerView) view.findViewById(R.id.rv_related_items_about_service);
        rvTagService = (RecyclerView) view.findViewById(R.id.rv_tag_about_service);
        expandCollapse.setOnClickListener(this);
        setUpData();
    }

    @Override
    public void onClick(View v)
    {
        if (v == expandCollapse)
        {
            // Handle clicks for expandCollapse
        }
    }


    private void setUpData()
    {
        if (tabAboutService == null)
        {
            return;
        }
        else
        {
            //   tvTitleAboutService.setText(tabAboutService);
            expandTextViewDescription.setText(tabAboutService.getAbout());
            tvDesOwnerService.setText(tabAboutService.getCreator().getName());
            tvNameOwnerService.setText(tabAboutService.getCreator().getAgnomen());
            if (tabAboutService.getCreator().getImage() != null && !tabAboutService.getCreator().getImage().equals(""))
            {
                Glide.with(getContext()).load(tabAboutService.getCreator().getImage()).into(ivImageCircleProfile);
            }
            // tvDateCreatorService.setText(tabAboutService.get);


            complimentItemsAboutAdapter = new ComplimentItemsAboutAdapter(getContext(), tabAboutService.getCompliments());
            rvComplimentService.setLayoutManager(new LinearLayoutManager(getContext()));
            rvComplimentService.addItemDecoration(new DividerItemDecoration(getContext()));
            rvComplimentService.setAdapter(complimentItemsAboutAdapter);

            if (tabAboutService.getServiceObjects().size() == 0)
            {
                tvSimilarService.setVisibility(View.GONE);
                rvRelatedService.setVisibility(View.GONE);

            }
            else
            {
                tvSimilarService.setVisibility(View.VISIBLE);
                rvRelatedService.setVisibility(View.VISIBLE);
            }
            relatedItemsAboutAdapter = new ServiceHomeAdapter(getContext(), tabAboutService.getServiceObjects(), new ServiceHomeAdapter.OnClickItemListener()
            {
                @Override
                public void onClickItem(View view, ServiceObject item)
                {
                    Intent i = new Intent(getContext(), ServiceActivity.class);
                    i.putExtra("1", "" + item.getId());
                    startActivity(i);
                }
            });

            rvRelatedService.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

            rvRelatedService.setAdapter(relatedItemsAboutAdapter);

            tagAboutAdapter = new TagAboutAdapter(getContext(), tabAboutService.getTags(), new TagAboutAdapter.OnClickItemListener()
            {
                @Override
                public void onClickItem(View view, String item)
                {

                }
            });
            rvTagService.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            ;
            rvTagService.setAdapter(tagAboutAdapter);
        }

    }


}
