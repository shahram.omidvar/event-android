package ir.kooleh.app.View.Ui.Activity.Auth;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.telephony.SmsMessage;

import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import androidx.core.app.ActivityCompat;


import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.auth.ApiCode;
import ir.kooleh.app.Structure.Enum.EnumListCommendApi;
import ir.kooleh.app.Structure.Enum.EnumVerifyCode;
import ir.kooleh.app.Structure.Enum.ListPermission;
import ir.kooleh.app.Structure.Interface.Auth.InterfaceCode;

import ir.kooleh.app.Structure.Interface.Custom.InterfaceGetPermissions;
import ir.kooleh.app.Structure.Model.User.ModelUser;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.Utility.Class.SettingKeyboard;
import ir.kooleh.app.Utility.Class.ShowPermissionDialog;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.View.Ui.Activity.Home.HomeActivity;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class VerifyCodeActivity extends AppCompatActivity implements
        View.OnClickListener,
        InterfaceCode.VerifyCode,
        InterfaceCode.SendCode,
        InterfaceGetPermissions
{
    //view
    private ImageView ivBack;
    private EditText etCode;
    private Button btnContinue;
    private Button btnSendAgainCode;
    private CTextView tvInSeconds;
    private TextView tvPhoneNumber, tvCountVerify;

    //var
    private Activity context;
    private Intent intent;
    private ViewLoading viewLoading;
    private CustomToast customToast;
    private UserShared userShared;
    private BroadcastReceiver broadcastReceiver = null;
    private ApiCode apiCode;
    private ShowPermissionDialog permissionDialog;


    private int lengthCode = -1;
    private String phoneNumber = "-1", type = "-1";

    public static final String PHONE_NUMBER = "phone_number";
    public static final String TYPE = "type";
    public static final String LENGTH_CODE = "length_code";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_code);
        context = VerifyCodeActivity.this;
        findView();
        createVar();
        getIntentData();
        setProp();

        requestPermission();
        SettingKeyboard.showKeyboard(context, etCode);

    }

    private void findView()
    {
        ivBack = findViewById(R.id.iv_back_verify_code);
        tvPhoneNumber = findViewById(R.id.tv_phone_number_verify);
        tvPhoneNumber = findViewById(R.id.tv_phone_number_verify);
        btnContinue = findViewById(R.id.btn_verify_code);
        etCode = findViewById(R.id.et_verify_code);
        tvCountVerify = findViewById(R.id.tv_count_down_verify_code);
        btnSendAgainCode = findViewById(R.id.btn_again_request_code);
        tvInSeconds = findViewById(R.id.tv_in_seconds_verify_code);

        btnContinue.setOnClickListener(this);
        btnSendAgainCode.setOnClickListener(this);
        ivBack.setOnClickListener(this);


        etCode.setOnEditorActionListener((v, actionId, event) ->
        {
            if (actionId == EditorInfo.IME_ACTION_DONE)
            {
                verifyCode();
                return true;
            }
            return false;
        });
    }

    private void createVar()
    {
        viewLoading = new ViewLoading(context);
        apiCode = new ApiCode(context);
        userShared = new UserShared(context);
        permissionDialog = new ShowPermissionDialog(this, this);
    }

    private void getIntentData()
    {
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        phoneNumber = bundle.getString(PHONE_NUMBER, "-1");
        type = bundle.getString(TYPE, "-1");
        lengthCode = bundle.getInt(LENGTH_CODE, -1);

        if (phoneNumber.equals("-1")
                || phoneNumber.length() != 11
                || type.equals("-1")
                || lengthCode == -1
        )
        {
            customToast = new CustomToast(this);
            customToast.showError(getString(R.string.error_send_data_intent));
            finish();
            return;
        }

        if (!type.equals(EnumVerifyCode.Register) &&
                !type.equals(EnumVerifyCode.LoginByCode) &&
                !type.equals(EnumVerifyCode.ResetPassword))
        {
            customToast = new CustomToast(this);
            customToast.showError("خطا در ارسال دیتا برای نوع کد");
            finish();
        }

    }

    private void setProp()
    {
        tvPhoneNumber.setText(String.format("%s ارسال شد ", phoneNumber));
        setCountDownTimer();
    }

    private void setCountDownTimer()
    {
        tvCountVerify.setVisibility(View.VISIBLE);
        tvInSeconds.setVisibility(View.VISIBLE);
        btnSendAgainCode.setEnabled(false);
        btnSendAgainCode.setTextColor(context.getResources().getColor(R.color.black));

        CountDownTimer countDownTimer = new CountDownTimer(180000, 1000)
        {
            @Override
            public void onTick(long l)
            {
                tvCountVerify.setText(String.valueOf(l / 1000));
            }

            @Override
            public void onFinish()
            {
                btnSendAgainCode.setEnabled(true);
                tvCountVerify.setVisibility(View.GONE);
                tvInSeconds.setVisibility(View.GONE);
                btnSendAgainCode.setText("ارسال مجدد کد");
                btnSendAgainCode.setTextColor(context.getResources().getColor(R.color.color_primary));
            }
        };
        countDownTimer.start();
    }


    private void requestPermission()
    {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED)
        {
            permissionDialog.getPermissionFromUser(ListPermission.RECEIVE_SMS);
        }
    }


    @Override
    public void onClick(View view)
    {
        if (view == ivBack)
            finish();
        else if (view == btnContinue)
            verifyCode();
        else if (view == btnSendAgainCode)
        {
            viewLoading.show();
            apiCode.SendCode(this, phoneNumber, type);
        }

    }

    private void verifyCode()
    {
        String code = etCode.getText().toString();
        if (code.length() != lengthCode)
        {
            customToast = new CustomToast(this);
            customToast.showWarnig("لطفا کد را کامل وارد کنید");
            return;
        }
        viewLoading.show();
        apiCode.VerifyCode(this, phoneNumber, code, type);
    }


    @Override
    public void onResponseVerifyCode(boolean response, String message, String commend, ModelUser user)
    {
        viewLoading.close();
        SettingKeyboard.hideKeyboard(this);
        if (response)
        {
            customToast = new CustomToast(this);
            customToast.showSuccess(message);
            if (commend.equals(EnumListCommendApi.VerifyRegister))
            {
                intent = new Intent(VerifyCodeActivity.this, RegisterActivity.class);
                intent.putExtra(RegisterActivity.PHONE_NUMBER, phoneNumber);
                startActivity(intent);
                finish();
            }


            else if (commend.equals(EnumListCommendApi.Register))
            {
                intent = new Intent(VerifyCodeActivity.this, RegisterActivity.class);
                intent.putExtra(RegisterActivity.PHONE_NUMBER, phoneNumber);
                startActivity(intent);
                finish();
            }

            else if (commend.equals(EnumListCommendApi.Home))
            {
                userShared.setData(user);
                Intent intent = new Intent(context, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }

            else if (commend.equals(EnumListCommendApi.Login))
            {
                intent = new Intent(VerifyCodeActivity.this, LoginActivity.class);
                intent.putExtra(LoginActivity.PHONE_NUMBER, phoneNumber);
                startActivity(intent);
                finish();
            }

            else
            {
                customToast = new CustomToast(this);
                customToast.showError("این ساختار پیدا نشد");
            }
        }

        else
        {
            customToast = new CustomToast(this);
            customToast.showError(message);
        }
    }


    private void readLatestSms(Bundle bundle)
    {

        Object[] message = (Object[]) bundle.get("pdus");
        String smsBody = "";

        try
        {
            for (Object o : message)
            {
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) o);
                smsBody = smsMessage.getMessageBody();

                int i = smsBody.indexOf(":");
                i=i+2;
                String sms = smsBody.substring(i, i + 4);
                etCode.setText(sms);
                verifyCode();
                return;
            }
            if (broadcastReceiver != null)
            {
                this.unregisterReceiver(broadcastReceiver);
            }
        }
        catch (Exception e)
        {
            customToast = new CustomToast(this);
            customToast.showError("خطا در خواندن پیام");
        }

    }


    @Override
    public void onResponseSendCode(boolean response, String message, String commend, int lengthCode)
    {
        viewLoading.close();
        if (response)
        {
            this.lengthCode = lengthCode;
            customToast = new CustomToast(this);
            customToast.showSuccess(message);
            setCountDownTimer();
        }
        else
        {
            customToast = new CustomToast(this);
            customToast.showError(message);
        }

    }

    @Override
    public void onResultRequestPermissions(boolean isOk, String message, int CodePermission)
    {
        if (!isOk)
        {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
        else
        {
            customToast = new CustomToast(this);
            customToast.showError(message + " لطفا دوباره انتخاب کنید");
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if (broadcastReceiver == null)
        {
            broadcastReceiver = new BroadcastReceiver()
            {
                @Override
                public void onReceive(Context context, Intent intent)
                {
                    Bundle bundle = intent.getExtras();
                    readLatestSms(bundle);
                }
            };
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
            intentFilter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
            this.registerReceiver(broadcastReceiver, intentFilter);
        }
    }
}