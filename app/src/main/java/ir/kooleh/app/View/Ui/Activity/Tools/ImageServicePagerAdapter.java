package ir.kooleh.app.View.Ui.Activity.Tools;

import android.content.Context;
import android.net.Uri;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;


public class ImageServicePagerAdapter extends PagerAdapter {

    private Context context;
    private OnClickPictures onClickPictures;
    private List<String> items;

    public ImageServicePagerAdapter(Context context, List<String> items) {
        this.context = context;
        this.items = items ;
    }
    public ImageServicePagerAdapter(Context context, String item) {
        this.context = context;
        items = new ArrayList<>();
        this.items.add(item);
    }

    @Override
    public Object instantiateItem(ViewGroup viewGroup, final int position) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_image_service, viewGroup, false);


        ImageView imageView = ((ImageView) view.findViewById(R.id.iv_service_item));
        if (items.size()!=0)
            Glide.with(context).load(Uri.parse(items.get(position))).placeholder(R.drawable.img_splash_gibar).into(imageView);
        else
            Glide.with(context).load(R.drawable.img_splash_gibar).into(imageView);

        viewGroup.addView(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickPictures != null)
                    onClickPictures.OnClickPitures(v, position);
            }
        });

        return view;
    }

    @Override
    public int getCount() {
        if (items.size() == 0)
            return 1;
        else
            return items.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);

    }

    public void setOnclick(OnClickPictures onclick) {
        this.onClickPictures = onclick;
    }

    public interface OnClickPictures {
        void OnClickPitures(View view, int position);
    }
}
