package ir.kooleh.app.View.ViewCustom;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import ir.kooleh.app.R;
import ir.kooleh.app.Utility.Class.NumberUtils;
import ir.kooleh.app.Utility.Volley.Controller;


public class CEditText extends androidx.appcompat.widget.AppCompatEditText
{
    private boolean digitSeperator;
    private int maxLenght;
    public CEditText(@NonNull Context context)
    {
        super(context);
        if (!isInEditMode())
            setTypeface(Controller.MyTypeface);
    }

    public CEditText(@NonNull Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        if (!isInEditMode())
            setTypeface(Controller.MyTypeface);

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CEditText, 0, 0);
        try {
            digitSeperator = ta.getBoolean(R.styleable.CEditText_digitSeperator, false);
            maxLenght = ta.getInt(R.styleable.CEditText_maxLenghtDigitSeperator, 10);
        } finally {
            ta.recycle();
        }
    }

    public CEditText(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode())
            setTypeface(Controller.MyTypeface);
    }

    @Override
    public void addTextChangedListener(TextWatcher watcher)
    {

        if (digitSeperator)
        {
            super.addTextChangedListener(new TextWatcher()
            {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count)
                {
                    removeTextChangedListener(this);
                    int length;
                    if (s.length() < 10)
                        length = s.length();
                    else
                        length = maxLenght;
                    s = NumberUtils.digitDivider(s.toString().substring(0, length).replaceAll(",", ""));
                    setText(s);
                    setSelection(getText().toString().length());
                    addTextChangedListener(this);
                }

                @Override
                public void afterTextChanged(Editable s) { }
            });
        }
        else
        {
            super.addTextChangedListener(watcher);
        }

    }
}
