package ir.kooleh.app.View.ViewCustom;



import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRadioButton;

import ir.kooleh.app.R;
import ir.kooleh.app.Utility.Volley.Controller;


public class CRadioButton extends AppCompatRadioButton
{
    private View view;
    private CTextView tvTitle;
    private CTextView tvValue;

    public CRadioButton(@NonNull Context context)
    {
        super(context);
        init(context);

        if (!isInEditMode())
            setTypeface(Controller.MyTypeface);
    }

    public CRadioButton(@NonNull Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        init(context);

        if (!isInEditMode())
            setTypeface(Controller.MyTypeface);
    }

    public CRadioButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context);

        if (!isInEditMode())
            setTypeface(Controller.MyTypeface);
    }

//
//    private RequestListener<Bitmap> requestListener = new RequestListener<Bitmap>() {
//        @Override
//        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
//            return false;
//        }
//
//        @Override
//        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
//            imageView.setImageBitmap(resource);
//            redrawLayout();
//            return false;
//        }
//    };

//    public void setImageResource(int resId) {
//        Glide.with(getContext())
//                .asBitmap()
//                .load(resId)
//                .apply(RequestOptions.bitmapTransform(
//                        new MultiTransformation<>(
//                                new CenterCrop(),
//                                new RoundedCornersTransformation(dp2px(getContext(), 24), 0, RoundedCornersTransformation.CornerType.ALL))
//                        )
//                )
//                .listener(requestListener)
//                .submit();
//    }

//    public void setImageBitmap(Bitmap bitmap) {
//        Glide.with(getContext())
//                .asBitmap()
//                .load(bitmap)
//                .apply(RequestOptions.bitmapTransform(
//                        new MultiTransformation<>(
//                                new CenterCrop(),
//                                new RoundedCornersTransformation(dp2px(getContext(), 24), 0, RoundedCornersTransformation.CornerType.ALL))
//                        )
//                )
//                .listener(requestListener)
//                .submit();
//    }

    // setText is a final method in ancestor, so we must take another name.
    public void setTitleWith(int resId) {
        tvTitle.setText(resId);
        redrawLayout();
    }

    public void setTitleWith(CharSequence text) {
        tvTitle.setText(text);
        redrawLayout();
    }

    public void setValueWith(int resId) {
        tvValue.setText(resId);
        redrawLayout();
    }

    public void setValueWith(CharSequence text) {
        tvValue.setText(text);
        redrawLayout();
    }

    private void init(Context context) {
        view = LayoutInflater.from(context).inflate(R.layout.custom_layout_radio_button, null);
        tvTitle = view.findViewById(R.id.tv_title_custom_layout_radio_button);
        tvValue = view.findViewById(R.id.tv_value_custom_layout_radio_button);
        redrawLayout();
    }

    private void redrawLayout() {
        view.setDrawingCacheEnabled(true);
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        view.buildDrawingCache(true);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        setCompoundDrawablesWithIntrinsicBounds(new BitmapDrawable(getResources(), bitmap), null, null, null);
        view.setDrawingCacheEnabled(false);
    }

    private int dp2px(Context context, int dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }

}
