package ir.kooleh.app.View.Ui.Activity.Load;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;


import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Comment.ApiComment;
import ir.kooleh.app.Server.Api.Load.ApiLoadActivity;
import ir.kooleh.app.Structure.Enum.EnCustomMessage;
import ir.kooleh.app.Structure.Interface.Comment.InterfaceComment;
import ir.kooleh.app.Structure.Interface.Custom.FragmentCommunicator;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCustomCreateComment;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceSlider;
import ir.kooleh.app.Structure.Interface.Load.InterfaceLoad;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Comment.ModelComment;
import ir.kooleh.app.Structure.Model.Comment.ModelUpdateVoteComment;
import ir.kooleh.app.Structure.Model.Load.ModelLoad;
import ir.kooleh.app.Utility.Adapter.Ads.Details.AdapterAdDetails;
import ir.kooleh.app.Utility.Adapter.Ads.Details.AdapterSliderAdDetails;
import ir.kooleh.app.Utility.Adapter.Comment.AdapterCommentLoad;
import ir.kooleh.app.View.Ui.Dialog.BottomSheetDialogMoreComments;
import ir.kooleh.app.View.Ui.Dialog.CustomCreateComment;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.View.Ui.Fragment.FragmentLoad;
import ir.kooleh.app.View.Ui.Fragment.FragmentMessageHandler;
import ir.kooleh.app.View.Ui.Fragment.FragmentSlider;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class LoadActivity extends AppCompatActivity implements
        InterfaceLoad.Activity,
        View.OnClickListener,
        InterfaceCustomCreateComment,
        InterfaceComment,
        InterfaceSlider,
        InterfaceMessageHandler.FragmentResult,
        FragmentCommunicator
{

    //View
    private ImageView ivBack, ivExtraComment;
    private AppCompatRatingBar ratingStarView;
    private CardView cvCall, cvComment;
    private CTextView tvCountOfComment, tvExtraComment, tvLoadDescription;
    private RecyclerView rvComment, rvAttributeAd;
    private LinearLayout lnrExtra;
    private CTextView tvNoComment;
    private TextView tvTitleToolbarLoad;

    private FrameLayout fragmentContainer;
    private ImageView ivDefaultPic;


    private LinearLayout lnrLoadActivity;
    private FragmentMessageHandler fragmentMessageHandler;
    private ViewPager2 viewPager2Slider;
    private DotsIndicator dotsIndicator;
//    Button btnTryAgain;

    //Var
    private AdapterSliderAdDetails adapterSlider;
    private AdapterCommentLoad adapterCommentLoad;
    private ApiLoadActivity apiLoadActivity;
    private ApiComment apiComment;
    private Activity context;
    private String loadId, adID;
    private ModelLoad modelLoad;
    private ViewLoading viewLoading;
    private CustomToast customToast;
    private CustomCreateComment customCreateComment;
    private AdapterAdDetails adapterAdsDetailsItemLoad;

    private final int nextPage = 1;
    private final int itemsPerPage = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load);

        initView();
        createVar();
        getIntentData();
    }


    public void initView()
    {
        fragmentContainer = findViewById(R.id.contaienr_fragment);
        ivBack = findViewById(R.id.iv_back_toolbar);
        tvTitleToolbarLoad = findViewById(R.id.tv_title_toolbar);

        ratingStarView = findViewById(R.id.rsv_load_activity);
        tvCountOfComment = findViewById(R.id.tv_count_of_comment);
        tvLoadDescription = findViewById(R.id.tv_load_description);
        tvExtraComment = findViewById(R.id.tv_extra_comment_load_activity);
        cvCall = findViewById(R.id.cv_call_load_activity);

        cvComment = findViewById(R.id.cv_comment_load_activity);
        tvNoComment = findViewById(R.id.tv_no_comment_load_activity);

        viewPager2Slider = findViewById(R.id.viewpater2_slider_load_details);
        dotsIndicator = findViewById(R.id.dots_indicator_load_details);
        ivDefaultPic = findViewById(R.id.iv_default_pic_load_details);

        rvAttributeAd = findViewById(R.id.rv_attribute_load);
        rvComment = findViewById(R.id.rv_comment_load_activity);

        lnrExtra = findViewById(R.id.lnr_extra_load_activity);
        ivExtraComment = findViewById(R.id.iv_extra_comment_load_activity);

        lnrLoadActivity = findViewById(R.id.lnr_load_activity);

        ivBack.setOnClickListener(this);
        cvCall.setOnClickListener(this);
        lnrExtra.setOnClickListener(this);

        cvComment.setOnClickListener(this);
    }


    private void createVar()
    {
        context = this;
        viewLoading = new ViewLoading(context);

        apiLoadActivity = new ApiLoadActivity(context);
        apiComment = new ApiComment(context);

        rvAttributeAd.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvAttributeAd.setAdapter(adapterAdsDetailsItemLoad = new AdapterAdDetails(context));

        rvComment.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rvComment.setAdapter(adapterCommentLoad = new AdapterCommentLoad(context));
    }


    private void getIntentData()
    {

        Bundle extras = getIntent().getExtras();
        loadId = extras.getString(FragmentLoad.ID_AD, "-1");

        if (loadId.equals("-1"))
        {
            Toast.makeText(context, "خطا این بار پیدا نشد", Toast.LENGTH_SHORT).show();
            finish();
        }

        else
            getLoad();

    }


    @Override
    public void onClick(View v)
    {

        if (v == ivBack)
        {
            finish();
        }
        else if (v == cvCall)
        {
            context.startActivities(
                    new Intent[]
                            {new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:" + modelLoad.getPhoneNumber()))});
        }
        else if (v == lnrExtra)
        {
            BottomSheetDialogMoreComments bottomSheetDialogMoreComments = new BottomSheetDialogMoreComments(this, adID);
            bottomSheetDialogMoreComments.show(getSupportFragmentManager(), "BottomSheetDialogMoreComments");
//            ivExtraComment.setVisibility(View.GONE);
//            tvExtraComment.setVisibility(View.GONE);
//            getListCommentLoad();

        }
        else if (v == cvComment)
        {
            customCreateComment = new CustomCreateComment(context, this);
            customCreateComment.make(Integer.parseInt(adID));
        }

    }


    private void getLoad()
    {

        lnrLoadActivity.setVisibility(View.GONE);
        viewLoading.show();
        apiLoadActivity.getLoadActivity(this, loadId);
    }


    @Override
    public void onResponseLoadActivity(boolean response, ModelLoad load, String message)
    {

        lnrLoadActivity.setVisibility(View.GONE);
        viewLoading.close();
        if (response)
        {
            setInfoLoad(load);
            lnrLoadActivity.setVisibility(View.VISIBLE);
        }

        else
        {
            fragmentMessageHandler = new FragmentMessageHandler(this);
            Bundle args = new Bundle();
            args.putString("message", message);
            fragmentMessageHandler.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        }
    }


    private void setInfoLoad(ModelLoad load)
    {
        modelLoad = load;
        tvCountOfComment.setText(String.format("نظرات %s", load.getCountComment()));
        lnrLoadActivity.setVisibility(View.VISIBLE);
        adapterAdsDetailsItemLoad.setListItemAds(load.getListAttribute());

        if (load.getListSlider() != null && load.getListSlider().size() >= 1)
        {
            adapterSlider = new AdapterSliderAdDetails(this, load.getListSlider());
            viewPager2Slider.setAdapter(adapterSlider);
            dotsIndicator.setViewPager2(viewPager2Slider);
        }
        else
        {
            ivDefaultPic.setVisibility(View.VISIBLE);
        }

        ratingStarView.setRating(load.getRate());
        adID = load.getIdAd();
        if (load.getListComment() != null && load.getListComment().size() > 0)
        {
            adapterCommentLoad.addList(load.getListComment());
        }
        else
        {
            rvComment.setVisibility(View.GONE);
            tvNoComment.setVisibility(View.VISIBLE);
            lnrExtra.setVisibility(View.GONE);
        }
        tvLoadDescription.setText(modelLoad.getDescription());

        tvTitleToolbarLoad.setText(modelLoad.getTitle());
    }


    private void getListCommentLoad()
    {
//        prbExtraComment.setVisibility(View.VISIBLE);
        apiComment.getListCommentAds(this, adID, nextPage, itemsPerPage);
    }


    @Override
    public void onResponseListComment(boolean response, List<ModelComment> listComment, ModelPaginate modelPaginate, String message)
    {
//        prbExtraComment.setVisibility(View.GONE);
        if (response)
        {
            if (listComment != null && listComment.size() == 0)
            {
//                fragmentMessageHandler = new FragmentMessageHandler(this);
//                Bundle args = new Bundle();
//                args.putString("message", "آیتمی برای نمایش پیدا نشد");
//                args.putSerializable(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE, EnumMessageType.EMPTY);
//                fragmentMessageHandler.setArguments(args);
//                getSupportFragmentManager().beginTransaction().replace(
//                        R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
            }
            adapterCommentLoad.addList(listComment);
        }
        else
        {
            customToast = new CustomToast(this);
            customToast.showError(message);
        }

    }


    @Override
    public void onClickCreateCommend(EnCustomMessage enCustomMessage, int adID, String massage, float rate)
    {
        if (EnCustomMessage.YES == enCustomMessage)
        {
            viewLoading.show();
            apiComment.setPointAndComment(this, massage, rate, adID);
        }
    }

    @Override
    public void onClickCreateCommend(EnCustomMessage enCustomMessage, int adID, String strValue, String massage, float rate)
    {

    }

    @Override
    public void onResponseVoteComment(boolean response, ModelUpdateVoteComment.Result updateVoteComment, String message, int position)
    {

    }

    @Override
    public void onResponseSavePointAndComment(boolean response, String message)
    {
        viewLoading.close();

        if (response)
        {
            customCreateComment.dismiss();
            customToast = new CustomToast(this);
            customToast.showSuccess(message);
        }
        else
        {
            customToast = new CustomToast(this);
            customToast.showError(message);
        }
    }

    @Override
    public void onClickedSlider(int currentItemPos)
    {
        if (modelLoad != null && modelLoad.getListSlider() != null)
        {
            fragmentContainer.setVisibility(View.VISIBLE);
            getSupportFragmentManager().beginTransaction().replace(R.id.contaienr_fragment,
                    new FragmentSlider(this, modelLoad.getListSlider(), currentItemPos)).addToBackStack(null).commit();
        }
    }

    @Override
    public void onClosedSlider()
    {
        fragmentContainer.setVisibility(View.GONE);
    }

    @Override
    public void onResponseMessageHandler()
    {
        getSupportFragmentManager().beginTransaction().remove(fragmentMessageHandler).commit();
        fragmentMessageHandler = null;
        getLoad();
    }

    @Override
    public void fragmentDetached()
    {
        fragmentContainer.setVisibility(View.GONE);
    }
}