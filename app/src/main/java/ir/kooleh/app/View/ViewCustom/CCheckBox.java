package ir.kooleh.app.View.ViewCustom;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import ir.kooleh.app.Utility.Volley.Controller;

public class CCheckBox extends androidx.appcompat.widget.AppCompatCheckBox
{
    public CCheckBox(@NonNull Context context)
    {
        super(context);

        if (!isInEditMode())
            setTypeface(Controller.MyTypeface);
    }

    public CCheckBox(@NonNull Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        if (!isInEditMode())
            setTypeface(Controller.MyTypeface);
    }

    public CCheckBox(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode())
            setTypeface(Controller.MyTypeface);
    }
}
