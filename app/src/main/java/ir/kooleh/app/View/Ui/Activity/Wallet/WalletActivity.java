package ir.kooleh.app.View.Ui.Activity.Wallet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Wallet.ApiWallet;
import ir.kooleh.app.Structure.Enum.EnumWalletModule;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Interface.Wallet.InterfaceWallet;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Wallet.ModelInfoWallet;
import ir.kooleh.app.Structure.Model.Wallet.ModelLogWallet;
import ir.kooleh.app.Utility.Adapter.Wallet.AdapterLogWallet;
import ir.kooleh.app.Utility.Class.InterClassDataPassKeys;
import ir.kooleh.app.Utility.Class.MessageHandlerUtils;
import ir.kooleh.app.Utility.Class.NumberUtils;
import ir.kooleh.app.View.Ui.Activity.SplashScreen.SplashActivity;
import ir.kooleh.app.View.Ui.Activity.general.PaymentActivity;
import ir.kooleh.app.View.Ui.Activity.general.ViewAllItemsActivity;
import ir.kooleh.app.View.Ui.Fragment.FragmentMessageHandler;
import ir.kooleh.app.View.ViewCustom.CEditText;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class WalletActivity extends AppCompatActivity
        implements View.OnClickListener,
        InterfaceWallet.Wallet.IntGetInfoWalletUser,
        InterfaceWallet.Wallet.IntGetLogsWalletUser,
        InterfaceMessageHandler.FragmentResult
{
    private CTextView tvTitleToolbar;
    private ImageView ivCloseActivity;
    private Toolbar toolbar;
    private BottomSheetDialog bottomSheetDialog;
    private CTextView tvWalletAmountWalletActivity;
    private CardView btnIncreaseDeposit;
    private RecyclerView rvLogWallet;
    private NestedScrollView nstActivityWallet;
    private CTextView tvEmptyListUserWallet;
    private CTextView tvViewAllItems;

    private FragmentMessageHandler fragmentMessageHandler;
    private FrameLayout containerFragmentMessageHandler;

    private ImageView ivCloseBottomSheet;
    private CTextView tvWalletAmountBottomSheet;
    private CTextView tvAdd50ThousandsBottomSheet, tvAdd100ThousandsBottomSheet, tvAdd200ThousandsBottomSheet;
    private CEditText etIncreaseWalletAmountBottomSheet;
    private CardView btnPayBottomSheet;

    private AdapterLogWallet adapterLogWallet;

    private ViewLoading viewLoading;
    private ApiWallet apiWallet;

    private final int nextPage = 1;
    private final int itemsPerPage = 5;


//    @Override
//    protected void onNewIntent(Intent intent)
//    {
//        super.onNewIntent(intent);
//        setIntent(intent);
//    }
//
//    @Override
//    protected void onResume()
//    {
//        super.onResume();
//        if (getIntent() != null)
//        {
//            Bundle data = getIntent().getExtras();
//            setIntent(null);
//            if (data != null)
//            {
//                String status = data.getString("status");
//                if (status.equals("ok"))
//                {
//                    setResult(1);
//                    viewLoading.show();
//                    apiWallet.getInfoWalletuser(this);
//                    resetLogWalletList();
//                }
//            }
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        setupBottomNavigation();
        initViews();
        initVars();
    }


    private void initViews()
    {
        toolbar = findViewById(R.id.toolbar_app);
        tvTitleToolbar = findViewById(R.id.tv_title_toolbar);
        ivCloseActivity = findViewById(R.id.iv_back_toolbar);
        tvWalletAmountWalletActivity = findViewById(R.id.tv_wallet_amount_wallet_activity);
        btnIncreaseDeposit = findViewById(R.id.btn_increase_deposit);
        rvLogWallet = findViewById(R.id.rv_log_wallet);
        nstActivityWallet = findViewById(R.id.nst_activity_wallet);
        containerFragmentMessageHandler = findViewById(R.id.container_message_handler_fragment);
        tvEmptyListUserWallet = findViewById(R.id.tv_empty_list_activity_wallet);
        tvViewAllItems = findViewById(R.id.tv_view_all_items);

        tvTitleToolbar.setText("کیف پول");
        tvTitleToolbar.setTextColor(getResources().getColor(R.color.gray_50));
        ivCloseActivity.setImageResource(R.drawable.ic_back_white);
        toolbar.setBackgroundColor(getResources().getColor(R.color.teal_300));

        ivCloseActivity.setOnClickListener(this);
        btnIncreaseDeposit.setOnClickListener(this);
        ivCloseBottomSheet.setOnClickListener(this);
        tvViewAllItems.setOnClickListener(this);
    }


    private void initVars()
    {
        adapterLogWallet = new AdapterLogWallet(this);
        rvLogWallet.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvLogWallet.setAdapter(adapterLogWallet);

        apiWallet = new ApiWallet(this);
        viewLoading = new ViewLoading(this);

        viewLoading.show();
        apiWallet.getLogsWalletUser(this, nextPage, itemsPerPage);
        apiWallet.getInfoWalletuser(this);
    }


    private void setupBottomNavigation() {
        bottomSheetDialog = new BottomSheetDialog(this, R.style.BottomSheetTransparent);
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_dialog_payment);

        ivCloseBottomSheet = bottomSheetDialog.findViewById(R.id.iv_close_bottom_sheet_payment);
        tvWalletAmountBottomSheet = bottomSheetDialog.findViewById(R.id.tv_wallet_amount_wallet_bottom_sheet);
        tvAdd50ThousandsBottomSheet = bottomSheetDialog.findViewById(R.id.tv_add_50_thousads_wallet_bottom_sheet);
        tvAdd100ThousandsBottomSheet = bottomSheetDialog.findViewById(R.id.tv_add_100_thousads_wallet_bottom_sheet);
        tvAdd200ThousandsBottomSheet = bottomSheetDialog.findViewById(R.id.tv_add_200_thousads_wallet_bottom_sheet);
        etIncreaseWalletAmountBottomSheet = bottomSheetDialog.findViewById(R.id.et_increase_wallet_amount_bottom_sheet);
        btnPayBottomSheet = bottomSheetDialog.findViewById(R.id.btn_pay_bottom_sheet);

        tvAdd50ThousandsBottomSheet.setOnClickListener(this);
        tvAdd100ThousandsBottomSheet.setOnClickListener(this);
        tvAdd200ThousandsBottomSheet.setOnClickListener(this);
        btnPayBottomSheet.setOnClickListener(this);

        etIncreaseWalletAmountBottomSheet.setOnFocusChangeListener((v, hasFocus) ->
        {
            if (!hasFocus) {
                hideKeyboard(v);
            }
        });

        bottomSheetDialog.setOnDismissListener(dialog -> {
            etIncreaseWalletAmountBottomSheet.clearFocus();
            etIncreaseWalletAmountBottomSheet.setText("");
        });
    }

    private void hideKeyboard(View view)
    {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void showError(String message)
    {
        fragmentMessageHandler = new FragmentMessageHandler(this);
        Bundle args = new Bundle();
        args.putString("message", message);
        args.putSerializable(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE, MessageHandlerUtils.getMessageTypeByMessage(message));
        fragmentMessageHandler.setArguments(args);
        getSupportFragmentManager().beginTransaction().replace(
                R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        containerFragmentMessageHandler.setVisibility(View.VISIBLE);
    }



    @Override
    public void onClick(View v)
    {
        if (v == ivCloseActivity)
        {
            if (isTaskRoot())
            {
                startActivity(new Intent(this, SplashActivity.class));
            }
            finish();
        }
        else if (v == btnIncreaseDeposit)
        {
            bottomSheetDialog.show();
        }
        else if (v == ivCloseBottomSheet)
        {
            bottomSheetDialog.dismiss();
        }
        else if (v == tvAdd50ThousandsBottomSheet)
        {
            etIncreaseWalletAmountBottomSheet.setText(tvAdd50ThousandsBottomSheet.getText().toString().replaceAll(",", ""));
            etIncreaseWalletAmountBottomSheet.requestFocus();
            etIncreaseWalletAmountBottomSheet.clearFocus();
        }
        else if (v == tvAdd100ThousandsBottomSheet)
        {
            etIncreaseWalletAmountBottomSheet.setText(tvAdd100ThousandsBottomSheet.getText().toString().replaceAll(",", ""));
            etIncreaseWalletAmountBottomSheet.requestFocus();
            etIncreaseWalletAmountBottomSheet.clearFocus();
        }
        else if (v == tvAdd200ThousandsBottomSheet)
        {
            etIncreaseWalletAmountBottomSheet.setText(tvAdd200ThousandsBottomSheet.getText().toString().replaceAll(",", ""));
            etIncreaseWalletAmountBottomSheet.requestFocus();
            etIncreaseWalletAmountBottomSheet.clearFocus();
        }
        else if (v == btnPayBottomSheet)
        {
            Intent intent = new Intent(this, PaymentActivity.class);
            intent.putExtra(PaymentActivity.KEY_PRICE_PAYMENT_ACTIVITY, etIncreaseWalletAmountBottomSheet.getText().toString().replaceAll(",", ""));
            intent.putExtra(PaymentActivity.KEY_TITLE_TOOLBAR_PAYMENT_ACTIVITY, "شارژ کیف پول");
            intent.putExtra(PaymentActivity.KEY_MODEL_PAYMENT_ACTIVITY, EnumWalletModule.BuyModels.MODEL_WALLET);
            startActivity(intent);
        }
        else if (v == tvViewAllItems)
        {
            Intent intent = new Intent(this, ViewAllItemsActivity.class);
            intent.putExtra(ViewAllItemsActivity.ITEMS_TYPE, ViewAllItemsActivity.ITEMS_TYPE_WALLET);
            intent.putExtra(ViewAllItemsActivity.TITLE_TOOLBAR, "تاریخچه کیف پول");
            startActivity(intent);
        }
    }


    @Override
    public void onResponseGetInfoWalletUser(boolean response, String message, ModelInfoWallet infoWallet)
    {
        viewLoading.close();
        nstActivityWallet.setVisibility(View.GONE);
        containerFragmentMessageHandler.setVisibility(View.GONE);
        if (response)
        {
            nstActivityWallet.setVisibility(View.VISIBLE);
            tvWalletAmountWalletActivity.setText(NumberUtils.digitDivider(String.valueOf(infoWallet.getAmount())) + " ریال");
            tvWalletAmountBottomSheet.setText(NumberUtils.digitDivider(String.valueOf(infoWallet.getAmount())) + " ریال");
        }
        else
        {
            showError(message);
        }
    }


    @Override
    public void onResponseGetLogsWalletUser(boolean response, String message, List<ModelLogWallet> logWalletList, ModelPaginate modelPaginate)
    {
        viewLoading.close();
        nstActivityWallet.setVisibility(View.GONE);
        containerFragmentMessageHandler.setVisibility(View.GONE);
        rvLogWallet.setVisibility(View.GONE);
        tvEmptyListUserWallet.setVisibility(View.GONE);
        if (response)
        {
            nstActivityWallet.setVisibility(View.VISIBLE);
            if (logWalletList.size() == 0)
            {
                tvEmptyListUserWallet.setVisibility(View.VISIBLE);
            }
            else
            {
                rvLogWallet.setVisibility(View.VISIBLE);
                adapterLogWallet.setList(logWalletList);
            }
        }
        else
        {
            showError(message);
        }
    }


    @Override
    public void onResponseMessageHandler()
    {
        getSupportFragmentManager().beginTransaction().remove(fragmentMessageHandler).commit();
        fragmentMessageHandler = null;
        viewLoading.show();
        apiWallet.getInfoWalletuser(this);
        apiWallet.getLogsWalletUser(this, nextPage, itemsPerPage);
    }

    @Override
    public void onBackPressed()
    {
        if (isTaskRoot())
        {
            startActivity(new Intent(this, SplashActivity.class));
            finish();
        }
        super.onBackPressed();
    }

    private void resetLogWalletList()
    {
        rvLogWallet.setAdapter(adapterLogWallet = new AdapterLogWallet(this));
        apiWallet.getLogsWalletUser(this, nextPage, itemsPerPage);
    }
}