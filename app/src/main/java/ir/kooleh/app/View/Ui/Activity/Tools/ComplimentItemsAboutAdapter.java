package ir.kooleh.app.View.Ui.Activity.Tools;

import android.animation.ObjectAnimator;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.github.aakira.expandablelayout.ExpandableLayout;
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.github.aakira.expandablelayout.Utils;


import java.util.List;

import ir.kooleh.app.R;

public class ComplimentItemsAboutAdapter extends RecyclerView.Adapter<ComplimentItemsAboutAdapter.AdapterHolder> {


    private Context context;
    private List<Compliment> list;
    public ComplimentItemsAboutAdapter(Context context , List<Compliment> list ) {

        this.context = context;
        this.list = list;
    }

    @Override
    public AdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final AdapterHolder viewHolder = new AdapterHolder(LayoutInflater.from(context).inflate(R.layout.item_about_service, parent , false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final AdapterHolder holder, int position) {
         final Compliment item = list.get(position);
        holder.setIsRecyclable(false);
        holder.txtTitle.setText(item.getTitle());
        holder.tvExplain.setText(item.getExplain());
        holder.expandableLayout.setInRecyclerView(true);
        holder.expandableLayout.setExpanded(holder.isExpand);
        holder.expandableLayout.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(holder.btnExpandLayout, 0f, 180f).start();
                holder.isExpand = true;
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(holder.btnExpandLayout, 180f, 0f).start();
                holder.isExpand =  false;
            }
        });
        holder.txtTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.expandableLayout.toggle();
            }
        });
        holder.btnExpandLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
               holder.expandableLayout.toggle();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void onClickButton(final ExpandableLayout expandableLayout) {
        expandableLayout.toggle();
    }

    public class AdapterHolder extends RecyclerView.ViewHolder {

        public TextView txtTitle;
        public TextView tvExplain;
        public RelativeLayout btnExpandLayout;
        public ExpandableLinearLayout expandableLayout;
        public boolean isExpand = false;

        public AdapterHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txt_expand_title);
            tvExplain = (TextView) itemView.findViewById(R.id.txt_expand_des);
            btnExpandLayout = (RelativeLayout) itemView.findViewById(R.id.toggle_expandable_layout);
            expandableLayout = (ExpandableLinearLayout) itemView.findViewById(R.id.expandable_layout);
        }
    }
    public ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
        return animator;
    }
}
