package ir.kooleh.app.View.Ui.Dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatRatingBar;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Enum.EnCustomMessage;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCustomCreateComment;
import ir.kooleh.app.View.ViewCustom.CEditText;


public class CustomCreateComment implements View.OnClickListener
{


    private AppCompatRatingBar ratingStarView;
    private AlertDialog alertDialog;
    private Button btnYes;
    private ImageView ivCloseCrateComment;
    private static CEditText etComment;

    private String strValue = "";


    private InterfaceCustomCreateComment interfaceCustomCreateComment;
    private int adID;

    public CustomCreateComment(Context context, InterfaceCustomCreateComment interfaceCustomCreateComment)
    {
        this.interfaceCustomCreateComment = interfaceCustomCreateComment;

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View inflater = LayoutInflater.from(context).inflate(R.layout.layout_custom_dialog_create_comment, null);
        builder.setView(inflater);
        alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alertDialog.setCancelable(true);
        initView(inflater);
    }


    private void initView(View view)
    {
        ratingStarView = view.findViewById(R.id.rsv_create_custom_comment);
        etComment = view.findViewById(R.id.et_create_custom_comment);
        btnYes = view.findViewById(R.id.btn_yes_custom_create_comment);
        ivCloseCrateComment = view.findViewById(R.id.iv_close_alert_crate_comment);
        setCancelable(false);

        etComment.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.toString().trim().length() >= 5)
                {
                    btnYes.setAlpha(1f);
                    btnYes.setClickable(true);
                }
                else
                {
                    btnYes.setAlpha(.5f);
                    btnYes.setClickable(false);
                }
            }
        });
        btnYes.setOnClickListener(this);
        ivCloseCrateComment.setOnClickListener(this);
        btnYes.setClickable(false);
        ratingStarView.setRating(1);
    }


    public void make(int adID)
    {
        this.adID = adID;
        alertDialog.show();
    }

    public void make(int adID, String strValue)
    {
        this.adID = adID;
        this.strValue = strValue;
        alertDialog.show();
    }

    public void setCancelable(boolean cancelable)
    {
        alertDialog.setCancelable(cancelable);

    }


    public void setVisibilityBtnYes(boolean show)
    {
        if (show)
            btnYes.setVisibility(View.VISIBLE);
        else
            btnYes.setVisibility(View.GONE);

    }


    public void dismiss()
    {
        if (alertDialog.isShowing())
        {
            alertDialog.dismiss();
            interfaceCustomCreateComment = null;
            ratingStarView = null;
            ivCloseCrateComment = null;
            etComment = null;
            btnYes = null;
            alertDialog = null;
        }
    }


    @Override
    public void onClick(View view)
    {

        if (interfaceCustomCreateComment != null)
        {
            if (view == btnYes)
            {
                if (!strValue.equals(""))
                {
                    interfaceCustomCreateComment.onClickCreateCommend(EnCustomMessage.YES, adID, strValue, etComment.getText() + "", ratingStarView.getRating());
                }
                else
                {
                    interfaceCustomCreateComment.onClickCreateCommend(EnCustomMessage.YES, adID, etComment.getText() + "", ratingStarView.getRating());
                }
            }
            else if (view == ivCloseCrateComment)
            {
                interfaceCustomCreateComment.onClickCreateCommend(EnCustomMessage.NO, -1, null, -1);
                dismiss();
            }
            else
            {
                interfaceCustomCreateComment.onClickCreateCommend(EnCustomMessage.NONE, -1, null, -1);
                dismiss();
            }
        }
    }

}
