package ir.kooleh.app.View.Ui.Activity.Tools;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import ir.kooleh.app.R;
import ir.kooleh.app.Utility.Volley.Controller;


public class CustomSnackbar  {

    public static Snackbar makeAction(Context context, View view, String message , String action, int duration, View.OnClickListener clickListener)
    {
        Snackbar snackbar = Snackbar.make(view,message,duration);
        View snackView = snackbar.getView();
        snackView.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        TextView tv = snackView.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTypeface(Controller.MyTypeface);
        Button btn =  snackView.findViewById(com.google.android.material.R.id.snackbar_action);
        btn.setTextColor(context.getResources().getColor(R.color.teal_400));
        btn.setTypeface(Controller.MyTypeface);
        snackbar.setAction(action,clickListener);

        return snackbar;
    }

    public static Snackbar make(Context context, View view, String message ,  int duration)
    {
        Snackbar snackbar = Snackbar.make(view,message,duration);
        View snackView = snackbar.getView();
        snackView.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        TextView tv = snackView.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTypeface(Controller.MyTypeface);

        return snackbar;
    }
}