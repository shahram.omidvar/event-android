package ir.kooleh.app.View.Ui.Dialog;

import android.app.FragmentManager;

import androidx.fragment.app.DialogFragment;

import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;

import com.mohamadamin.persianmaterialdatetimepicker.time.TimePickerDialog;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;

import java.util.Calendar;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Enum.EnTypeDateTime;
import ir.kooleh.app.Structure.Interface.Dialog.InterfaceDateTimePicker;


public class DateAndTimePickerDialog
{
    private final PersianCalendar pCalendar;
    Calendar calendar;

    private final InterfaceDateTimePicker interfaceDateTimePicker;
    private final FragmentManager fragmentManager;

    public DateAndTimePickerDialog(InterfaceDateTimePicker interfaceDateTimePicker, FragmentManager fragmentManager)
    {
        this.interfaceDateTimePicker = interfaceDateTimePicker;
        this.fragmentManager = fragmentManager;
        pCalendar = new PersianCalendar();

        calendar = Calendar.getInstance();
    }

    public void showDate()
    {
        String[] date = {""};

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance((view, year, monthOfYear, dayOfMonth) ->
        {
            monthOfYear++;
            date[0] = "" + year + "/" + (monthOfYear < 10 ? "0" : "") + monthOfYear + "/" + (dayOfMonth < 10 ? "0" : "") + dayOfMonth;
            interfaceDateTimePicker.onResponseDateTime(EnTypeDateTime.DATE, date[0]);

        }, pCalendar.getPersianYear(), pCalendar.getPersianMonth(), pCalendar.getPersianDay());
        datePickerDialog.setStyle(DialogFragment.STYLE_NORMAL, R.style.datePickerDialog);

        datePickerDialog.show(fragmentManager, "");
    }

    public void showTime()
    {
        String[] time = {""};

        PersianCalendar now = new PersianCalendar();
        TimePickerDialog tpd = TimePickerDialog.newInstance((view, hourOfDay, minute) ->
        {

            time[0] = (minute < 10 ? "0" : "") + minute + " : " + (hourOfDay < 10 ? "0" : "") + hourOfDay;
            interfaceDateTimePicker.onResponseDateTime(EnTypeDateTime.TIME, time[0]);
        }, now.get(PersianCalendar.HOUR_OF_DAY), now.get(PersianCalendar.MINUTE), true);

        tpd.show(fragmentManager, "");

    }

}
