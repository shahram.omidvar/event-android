package ir.kooleh.app.View.Ui.Dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Enum.EnCustomMessage;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCustomMessage;


public class CustomDialogMessage implements View.OnClickListener
{
    private AlertDialog alertDialog;

    private CardView btnYes, btnNo;
    private TextView tvTitle, tvExplain;

    private InterfaceCustomMessage interfaceCustomMessage;

    public CustomDialogMessage makeMessage(Context context, String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View inflater = LayoutInflater.from(context).inflate(R.layout.layout_custom_message, null);
        builder.setView(inflater);
        alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        initView(inflater);
        setTitle(message);

        return this;
    }


    public CustomDialogMessage makeMessage(Context context, InterfaceCustomMessage interfaceCustomMessage)
    {
        this.interfaceCustomMessage = interfaceCustomMessage;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View inflater = LayoutInflater.from(context).inflate(R.layout.layout_custom_message, null);
        builder.setView(inflater);
        alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        initView(inflater);

        return this;
    }


    private void initView(View view)
    {
        btnYes = view.findViewById(R.id.btn_yes_custom_message);
        btnNo = view.findViewById(R.id.btn_no_custom_message);
        tvTitle = view.findViewById(R.id.tv_title_custom_message);
        tvExplain = view.findViewById(R.id.tv_explain_custom_message);

        tvExplain.setVisibility(View.GONE);

        btnNo.setOnClickListener(this);
        btnYes.setOnClickListener(this);
    }

    public CustomDialogMessage setTitle(String title)
    {
        tvTitle.setText(title);
        return this;
    }


    public CustomDialogMessage setCancelable(boolean close)
    {
        alertDialog.setCancelable(close);
        return this;
    }

    public CustomDialogMessage setExplain(String explain)
    {
        tvExplain.setVisibility(View.VISIBLE);
        tvExplain.setText(explain);
        return this;
    }

    public CustomDialogMessage setVisibilityBtnYes(boolean show)
    {
        if (show)
            btnYes.setVisibility(View.VISIBLE);
        else
        {
            btnYes.setVisibility(View.GONE);
        }
        return this;
    }

    public CustomDialogMessage setVisibilityBtnNo(boolean show)
    {
        if (show)
            btnNo.setVisibility(View.VISIBLE);
        else
        {
            btnNo.setVisibility(View.GONE);
        }
        return this;
    }

    public void show()
    {
        if (alertDialog != null)
        {
            dismiss();
            alertDialog.show();
        }
    }

    public void dismiss()
    {
        if (alertDialog.isShowing())
        {
            alertDialog.dismiss();
            alertDialog = null;
            btnNo = null;
            btnYes = null;
            tvTitle = null;
            interfaceCustomMessage = null;
        }
    }

    @Override
    public void onClick(View view)
    {
        if (interfaceCustomMessage != null)
        {
            if (view == btnYes)
            {
                interfaceCustomMessage.onClickCustomDialog(EnCustomMessage.YES);
            }
            else if (view == btnNo)
            {
                interfaceCustomMessage.onClickCustomDialog(EnCustomMessage.NO);
            }
            else
            {
                interfaceCustomMessage.onClickCustomDialog(EnCustomMessage.NONE);
            }
            dismiss();
        }
    }
}
