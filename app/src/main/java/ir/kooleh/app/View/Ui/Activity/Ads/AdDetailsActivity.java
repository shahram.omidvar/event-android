package ir.kooleh.app.View.Ui.Activity.Ads;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager2.widget.ViewPager2;

import android.animation.ArgbEvaluator;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Ads.ApiAds;
import ir.kooleh.app.Server.Api.Ads.ApiAuction;
import ir.kooleh.app.Server.Api.Dashboard.ApiAdDetails;
import ir.kooleh.app.Server.Base.BaseServer;
import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Enum.EnumBtnAdDetails;
import ir.kooleh.app.Structure.Enum.EnumWalletModule;
import ir.kooleh.app.Structure.Interface.Ads.InterfaceAds;
import ir.kooleh.app.Structure.Interface.Custom.FragmentCommunicator;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceBottomSheetCreateOffer;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceBottomSheetPaymentAd;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCustomDialogSetActionAd;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCustomDialogUpdateStepAd;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceSlider;
import ir.kooleh.app.Structure.Interface.Dashboard.InterfaceDashboard;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Model.Ads.ModelAd;
import ir.kooleh.app.Structure.Model.Ads.ModelAuctionOffer;
import ir.kooleh.app.Utility.Adapter.Ads.Auction.AdapterAuctionOffers;
import ir.kooleh.app.Utility.Adapter.Ads.Details.AdapterAdDetails;
import ir.kooleh.app.Utility.Adapter.Ads.Details.AdapterAdDetailsSpecial;
import ir.kooleh.app.Utility.Adapter.Ads.Details.AdapterSliderAdDetails;
import ir.kooleh.app.Utility.Class.AdDetailsUtils;
import ir.kooleh.app.Utility.Class.AnimationUtils;
import ir.kooleh.app.Utility.Class.NumberUtils;
import ir.kooleh.app.View.Ui.Activity.dashboard.AdDetailsChooseWinnerActivity;
import ir.kooleh.app.View.Ui.Dialog.BottomSheetDialogCreateOffer;
import ir.kooleh.app.View.Ui.Dialog.BottomSheetDialogPayment;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.View.Ui.Dialog.CustomDialogSetActionAd;
import ir.kooleh.app.View.Ui.Dialog.CustomDialogUpdateStepAd;
import ir.kooleh.app.View.Ui.Fragment.FragmentMessageHandler;
import ir.kooleh.app.View.Ui.Fragment.FragmentSlider;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class AdDetailsActivity extends AppCompatActivity implements View.OnClickListener,
        InterfaceSlider, InterfaceAds.IInfo,
        InterfaceMessageHandler.FragmentResult,
        InterfaceAds.AuctionAd.intApiGetOfferList,
        InterfaceCustomDialogSetActionAd,
        InterfaceCustomDialogUpdateStepAd,
        InterfaceDashboard.Apis.IntSetActionAd,
        InterfaceDashboard.Apis.IntUpdateStepAd,
        InterfaceDashboard.Apis.IntShowInfoAuctionOwner,
        InterfaceBottomSheetPaymentAd,
        FragmentCommunicator,
        InterfaceBottomSheetCreateOffer
{

    // view
    private SwipeRefreshLayout swipeRefreshLayout;
    private RelativeLayout toolbar;
    private TextView tvTitleToolbar;
    private ImageView ivBackAdDetails;
    private NestedScrollView nstAdsDetailsActivity;
    private LinearLayout lnrAdsDetailsActivity;
    private FrameLayout fragmentContainer;
    private RecyclerView rvSpecialAds, rvItemAds;
    private CTextView tvAdTitle, tvAdTime, tvAdDescription;
    private FragmentMessageHandler fragmentMessageHandler;
    private RecyclerView rvAuctionOffers;
    private CardView containerRvAuctionOffers;
    private FrameLayout containerMessageHandler;
    private CTextView tvCountdownTimer;
    private ImageView imgNoPic;
    private CTextView tvNoOfferOfferlist;
    private ViewPager2 viewPager2Slider;
    private DotsIndicator dotsIndicator;

    private LinearLayout containerBtn;
    private CardView btnOptionOne, btnOptionTwo;
    private CTextView tvBtnOptionOne, tvBtnOptionTwo;
    private ImageView imgBtnOptionOne, imgBtnOptionTwo;


    // var
    private AdapterAdDetails adapterAdsDetailsItem;
    private AdapterAdDetailsSpecial adapterAdsDetailsSpecial;
    private AdapterAuctionOffers adapterAuctionOffers;
    private AdapterSliderAdDetails adapterSliderAdDetails;
    private ApiAds apiAds;
    private ApiAdDetails apiAdDetails;
    private ViewLoading viewLoading;
    private CustomToast customToast;
    private CustomDialogSetActionAd customDialogSetActionAd;
    private CustomDialogUpdateStepAd customDialogUpdateStepAd;
    BottomSheetDialogCreateOffer bottomSheetDialogCreateOffer;
    private ApiAuction apiAuction;


    public static final String Ad_ID = "ad_id";
    public static final String ADS_TITLE = "ads_title";
    public static final String API_NAME = "api_name";
    public static final String MY_AD = "my_ad";
    public static final String AUCTION_AD_TYPE = "auction_ad";

    public static final String IS_MY_AD = "is_my_ad";
    public static final String NOT_MY_AD = "not_my_ad";


    private String adID = "";
    public String phoneNumber = "-1";
    private String apiName;

    private ModelAd modelAd;

    private CountDownTimer countDownTimer;


//    @Override
//    protected void onNewIntent(Intent intent)
//    {
//        super.onNewIntent(intent);
//        setIntent(intent);
//    }
//
//    @Override
//    protected void onResume()
//    {
//        super.onResume();
//        if (getIntent() != null)
//        {
//            Bundle data = getIntent().getExtras();
//            setIntent(null);
//            if (data != null)
//            {
//                String status = data.getString("status");
//                if (status != null)
//                {
//                    if (status.equals("ok"))
//                    {
//                        getAdInfo();
//                    }
//                }
//            }
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_details);

        initViews();
        initVars();
        getIntentData();
    }


    private void initViews()
    {
        swipeRefreshLayout = findViewById(R.id.swipe_ad_details);
        fragmentContainer = findViewById(R.id.contaienr_fragment);
        lnrAdsDetailsActivity = findViewById(R.id.lnr_ads_details);
        rvItemAds = findViewById(R.id.rv_ad_items);
        rvSpecialAds = findViewById(R.id.rv_special_ad_items);
        viewPager2Slider = findViewById(R.id.viewpater2_slider_ad_details);
        dotsIndicator = findViewById(R.id.dots_indicator_ad_details);
        containerMessageHandler = findViewById(R.id.container_message_handler_fragment);

        toolbar = findViewById(R.id.toolbar_slider);
        ivBackAdDetails = findViewById(R.id.iv_back_toolbar_slider);
        tvTitleToolbar = findViewById(R.id.tv_title_toolbar_slider);
        tvAdTitle = findViewById(R.id.tv_ad_title);
        tvAdTime = findViewById(R.id.tv_ad_time);
        tvAdDescription = findViewById(R.id.tv_ad_description);
        nstAdsDetailsActivity = findViewById(R.id.nested_ads_details);
        rvAuctionOffers = findViewById(R.id.rv_auction_offer);
        containerRvAuctionOffers = findViewById(R.id.container_rv_auction_offer);
        tvCountdownTimer = findViewById(R.id.tv_countdown_timer_ad_details);
        imgNoPic = findViewById(R.id.img_default_pic_ad_details);
        tvNoOfferOfferlist = findViewById(R.id.tv_no_offer_offerlist_ad_details);

        containerBtn = findViewById(R.id.container_btn_ad_details);
        btnOptionOne = findViewById(R.id.btn_option_one_ad_details);
        tvBtnOptionOne = findViewById(R.id.tv_btn_option_one_ad_details);
        imgBtnOptionOne = findViewById(R.id.img_btn_option_one_ad_details);

        btnOptionTwo = findViewById(R.id.btn_option_two_ad_details);
        tvBtnOptionTwo = findViewById(R.id.tv_btn_option_two_ad_details);
        imgBtnOptionTwo = findViewById(R.id.img_btn_option_two_ad_details);

        btnOptionOne.setOnClickListener(this);
        btnOptionTwo.setOnClickListener(this);
        ivBackAdDetails.setOnClickListener(this);

        tvAdTitle.setSelected(true);

        btnOptionOne.setVisibility(View.GONE);
        btnOptionTwo.setVisibility(View.GONE);

        swipeRefreshLayout.setOnRefreshListener(this::getAdInfo);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            nstAdsDetailsActivity.setOnScrollChangeListener((View.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) ->
            {
                Rect scrollBounds = new Rect();
                nstAdsDetailsActivity.getHitRect(scrollBounds);

                if (viewPager2Slider.getLocalVisibleRect(scrollBounds) && tvTitleToolbar.getVisibility() == View.VISIBLE)
                {
                    AnimationUtils.alphaAnimation(tvTitleToolbar, 200, 1, 0, View.INVISIBLE);
                    AnimationUtils.objectAnimatorOfFloat(toolbar, "elevation", 50, 0);
                    AnimationUtils.objectAnimatorOfObject(toolbar, "backgroundColor", new ArgbEvaluator(),
                            200, 0xffF8F8F8, 0x55ffffff);
                }
                else if (!viewPager2Slider.getLocalVisibleRect(scrollBounds) && tvTitleToolbar.getVisibility() == View.INVISIBLE)
                {
                    AnimationUtils.alphaAnimation(tvTitleToolbar, 200, 0, 1, View.VISIBLE);
                    AnimationUtils.objectAnimatorOfFloat(toolbar, "elevation", 50, 40);
                    AnimationUtils.objectAnimatorOfObject(toolbar, "backgroundColor", new ArgbEvaluator(),
                            200, 0x55ffffff, 0xffF8F8F8);
                }
            });
        }
    }


    private void initVars()
    {
        apiAds = new ApiAds(this);
        viewLoading = new ViewLoading(this);
        apiAuction = new ApiAuction(this);
        apiAdDetails = new ApiAdDetails(this);

        rvSpecialAds.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvSpecialAds.setAdapter(adapterAdsDetailsSpecial = new AdapterAdDetailsSpecial(this));

        rvItemAds.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvItemAds.setAdapter(adapterAdsDetailsItem = new AdapterAdDetails(this));
    }



    private void getIntentData()
    {
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        adID = bundle.getString(Ad_ID, "");
        String titleAd = bundle.getString(ADS_TITLE, "-1");
        apiName = bundle.getString(API_NAME, UrlAPI.GET_AD);
        if (adID.equals(""))
        {
            customToast = new CustomToast(this);
            customToast.showError(getString(R.string.error_send_data_intent));
            finish();
        }
        else if (titleAd.equals("-1"))
        {
            customToast = new CustomToast(this);
            customToast.showError(getString(R.string.error_send_data_intent));
            finish();
        }
        else
        {
            tvTitleToolbar.setText(String.format("%s", titleAd));
            getAdInfo();
        }

    }

    private void getAdInfo()
    {
        Log.i("addetailsbitch", "getAdInfo: ");
        if (bottomSheetDialogCreateOffer != null)
        {
            bottomSheetDialogCreateOffer.dismiss();
        }
        if (customDialogSetActionAd != null)
        {
            customDialogSetActionAd.dismiss();
        }
        if (customDialogUpdateStepAd != null)
        {
            customDialogUpdateStepAd.dismiss();
        }
        if (countDownTimer != null)
        {
            countDownTimer.cancel();
            countDownTimer = null;
        }
        tvCountdownTimer.setText("00:00:00");
        viewLoading.show();
        apiAds.getAd(this, adID, apiName);
    }


    @Override
    public void onResponseGetAds(boolean response, ModelAd modelAd, String message)
    {
        viewLoading.close();
        Log.i("addetailsbitch", "onResponseGetAds: ");
        lnrAdsDetailsActivity.setVisibility(View.GONE);
        containerBtn.setVisibility(View.GONE);
        containerMessageHandler.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
        if (response)
        {
            lnrAdsDetailsActivity.setVisibility(View.VISIBLE);
            containerBtn.setVisibility(View.VISIBLE);
            swipeRefreshLayout.setRefreshing(false);
            this.modelAd = modelAd;
            setAdsInfo(modelAd);
            setBtns(modelAd.getBtnAdList());
            if (modelAd.getBaseData().getTypeAd().equals(AUCTION_AD_TYPE))
            {
                handleAuctionAd();
            }
        }
        else
        {
            if (!this.isFinishing())
            {
                containerMessageHandler.setVisibility(View.VISIBLE);
                fragmentMessageHandler = new FragmentMessageHandler(this);
                Bundle args = new Bundle();
                args.putString("message", message);
                fragmentMessageHandler.setArguments(args);
                getSupportFragmentManager().beginTransaction().replace(
                        R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
            }
        }
    }


    private void setAdsInfo(ModelAd adsInfo)
    {
        tvAdTitle.setText(adsInfo.getBaseData().getTitleAd());
        tvAdTime.setText(adsInfo.getBaseData().getDateCreateAd());
        tvAdDescription.setText(adsInfo.getBaseData().getExplainAd());

        ModelAd.ListDataAd listDataAd = modelAd.getListDataAd();
        rvItemAds.setVisibility(View.GONE);
        rvSpecialAds.setVisibility(View.GONE);
        if (listDataAd != null)
        {
            if (listDataAd.getItemsAd().size() > 0)
            {
                adapterAdsDetailsItem.setListItemAds(listDataAd.getItemsAd());
                rvItemAds.setVisibility(View.VISIBLE);
            }

            if (listDataAd.getSpecialsAd().size() > 0)
            {
                adapterAdsDetailsSpecial.setListSpecialAds(listDataAd.getSpecialsAd());
                rvSpecialAds.setVisibility(View.VISIBLE);
            }
        }

        if (adsInfo.getSlider() != null && adsInfo.getSlider().size() >= 1)
        {
            adapterSliderAdDetails = new AdapterSliderAdDetails(this, adsInfo.getSlider());
            viewPager2Slider.setAdapter(adapterSliderAdDetails);
            dotsIndicator.setViewPager2(viewPager2Slider);
            viewPager2Slider.setVisibility(View.VISIBLE);
        }
        else
        {
            imgNoPic.setVisibility(View.VISIBLE);
        }
    }


    private void setBtns(List<ModelAd.BtnAd> btnAdList)
    {
        if (btnAdList.size() >= 1)
        {
            containerBtn.setVisibility(View.VISIBLE);
            btnOptionOne.setVisibility(View.VISIBLE);
            tvBtnOptionOne.setText(btnAdList.get(0).getFaName());
            tvBtnOptionOne.setTextColor(Color.parseColor(btnAdList.get(0).getTextColor()));
            btnOptionOne.setCardBackgroundColor(Color.parseColor(btnAdList.get(0).getBackColor()));
            Glide.with(this).load(btnAdList.get(0).getIcon()).into(imgBtnOptionOne);
        }
        else
        {
            containerBtn.setVisibility(View.GONE);
        }

        if (btnAdList.size() >= 2)
        {
            btnOptionTwo.setVisibility(View.VISIBLE);
            btnOptionTwo.setCardBackgroundColor(Color.parseColor(btnAdList.get(1).getBackColor()));
            tvBtnOptionTwo.setText(btnAdList.get(1).getFaName());
            tvBtnOptionTwo.setTextColor(Color.parseColor(btnAdList.get(1).getTextColor()));
            Glide.with(this).load(btnAdList.get(1).getIcon()).into(imgBtnOptionTwo);
        }
    }

    @Override
    public void onClickedSlider(int currentItemPos)
    {
        if (modelAd != null && modelAd.getSlider() != null)
        {
            fragmentContainer.setVisibility(View.VISIBLE);
            getSupportFragmentManager().beginTransaction().replace(R.id.contaienr_fragment,
                    new FragmentSlider(this, modelAd.getSlider(), currentItemPos)).addToBackStack("fragmentSlider").commit();
        }
    }


    @Override
    public void onClosedSlider()
    {
        fragmentContainer.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v)
    {
        if (v == btnOptionOne)
        {
            String action = AdDetailsUtils.getBtnAction(modelAd.getBtnAdList().get(0).getAction(), modelAd.getBtnAdList().get(0).getValue());
            setBtnApi(action);
        }
        else if (v == btnOptionTwo)
        {
            String action = AdDetailsUtils.getBtnAction(modelAd.getBtnAdList().get(1).getAction(), modelAd.getBtnAdList().get(1).getValue());
            setBtnApi(action);
        }
        else if (v == ivBackAdDetails)
        {
            finish();
        }

    }


    private void setBtnApi(String action)
    {
        String strKey, strValue = "";
        if (action.contains("#"))
        {
            strKey = action.split("#")[0];
            strValue = action.split("#")[1];
        }
        else
        {
            strKey = action;
        }

        switch (strKey)
        {
            case EnumBtnAdDetails.ActionType.DIALOG:
                bottomSheetDialogCreateOffer = new BottomSheetDialogCreateOffer(this, modelAd.getBaseData().getId());
                bottomSheetDialogCreateOffer.show(getSupportFragmentManager(), "bottomSheetCreateAd");
                return;
            case EnumBtnAdDetails.ActionCallValueType.CALL:
                startActivities(new Intent[] {new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:" + strValue))});
                return;
            case EnumBtnAdDetails.ActionAPIValueType.SET_ACTION_AD:
                customDialogSetActionAd = new CustomDialogSetActionAd();
                customDialogSetActionAd.make(this, strValue);
                customDialogSetActionAd.show();
                return;
            case EnumBtnAdDetails.ActionAPIValueType.UPDATE_STEP_AD:
                customDialogUpdateStepAd = new CustomDialogUpdateStepAd();
                customDialogUpdateStepAd.make(this, strValue);
                customDialogUpdateStepAd.show();
                return;
            case EnumBtnAdDetails.ActionType.OPEN_ACTIVITY:
                viewLoading.show();
                apiAdDetails.showInfoAuctionOwner(this, modelAd.getBaseData().getId(), BaseServer.ApiV1 + strValue);
                break;
        }
    }


    @Override
    public void onResponseMessageHandler()
    {
        getSupportFragmentManager().beginTransaction().remove(fragmentMessageHandler).commit();
        fragmentMessageHandler = null;
        getAdInfo();
    }


    public void handleAuctionAd()
    {
        if (
                (modelAd.getBaseData().getIsMyAd() != null
                        && modelAd.getBaseData().getIsMyAd().equals("true")
                        && modelAd.getBaseData().getCountDown().equals("انتخاب برنده"))
        )
        {
            Intent intent = new Intent(this, AdDetailsChooseWinnerActivity.class);
            intent.putExtra(AdDetailsChooseWinnerActivity.Ad_ID, modelAd.getBaseData().getId());
            intent.putExtra(AdDetailsChooseWinnerActivity.ADS_TITLE, modelAd.getBaseData().getTitleAd());
            intent.putExtra(AdDetailsActivity.API_NAME, UrlAPI.GET_MY_AD);
            startActivity(intent);
            setResult(2);
            finish();
        }
        containerRvAuctionOffers.setVisibility(View.VISIBLE);
        rvAuctionOffers.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        adapterAuctionOffers = new AdapterAuctionOffers(this, new ArrayList<>());
        rvAuctionOffers.setAdapter(adapterAuctionOffers);

        apiAuction.getOfferList(this, modelAd.getBaseData().getId());

        if (NumberUtils.isNumeric(modelAd.getBaseData().getCountDown()) && !modelAd.getBaseData().getCountDown().equals("0"))
        {
            tvCountdownTimer.setVisibility(View.VISIBLE);
            countDownTimer = new CountDownTimer(Integer.parseInt(modelAd.getBaseData().getCountDown()) * 1000L, 1000) {
                public void onTick(long millisUntilFinished) {
                    long seconds = millisUntilFinished / 1000;
                    long minutes = seconds / 60;
                    long hours = minutes / 60;
                    String strCountdownTime = String.format(Locale.ENGLISH, "%02d", hours % 24)
                            + ":" + String.format(Locale.ENGLISH, "%02d", minutes % 60)
                            + ":" + String.format(Locale.ENGLISH, "%02d", seconds % 60);
                    tvCountdownTimer.setText(strCountdownTime);
                }
                public void onFinish()
                {
                    getAdInfo();
                    tvCountdownTimer.setText("00:00:00");
                }
            }.start();
        }
    }


    @Override
    public void onResponseGetOfferList(boolean response, List<ModelAuctionOffer> auctionOfferList, String message)
    {
        rvAuctionOffers.setVisibility(View.GONE);
        tvNoOfferOfferlist.setVisibility(View.GONE);
        viewLoading.close();
        if (response)
        {
            if (auctionOfferList.size() == 0)
            {
                tvNoOfferOfferlist.setVisibility(View.VISIBLE);
            }
            else
            {
                rvAuctionOffers.setVisibility(View.VISIBLE);
                adapterAuctionOffers.setList(auctionOfferList);
            }
        }
    }


    @Override
    public void onResponseDialogSetActionAd(String explain, String command)
    {
        viewLoading.show();
        apiAdDetails.setActionAd(this, modelAd.getBaseData().getId(), explain, command);
    }

    @Override
    public void onResponseSetActionAd(boolean response, String message)
    {
        if (response)
        {
            customToast = new CustomToast(this);
            customToast.showSuccess(message);
            customDialogSetActionAd.dismiss();
            customDialogSetActionAd = null;
            apiAds.getAd(this, modelAd.getBaseData().getId(), apiName);
            setResult(1);
        }
        else
        {
            viewLoading.close();
            customToast = new CustomToast(this);
            customToast.showError(message);
        }
    }

    @Override
    public void onResponseDialogUpdateStepAd(String step, String actionID, String explain)
    {
        viewLoading.show();
        apiAdDetails.updateStepAd(this, modelAd.getBaseData().getId(), step, actionID, explain);
    }

    @Override
    public void onResponseUpdateStepAd(boolean response, String message)
    {
        viewLoading.close();
        if (response)
        {
            customToast = new CustomToast(this);
            customToast.showSuccess(message);
            customDialogUpdateStepAd.dismiss();
            customDialogUpdateStepAd = null;
            Intent returnIntent = new Intent();
            setResult(1, returnIntent);
            finish();
        }
        else
        {
            customToast = new CustomToast(this);
            customToast.showError(message);
        }
    }

    @Override
    public void onResponseShowInfoAuctionOwnerPayment(boolean response, String extraMessage, String price, String message)
    {
        viewLoading.close();
        if (response)
        {
            BottomSheetDialogPayment bottomSheetDialogPayment;
            bottomSheetDialogPayment = new BottomSheetDialogPayment(this, EnumWalletModule.BuyModels.MODEL_AD,
                    extraMessage, String.valueOf((long)Double.parseDouble(price)), modelAd.getBaseData().getId());
            bottomSheetDialogPayment.show(getSupportFragmentManager(), "bottomSheetPayment");
        }
        else
        {
            customToast = new CustomToast(this);
            customToast.showError(message);
        }
    }

    @Override
    public void onResponseShowInfoAuctionOwnerShow(boolean response, ModelAd modelAd, String message)
    {
        viewLoading.close();
        nstAdsDetailsActivity.setVisibility(View.GONE);
        if (response)
        {
            nstAdsDetailsActivity.setVisibility(View.VISIBLE);
            this.modelAd = modelAd;
            setAdsInfo(modelAd);
            setBtns(modelAd.getBtnAdList());
            if (modelAd.getBaseData().getTypeAd().equals(AUCTION_AD_TYPE))
            {
                handleAuctionAd();
            }
        }

        else
        {
            fragmentMessageHandler = new FragmentMessageHandler(this);
            Bundle args = new Bundle();
            args.putString("message", message);
            fragmentMessageHandler.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        }
    }

    @Override
    public void onResponseBottomSheetPaymentAd()
    {
        viewLoading.show();
        apiAds.getAd(this, adID, apiName);
    }

    @Override
    protected void onDestroy()
    {

        super.onDestroy();
    }

    @Override
    public void fragmentDetached()
    {
        fragmentContainer.setVisibility(View.GONE);
    }

    @Override
    public void onResponseBottomSheetCreateOffer()
    {
        viewLoading.show();
        apiAds.getAd(this, adID, apiName);
    }
}