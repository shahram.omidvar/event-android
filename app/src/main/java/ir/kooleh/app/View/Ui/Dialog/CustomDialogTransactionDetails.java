package ir.kooleh.app.View.Ui.Dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Model.Wallet.ModelTransactionDetails;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class CustomDialogTransactionDetails implements
        View.OnClickListener
{
    public AlertDialog dialog;
    private ImageView ivCloseDialog;
    private LinearLayout lnrContainerTvs;
    private CTextView tvId, tvBuyID, tvGatewayName, tvGatewayMessage, tvStatusMessage, tvCode, tvNumberCart, tvTimeBuy, tvDateCreate;
    private CTextView tvExplain, tvOtherData;
    private ModelTransactionDetails modelTransactionDetails;

    public CustomDialogTransactionDetails(ModelTransactionDetails modelTransactionDetails)
    {
        this.modelTransactionDetails= modelTransactionDetails;
    }

    public void make(Context context)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View inflater = LayoutInflater.from(context).inflate(R.layout.custom_dialog_transaction_details, null);
        builder.setView(inflater);
        dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setDimAmount(0.8f);

        initView(inflater);

        dialog.setOnKeyListener((dialog, keyCode, event) ->
        {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP && !event.isCanceled())
            {
                dismiss();
                return true;
            }
            return false;
        });

        dialog.setCanceledOnTouchOutside(false);
    }



    private void initView(View view)
    {
        ivCloseDialog = view.findViewById(R.id.iv_close_dialog_payment_method);
        lnrContainerTvs = view.findViewById(R.id.lnr_container_tvs_transaction_details);
        tvId = view.findViewById(R.id.tv_id_transaction_details);
        tvBuyID = view.findViewById(R.id.tv_buy_id_transaction_details);
        tvGatewayName = view.findViewById(R.id.tv_gateway_name_transaction_details);
        tvGatewayMessage = view.findViewById(R.id.tv_gateway_message_transaction_details);
        tvStatusMessage = view.findViewById(R.id.tv_status_message_transaction_details);
        tvCode = view.findViewById(R.id.tv_code_transaction_details);
        tvNumberCart = view.findViewById(R.id.tv_number_cart_transaction_details);
        tvTimeBuy = view.findViewById(R.id.tv_time_buy_transaction_details);
        tvDateCreate = view.findViewById(R.id.tv_data_create_transaction_details);
        tvExplain = view.findViewById(R.id.tv_explain_transaction_details);
        tvOtherData = view.findViewById(R.id.tv_other_data_transaction_details);


        if (modelTransactionDetails != null)
        {
            if (modelTransactionDetails.getId() != null)
                tvId.setText(modelTransactionDetails.getId());
            else
                tvId.setVisibility(View.GONE);

            if (modelTransactionDetails.getBuyId() != null)
                tvBuyID.setText(modelTransactionDetails.getBuyId());
            else
                tvBuyID.setVisibility(View.GONE);

            if (modelTransactionDetails.getGatewayName() != null)
                tvGatewayName.setText(modelTransactionDetails.getGatewayName());
            else
                tvGatewayName.setVisibility(View.GONE);

            if (modelTransactionDetails.getGatewayMessage() != null)
                tvGatewayMessage.setText(modelTransactionDetails.getGatewayMessage());
            else
                tvGatewayMessage.setVisibility(View.GONE);

            if (modelTransactionDetails.getStatusMessage() != null)
                tvStatusMessage.setText(modelTransactionDetails.getStatusMessage());
            else
                tvStatusMessage.setVisibility(View.GONE);

            if (modelTransactionDetails.getCode() != null)
                tvCode.setText(modelTransactionDetails.getCode());
            else
                tvCode.setVisibility(View.GONE);

            if (modelTransactionDetails.getNumberCart() != null)
                tvNumberCart.setText(modelTransactionDetails.getNumberCart());
            else
                tvNumberCart.setVisibility(View.GONE);

            if (modelTransactionDetails.getTimeBuy() != null)
                tvTimeBuy.setText(modelTransactionDetails.getTimeBuy());
            else
                tvTimeBuy.setVisibility(View.GONE);

            if (modelTransactionDetails.getDateCreate() != null)
                tvDateCreate.setText(modelTransactionDetails.getDateCreate());
            else
                tvDateCreate.setVisibility(View.GONE);

            if (modelTransactionDetails.getExplain() != null)
                tvExplain.setText(modelTransactionDetails.getExplain());
            else
                tvExplain.setVisibility(View.GONE);

            if (modelTransactionDetails.getOtherData() != null)
                tvOtherData.setText(modelTransactionDetails.getOtherData());
            else
                tvOtherData.setVisibility(View.GONE);
        }


        int nextTvBackgroundColor = R.color.gray_50;
        for (int i = 0; i < lnrContainerTvs.getChildCount(); i++) {
            View v = lnrContainerTvs.getChildAt(i);
            if (v.getVisibility() != View.GONE) {
                v.setBackgroundResource(nextTvBackgroundColor);
                if (nextTvBackgroundColor == R.color.gray_50)
                    nextTvBackgroundColor = R.color.white;
                else
                    nextTvBackgroundColor = R.color.gray_50;
            }
        }


        ivCloseDialog.setOnClickListener(this);
    }



    public void show()
    {
        if (dialog != null)
        {
            dismiss();
            dialog.show();
        }
    }


    public void dismiss()
    {
        if (dialog.isShowing())
        {
            dialog.dismiss();
            modelTransactionDetails = null;
            ivCloseDialog = null;
            tvId = null;
            tvBuyID = null;
            tvGatewayName = null;
            tvGatewayMessage = null;
            tvStatusMessage = null;
            tvCode = null;
            tvTimeBuy = null;
            tvNumberCart = null;
            tvDateCreate = null;
            tvExplain = null;
            tvOtherData = null;
            dialog = null;
        }
    }

    @Override
    public void onClick(View view)
    {
        if (view == ivCloseDialog)
        {
            dismiss();
        }
    }
}
