package ir.kooleh.app.View.Ui.Activity.Tools;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;


public class ServicePagerAdapter extends FragmentStatePagerAdapter {


    private List<Fragment> fragments;
    private List<String> title;

    public ServicePagerAdapter(FragmentManager fm ,List<Fragment> fragments,List<String> title) {
        super(fm);
        this.fragments = fragments;
        this.title = title;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return fragments.get(0);
            case 1:
                return fragments.get(1);
            case 2:
                return fragments.get(2);
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return title.get(0);
            case 1:
                return title.get(1);
            case 2:
                return title.get(2);
            default:
                return "";
        }

    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
