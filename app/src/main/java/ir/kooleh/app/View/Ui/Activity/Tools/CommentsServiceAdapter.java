package ir.kooleh.app.View.Ui.Activity.Tools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import ir.kooleh.app.R;

public class CommentsServiceAdapter extends RecyclerView.Adapter<CommentsServiceAdapter.AdapterHolder> {


    private Context context;
    private List<Comment> list;
    private OnClickItemListener listener;

    public CommentsServiceAdapter(Context context , List<Comment> list ,OnClickItemListener listener) {

        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @Override
    public AdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final AdapterHolder viewHolder = new AdapterHolder(LayoutInflater.from(context).inflate(R.layout.item_commetns_service , parent , false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final AdapterHolder holder, int position) {
        final Comment item = list.get(position);
        holder.tvUserNameItemComment.setText(item.getFullName());
        holder.tvDateItemComment.setText(item.getDate());
        holder.tvMessageItemComment.setText(item.getText());

        switch (item.getPointId()){
            case  "0":
                holder.ivRate.setColorFilter(ContextCompat.getColor(context, R.color.red_400));
                holder.ivRate.setImageDrawable(context.getResources().getDrawable(R.drawable.ico_black_point_1));
                break;
            case  "1":
                holder.ivRate.setColorFilter(ContextCompat.getColor(context, R.color.orange_400));
                holder.ivRate.setImageDrawable(context.getResources().getDrawable(R.drawable.ico_black_point_2));
                break;
            case  "2":
                holder.ivRate.setColorFilter(ContextCompat.getColor(context, R.color.yellow_400));
                holder.ivRate.setImageDrawable(context.getResources().getDrawable(R.drawable.ico_black_point_3));
                break;
            case "3":
                holder.ivRate.setColorFilter(ContextCompat.getColor(context, R.color.green_300));
                holder.ivRate.setImageDrawable(context.getResources().getDrawable(R.drawable.ico_black_point_4));
                break;
            case "4" +
                    "":
                holder.ivRate.setColorFilter(ContextCompat.getColor(context, R.color.green_400));
                holder.ivRate.setImageDrawable(context.getResources().getDrawable(R.drawable.ico_black_point_5));
                break;
        }

        if (item.getImage()!=null && !item.getImage().equals(""))
            Glide.with(context).load(item.getImage()).error(R.drawable.ic_profile).placeholder(R.drawable.ic_profile).into(holder.ivProfileItemComment);
        else
            Glide.with(context).load(R.drawable.ic_profile).into(holder.ivProfileItemComment);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickItem(v,item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class AdapterHolder extends RecyclerView.ViewHolder {

        private CircleImageView ivProfileItemComment;
        private ImageView ivRate;
        private TextView tvDateItemComment;
        private TextView tvUserNameItemComment;
        private TextView tvMessageItemComment;

        public AdapterHolder(View itemView) {
            super(itemView);
            ivProfileItemComment = (CircleImageView)itemView.findViewById( R.id.iv_profile_comment_item);
            ivRate = (ImageView)itemView.findViewById( R.id.iv_rate_comment_item);
            tvDateItemComment = (TextView)itemView.findViewById( R.id.tv_date_item_comment );
            tvUserNameItemComment = (TextView)itemView.findViewById( R.id.tv_user_name_item_comment );
            tvMessageItemComment = (TextView)itemView.findViewById( R.id.tv_message_comment_item);
        }
    }

    public interface OnClickItemListener{
        void onClickItem(View view, Comment item);
    }
}
