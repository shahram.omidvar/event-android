package ir.kooleh.app.View.Ui.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Enum.EnumMessageType;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Utility.Class.InterClassDataPassKeys;
import ir.kooleh.app.View.ViewCustom.CButton;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class FragmentMessageHandler extends Fragment implements View.OnClickListener
{
    private View view;
    private CTextView messageTv;
    private CButton messageBtn;

    private String strMessage;
    private String strHomeFragmentType;
    private LottieAnimationView lottieAnimationView;

    private InterfaceMessageHandler.FragmentResult interfaceMessageHandler;
    private InterfaceMessageHandler.HomeFragmentsResponse homeFragmentsResponse;


    public FragmentMessageHandler(InterfaceMessageHandler.FragmentResult interfaceMessageHandler)
    {
        this.interfaceMessageHandler = interfaceMessageHandler;
    }

    public FragmentMessageHandler(InterfaceMessageHandler.FragmentResult interfaceMessageHandler, InterfaceMessageHandler.HomeFragmentsResponse homeFragmentsResponse)
    {
        this.interfaceMessageHandler = interfaceMessageHandler;
        this.homeFragmentsResponse = homeFragmentsResponse;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_message_handler, container, false);
        initViews();
        initVars();
        setViewsValue();
        return view;
    }



    private void initViews()
    {
        messageTv = view.findViewById(R.id.tv_message);
        messageBtn = view.findViewById(R.id.btn_message);
        lottieAnimationView = view.findViewById(R.id.animation_message);
        messageBtn.setOnClickListener(this);
    }

    private void initVars()
    {
        strMessage = getArguments().getString("message");
        strHomeFragmentType = getArguments().getString(InterClassDataPassKeys.FragmentMessageHandler.FRAGMENT_TYPE, "");
        EnumMessageType enumMessageType = (EnumMessageType) getArguments().get(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE);

        if (enumMessageType == EnumMessageType.NO_NETWORK)
        {
            lottieAnimationView.clearAnimation();
            lottieAnimationView.cancelAnimation();
            lottieAnimationView.setAnimation(R.raw.animation_connection_error);
            lottieAnimationView.playAnimation();
        }
        else if (enumMessageType == EnumMessageType.EMPTY)
        {
            lottieAnimationView.clearAnimation();
            lottieAnimationView.cancelAnimation();
            lottieAnimationView.setAnimation(R.raw.animation_empty);
            lottieAnimationView.playAnimation();
        }
    }

    @Override
    public void onDetach()
    {
        interfaceMessageHandler = null;
        homeFragmentsResponse = null;
        view = null;
        messageBtn = null;
        messageTv = null;
        lottieAnimationView = null;
        strMessage = null;
        strHomeFragmentType = null;
        super.onDetach();
    }

    private void setViewsValue()
    {
        messageTv.setText(strMessage);
    }


    @Override
    public void onClick(View v)
    {
        if (v == messageBtn)
        {
            if (!strHomeFragmentType.equals(""))
            {
                homeFragmentsResponse.onResponseMessageHandler(strHomeFragmentType);
            } else
            {
                interfaceMessageHandler.onResponseMessageHandler();
            }
        }
    }
}
