
package ir.kooleh.app.View.Ui.Activity.Tools;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BasicService {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("location_name")
    @Expose
    private String locationName;
    @SerializedName("s_type")
    @Expose
    private Object sType;
    @SerializedName("is_bookmark")
    @Expose
    private Integer isBookmark;
    @SerializedName("share_link")
    @Expose
    private String shareLink;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("slider")
    @Expose
    private List<String> slider = null;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("off")
    @Expose
    private Object off;
    @SerializedName("next_link")
    @Expose
    private Object nextLink;
    @SerializedName("title_link")
    @Expose
    private String titleLink;
    @SerializedName("model_link")
    @Expose
    private String modelLink;
    @SerializedName("rate")
    @Expose
    private String rate;
    @SerializedName("comment_count")
    @Expose
    private Integer commentCount;
    @SerializedName("count_view")
    @Expose
    private String countView;
    @SerializedName("start_date")
    @Expose
    private Object startDate;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;

    @SerializedName("option_menu")
    @Expose
    private List<MenuOption> menuOptions = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Object getSType() {
        return sType;
    }

    public void setSType(Object sType) {
        this.sType = sType;
    }

    public Integer getIsBookmark() {
        return isBookmark;
    }

    public void setIsBookmark(Integer isBookmark) {
        this.isBookmark = isBookmark;
    }

    public String getShareLink() {
        return shareLink;
    }

    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<String> getSlider() {
        return slider;
    }

    public void setSlider(List<String> slider) {
        this.slider = slider;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Object getOff() {
        return off;
    }

    public void setOff(Object off) {
        this.off = off;
    }

    public Object getNextLink() {
        return nextLink;
    }

    public void setNextLink(Object nextLink) {
        this.nextLink = nextLink;
    }

    public String getTitleLink() {
        return titleLink;
    }

    public void setTitleLink(String titleLink) {
        this.titleLink = titleLink;
    }

    public String getModelLink() {
        return modelLink;
    }

    public void setModelLink(String modelLink) {
        this.modelLink = modelLink;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public String getCountView() {
        return countView;
    }

    public void setCountView(String countView) {
        this.countView = countView;
    }

    public Object getStartDate() {
        return startDate;
    }

    public void setStartDate(Object startDate) {
        this.startDate = startDate;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public List<MenuOption> getMenuOptions() {
        return menuOptions;
    }

    public void setMenuOptions(List<MenuOption> menuOptions) {
        this.menuOptions = menuOptions;
    }
}
