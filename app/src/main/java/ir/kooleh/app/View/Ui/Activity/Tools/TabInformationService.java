
package ir.kooleh.app.View.Ui.Activity.Tools;


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TabInformationService {

    @SerializedName("tab_name")
    @Expose
    private String tabName;
    @SerializedName("more_info")
    @Expose
    private List<MoreInfo> moreInfo = null;
    @SerializedName("com_channels")
    @Expose
    private ComChannels comChannels;
    @SerializedName("location")
    @Expose
    private Location location;

    @SerializedName("study_link")
    @Expose
    private Object studyLink;

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    public List<MoreInfo> getMoreInfo() {
        return moreInfo;
    }

    public void setMoreInfo(List<MoreInfo> moreInfo) {
        this.moreInfo = moreInfo;
    }

    public ComChannels getComChannels() {
        return comChannels;
    }

    public void setComChannels(ComChannels comChannels) {
        this.comChannels = comChannels;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Object getStudyLink() {
        return studyLink;
    }

    public void setStudyLink(Object studyLink) {
        this.studyLink = studyLink;
    }
}
