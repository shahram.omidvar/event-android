package ir.kooleh.app.View.ViewCustom;



import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRadioButton;

import ir.kooleh.app.R;
import ir.kooleh.app.Utility.Volley.Controller;


public class CSRadioButton extends AppCompatRadioButton
{
    private View view;
    private CTextView tvTitle;
    private CTextView tvValue;

    public CSRadioButton(@NonNull Context context)
    {
        super(context);

        if (!isInEditMode())
            setTypeface(Controller.MyTypeface);
    }

    public CSRadioButton(@NonNull Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);

        if (!isInEditMode())
            setTypeface(Controller.MyTypeface);
    }

    public CSRadioButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);

        if (!isInEditMode())
            setTypeface(Controller.MyTypeface);
    }

//
//    private RequestListener<Bitmap> requestListener = new RequestListener<Bitmap>() {
//        @Override
//        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
//            return false;
//        }
//
//        @Override
//        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
//            imageView.setImageBitmap(resource);
//            redrawLayout();
//            return false;
//        }
//    };

//    public void setImageResource(int resId) {
//        Glide.with(getContext())
//                .asBitmap()
//                .load(resId)
//                .apply(RequestOptions.bitmapTransform(
//                        new MultiTransformation<>(
//                                new CenterCrop(),
//                                new RoundedCornersTransformation(dp2px(getContext(), 24), 0, RoundedCornersTransformation.CornerType.ALL))
//                        )
//                )
//                .listener(requestListener)
//                .submit();
//    }

//    public void setImageBitmap(Bitmap bitmap) {
//        Glide.with(getContext())
//                .asBitmap()
//                .load(bitmap)
//                .apply(RequestOptions.bitmapTransform(
//                        new MultiTransformation<>(
//                                new CenterCrop(),
//                                new RoundedCornersTransformation(dp2px(getContext(), 24), 0, RoundedCornersTransformation.CornerType.ALL))
//                        )
//                )
//                .listener(requestListener)
//                .submit();
//    }

    // setText is a final method in ancestor, so we must take another name.


}
