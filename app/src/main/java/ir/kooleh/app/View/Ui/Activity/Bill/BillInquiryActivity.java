package ir.kooleh.app.View.Ui.Activity.Bill;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Bill.ApiBills;
import ir.kooleh.app.Structure.Interface.Bill.InterfaceUserBill;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Model.Bills.ModelBillCategories;
import ir.kooleh.app.Structure.Model.Bills.ModelUserBill;
import ir.kooleh.app.Utility.Adapter.Bill.AdapterUserBill;
import ir.kooleh.app.Utility.Class.ColorUtils;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.Utility.Class.NumberUtils;
import ir.kooleh.app.View.Ui.Fragment.FragmentMessageHandler;
import ir.kooleh.app.View.ViewCustom.CEditText;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class BillInquiryActivity extends AppCompatActivity implements
        View.OnClickListener,
        InterfaceUserBill.UserBill.IntGetUserBills,
        InterfaceUserBill.BillInquiry.IntInquireBill,
        InterfaceUserBill.BillInquiry.IntClickBillItem,
        InterfaceMessageHandler.FragmentResult
{
    private Toolbar toolbar;
    private CTextView tvTitleToolbar;
    private ImageView ivBackToolbar;
    private CardView containerIcon;
    private ImageView ivIconBill;
    private CEditText etBillIDOrPhoneNumber;
    private CardView btnAcionEditText;
    private ImageView ivBtnActionEdittext;
    private TextInputLayout txtInputEtBillIDOrPhoneNumber;
    private CardView btnInquireBill;
    private RecyclerView rvBillInquiry;
    private CTextView tvNoBill;
//    private CTextView tvViewAllItems;

    private FrameLayout containerFragmentMessageHandler;
    private FragmentMessageHandler fragmentMessageHandler;
    private NestedScrollView nsvBillInquiryActivity;

    private AdapterUserBill adapterUserBill;

    private ViewLoading viewLoading;
    private CustomToast customToast;
    private ApiBills apiBills;

    private final int nextPage = 1;
    private final int itemsPerPage = 5;

    public static final String KEY_OBJECT_BILL_TYPE = "objectBillType";
    public static final String KEY_TYPE_REQUEST_BILL_ID = "bill_id";
    public static final String KEY_TYPE_REQUEST_MOBILE_NUMBER = "mobile_number";

    private ModelBillCategories.Child modelBillType;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_inquiry);
        initViews();
        initVars();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initViews()
    {
        toolbar = findViewById(R.id.toolbar_app);
        tvTitleToolbar = findViewById(R.id.tv_title_toolbar);
        ivBackToolbar = findViewById(R.id.iv_back_toolbar);
        containerIcon = findViewById(R.id.container_icon_bill_inquiry);
        ivIconBill = findViewById(R.id.iv_icon_bill_inquiry);
        btnInquireBill = findViewById(R.id.btn_inquire_bill);
        rvBillInquiry = findViewById(R.id.rv_bill_inquiry);
        tvNoBill = findViewById(R.id.tv_no_bills_bill_inquiry);
        nsvBillInquiryActivity = findViewById(R.id.nsv_bill_inquiry_activity);
        containerFragmentMessageHandler = findViewById(R.id.container_message_handler_fragment);
//        tvViewAllItems = findViewById(R.id.tv_view_all_items);

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        modelBillType = gson.fromJson(getIntent().getExtras().getString(KEY_OBJECT_BILL_TYPE), ModelBillCategories.Child.class);

        toolbar.setBackgroundColor(Color.parseColor(modelBillType.getBackColor()));
        ivBackToolbar.setColorFilter(getResources().getColor(R.color.gray_650), PorterDuff.Mode.SRC_IN);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ColorUtils.darken(
                Color.parseColor(modelBillType.getBackColor()), .2));

        tvTitleToolbar.setText(modelBillType.getfName());
        tvTitleToolbar.setTextColor(getResources().getColor(R.color.gray_650));

        etBillIDOrPhoneNumber = findViewById(R.id.et_bill_id_or_phone_number);
        txtInputEtBillIDOrPhoneNumber = findViewById(R.id.txt_input_layout_bill_id_or_phone_number);
        ivBtnActionEdittext = findViewById(R.id.iv_btn_action_edittext_inquiry_bill_activity);
        btnAcionEditText = findViewById(R.id.btn_action_edittext_inquiry_bill_activity);

        btnAcionEditText.setOnClickListener(this);

        if (modelBillType.getTypeRequest().equals(KEY_TYPE_REQUEST_BILL_ID))
        {
            ivBtnActionEdittext.setImageResource(R.drawable.ic_barcode);
            txtInputEtBillIDOrPhoneNumber.setHint("شناسه قبض");
        }
        else
        {
            ivBtnActionEdittext.setImageResource(R.drawable.ic_contact);
            txtInputEtBillIDOrPhoneNumber.setHint("شماره تلفن(یازده رقم)");
            etBillIDOrPhoneNumber.setFilters(new InputFilter[] {new InputFilter.LengthFilter(Integer.parseInt(modelBillType.getCountBillID()))});
        }

        ivBackToolbar.setOnClickListener(this);
        btnInquireBill.setOnClickListener(this);
//        tvViewAllItems.setOnClickListener(this);

        etBillIDOrPhoneNumber.setFilters(new InputFilter[] { new InputFilter.LengthFilter(Integer.parseInt(modelBillType.getCountBillID())) });


        etBillIDOrPhoneNumber.setOnTouchListener((v, event) ->
        {
            txtInputEtBillIDOrPhoneNumber.setErrorEnabled(false); btnAcionEditText.setVisibility(View.VISIBLE);
            return false;
        });

        etBillIDOrPhoneNumber.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s)
            {
                if (s.length() > 0)
                {
                    btnAcionEditText.setVisibility(View.VISIBLE);
                    txtInputEtBillIDOrPhoneNumber.setErrorEnabled(false);
                }
            }
        });
    }

    private void initVars()
    {
        viewLoading = new ViewLoading(this);
        apiBills = new ApiBills(this);

        containerIcon.setCardBackgroundColor(Color.parseColor(modelBillType.getBackColor()));
        Glide.with(this).load(modelBillType.getImage()).into(ivIconBill);

        adapterUserBill = new AdapterUserBill(this,
                modelBillType.getfName(),
                modelBillType.getBackColor(),
                modelBillType.getImage());

        rvBillInquiry.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvBillInquiry.setNestedScrollingEnabled(false);
        rvBillInquiry.setAdapter(adapterUserBill);

        viewLoading.show();
        apiBills.getUserBills(this, modelBillType.getId(), nextPage, itemsPerPage);
    }

    @Override
    public void onClick(View v)
    {
        if (v == ivBackToolbar)
        {
            finish();
        }
        else if (v == btnInquireBill)
        {
            if (!etBillIDOrPhoneNumber.getText().toString().equals(""))
            {
                viewLoading.show();
                if (modelBillType.getTypeRequest().equals(KEY_TYPE_REQUEST_BILL_ID))
                {
                    apiBills.inquireBill(this, modelBillType.getId(), etBillIDOrPhoneNumber.getText().toString());
                }
                else
                {
                    apiBills.inquirePhoneBill(this, modelBillType.getId(), etBillIDOrPhoneNumber.getText().toString());
                }
            }
            else
            {
                btnAcionEditText.setVisibility(View.GONE);
                txtInputEtBillIDOrPhoneNumber.setErrorEnabled(true);
                txtInputEtBillIDOrPhoneNumber.setError("این فیلد نمیتواند خالی باشد");
            }
        }
        else if (v == btnAcionEditText)
        {
            if (modelBillType.getTypeRequest().equals(KEY_TYPE_REQUEST_BILL_ID))
            {
                IntentIntegrator integrator = new IntentIntegrator(this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                integrator.setCameraId(0);
                integrator.setPrompt("برای اسکن بارکد را در درون ناحیه مستطیلی قرار دهید");
                integrator.setBeepEnabled(false);
                integrator.setOrientationLocked(false);
                integrator.setBarcodeImageEnabled(true);
                integrator.initiateScan();
            }
            else
            {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, 1);
                }
            }
        }
//        else if (v == tvViewAllItems)
//        {
//            Intent intent = new Intent(this, ViewAllItemsActivity.class);
//            GsonBuilder builder = new GsonBuilder();
//            Gson gson = builder.create();
//            intent.putExtra(BillInquiryActivity.KEY_OBJECT_BILL_TYPE, gson.toJson(modelBillType));
//            intent.putExtra(ViewAllItemsActivity.ITEMS_TYPE, ViewAllItemsActivity.ITEMS_TYPE_BILL);
//            startActivity(intent);
//        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                Uri contactUri = data.getData();
                String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER};
                Cursor cursor = getContentResolver().query(contactUri, projection, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

                    String number = cursor.getString(numberIndex);
                    number = number.trim().replace("+", "").replace(" ", "");
                    if (NumberUtils.isNumeric(number))
                    {
                        number = "0" + number.substring(number.length() - (Integer.parseInt(modelBillType.getCountBillID()) - 1));
                        etBillIDOrPhoneNumber.setText(number);
                    }
                    else
                    {
                        customToast = new CustomToast(this);
                        customToast.showError("شماره انتخابی معتبر نمیباشد");
                    }

                    cursor.close();
                }
            }
            return;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null)
        {
            if (result.getContents() != null)
                etBillIDOrPhoneNumber.setText(result.getContents().substring(0, Math.min(Integer.parseInt(modelBillType.getCountBillID()), result.getContents().length())));
        }
        else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void onResponseGetUserBills(boolean response, String message, List<ModelUserBill> userBillList)
    {
        viewLoading.close();
        nsvBillInquiryActivity.setVisibility(View.GONE);
        containerFragmentMessageHandler.setVisibility(View.GONE);
        if (response)
        {
            nsvBillInquiryActivity.setVisibility(View.VISIBLE);
            if (userBillList.size() == 0)
            {
                tvNoBill.setVisibility(View.VISIBLE);
                rvBillInquiry.setVisibility(View.GONE);
            }
            else
            {
                rvBillInquiry.setVisibility(View.VISIBLE);
                tvNoBill.setVisibility(View.GONE);
                adapterUserBill.setList(userBillList);
            }
        }
        else
        {
            containerFragmentMessageHandler.setVisibility(View.VISIBLE);
            fragmentMessageHandler = new FragmentMessageHandler(this);
            Bundle args = new Bundle();
            args.putString("message", message);
            fragmentMessageHandler.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        }
    }

    @Override
    public void onResponseInquireBill(boolean response, String message, String billInquiryResult)
    {
        if (response)
        {
            apiBills.getUserBills(this, modelBillType.getId(), nextPage, itemsPerPage);
            Intent intent = new Intent(this, BillDetailsActivity.class);
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            intent.putExtra(BillInquiryActivity.KEY_OBJECT_BILL_TYPE, gson.toJson(modelBillType));
            intent.putExtra(BillDetailsActivity.KEY_BILL_DETAILS, billInquiryResult);
            startActivity(intent);
        }
        else
        {
            viewLoading.close();
            customToast = new CustomToast(this);
            customToast.showError(message);
        }
    }

    @Override
    public void onResponseIntClickBillItem(String value)
    {
        etBillIDOrPhoneNumber.setText(value);
    }

    @Override
    public void onResponseMessageHandler()
    {
        viewLoading.show();
        apiBills.getUserBills(this, modelBillType.getId(), nextPage, itemsPerPage);
    }
}