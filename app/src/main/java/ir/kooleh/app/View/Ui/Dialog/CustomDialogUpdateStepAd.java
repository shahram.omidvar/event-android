package ir.kooleh.app.View.Ui.Dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import androidx.cardview.widget.CardView;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Enum.EnumBtnAdDetails;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCustomDialogUpdateStepAd;
import ir.kooleh.app.View.ViewCustom.CEditText;
import ir.kooleh.app.View.ViewCustom.CTextView;


public class CustomDialogUpdateStepAd implements View.OnClickListener
{
    private AlertDialog dialog;
    private CardView btnPositive, btnNegative;
    private CEditText etExplain;
    private CTextView tvTitle;
    private ImageView ivClose;

    private InterfaceCustomDialogUpdateStepAd interfaceCustomDialogUpdateStepAd;

    private String step;

    public void make(Activity activity, String step)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        View inflater = LayoutInflater.from(activity).inflate(R.layout.custom_dialog_update_step_ad, null);
        builder.setView(inflater);
        dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        initView(inflater);
        interfaceCustomDialogUpdateStepAd = (InterfaceCustomDialogUpdateStepAd) activity;
        this.step = step;
        fitTitleToStep();

        dialog.setOnKeyListener((dialog, keyCode, event) ->
        {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP && !event.isCanceled())
            {
                dismiss();
                return true;
            }
            return false;
        });

        dialog.setCanceledOnTouchOutside(false);
    }



    private void initView(View view)
    {
        tvTitle = view.findViewById(R.id.tv_title_update_step_ad);
        etExplain = view.findViewById(R.id.et_explain_update_step_ad);
        btnPositive = view.findViewById(R.id.btn_positive_update_step_ad);
        btnNegative = view.findViewById(R.id.btn_negative_update_step_ad);
        ivClose = view.findViewById(R.id.iv_close_dialog_update_step_ad);

        btnPositive.setOnClickListener(this);
        btnNegative.setOnClickListener(this);
        ivClose.setOnClickListener(this);
    }


    private void fitTitleToStep()
    {
        switch (step)
        {
            case EnumBtnAdDetails.ActionAPIValueType.UPDATE_STEP_AD_TYPE.STEP_CANCEL_PENDING:
            case EnumBtnAdDetails.ActionAPIValueType.UPDATE_STEP_AD_TYPE.STEP_CANCEL_AUCTION:
                tvTitle.setText("دلیل لغو آگهی را وارد کنید (اختیاری)");
                break;
            case EnumBtnAdDetails.ActionAPIValueType.UPDATE_STEP_AD_TYPE.STEP_CLOSE_AD:
                tvTitle.setText("دلیل بستن آگهی را وارد کنید (اختیاری)");
                break;
        }
    }



    public void show()
    {
        if (dialog != null)
        {
            dismiss();
            dialog.show();
        }
    }


    public void dismiss()
    {
        if (dialog != null)
        {
            if (dialog.isShowing())
            {
                dialog.dismiss();
                dialog = null;
                interfaceCustomDialogUpdateStepAd = null;
                btnPositive = null;
                btnNegative = null;
                etExplain = null;
                tvTitle = null;
                ivClose = null;
                step = null;
            }
        }
    }

    @Override
    public void onClick(View view)
    {

        if (view == btnPositive)
        {
            interfaceCustomDialogUpdateStepAd.onResponseDialogUpdateStepAd(step, "307", etExplain.getText().toString());
        }
        else if (view == btnNegative)
        {
            interfaceCustomDialogUpdateStepAd.onResponseDialogUpdateStepAd(step, "308", etExplain.getText().toString());
        }
        else if (view == ivClose)
        {
            dismiss();
        }
    }
}
