package ir.kooleh.app.View.Ui.Dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import androidx.cardview.widget.CardView;

import com.google.android.material.textfield.TextInputLayout;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Enum.EnumBtnAdDetails;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCustomDialogSetActionAd;
import ir.kooleh.app.View.ViewCustom.CEditText;
import ir.kooleh.app.View.ViewCustom.CTextView;


public class CustomDialogSetActionAd implements View.OnClickListener
{
    private AlertDialog dialog;
    private CTextView btnCancel;
    private CTextView tvTitle;
    private CardView btnApply;
    private CTextView tvBtnApply;
    private CEditText etExplain;
    private TextInputLayout txtInputEtExplain;

    private InterfaceCustomDialogSetActionAd interfaceCustomDialogSetActionAd;

    private String command;

    public void make(Activity activity, String command)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        View inflater = LayoutInflater.from(activity).inflate(R.layout.custom_dialog_set_action_ad, null);
        builder.setView(inflater);
        dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        initView(inflater);
        interfaceCustomDialogSetActionAd = (InterfaceCustomDialogSetActionAd) activity;

        this.command = command;
        setdialogType(activity);


        dialog.setOnKeyListener((dialog, keyCode, event) ->
        {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP && !event.isCanceled())
            {
                dismiss();
                return true;
            }
            return false;
        });

        dialog.setCanceledOnTouchOutside(false);
    }



    private void initView(View view)
    {
        tvTitle = view.findViewById(R.id.tv_title_set_action_ad);
        btnCancel = view.findViewById(R.id.btn_cancel_offer_dialog);
        btnApply = view.findViewById(R.id.btn_apply_set_action_ad);
        etExplain = view.findViewById(R.id.et_explain_set_action_ad);
        txtInputEtExplain = view.findViewById(R.id.txt_input_explain_set_action_ad);
        tvBtnApply = view.findViewById(R.id.tv_btn_apply_set_action_ad);

        btnCancel.setOnClickListener(this);
        btnApply.setOnClickListener(this);
    }


    private void setdialogType(Activity activity)
    {
        switch (command)
        {
            case EnumBtnAdDetails.ActionAPIValueType.SET_ACTION_AD_TYPE.DELETE_MY_OFFER:
                tvTitle.setText("آیا از حذف پیشنهاد خود اطمینان دارید");
                txtInputEtExplain.setVisibility(View.GONE);
                btnApply.setCardBackgroundColor(activity.getResources().getColor(R.color.red_400));
                tvBtnApply.setText("حذف");
                break;
            case EnumBtnAdDetails.ActionAPIValueType.SET_ACTION_AD_TYPE.END_FAST_AUCTION:
                tvTitle.setText("با اتمام زمان مناقصه، دیگر امکان پیشنهاد گذاری برای این مناقصه وجود نخواهد داشت و به صفحه انتخاب برنده هدایت می شوید. آیا از تصمیم خود مطمئنید؟");
                txtInputEtExplain.setVisibility(View.GONE);
                btnApply.setCardBackgroundColor(activity.getResources().getColor(R.color.orange_400));
                tvBtnApply.setText("اتمام زمان مناقصه");
                break;
        }
    }



    public void show()
    {
        if (dialog != null)
        {
            dismiss();
            dialog.show();
        }
    }


    public void dismiss()
    {
        if (dialog != null)
        {
            if (dialog.isShowing())
            {
                dialog.dismiss();
                interfaceCustomDialogSetActionAd = null;
                dialog = null;
                btnCancel = null;
                tvTitle = null;
                btnApply = null;
                tvBtnApply = null;
                etExplain = null;
                txtInputEtExplain = null;
            }
        }
    }

    @Override
    public void onClick(View view)
    {

        if (view == btnApply)
        {
            interfaceCustomDialogSetActionAd.onResponseDialogSetActionAd(etExplain.getText().toString(), command);
        }
        else if (view == btnCancel)
        {
            dismiss();
        }
    }
}
