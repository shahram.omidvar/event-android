package ir.kooleh.app.View.Ui.Activity.dashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager2.widget.ViewPager2;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import net.cachapa.expandablelayout.ExpandableLayout;


import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Ads.ApiAds;
import ir.kooleh.app.Server.Api.Dashboard.ApiAdDetails;
import ir.kooleh.app.Server.Api.Dashboard.ApiDashboard;
import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Enum.EnCustomMessage;
import ir.kooleh.app.Structure.Enum.EnumBtnAdDetails;
import ir.kooleh.app.Structure.Interface.Ads.InterfaceAds;
import ir.kooleh.app.Structure.Interface.Custom.FragmentCommunicator;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCustomCreateComment;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCustomDialogUpdateStepAd;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCustomMessage;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceSlider;
import ir.kooleh.app.Structure.Interface.Dashboard.InterfaceDashboard;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Model.Ads.ModelAd;
import ir.kooleh.app.Structure.Model.Dashboard.ModelOffersMyAd;
import ir.kooleh.app.Utility.Adapter.Ads.Details.AdapterAdDetails;
import ir.kooleh.app.Utility.Adapter.Ads.Details.AdapterAdDetailsSpecial;
import ir.kooleh.app.Utility.Adapter.Ads.Details.AdapterSliderAdDetails;
import ir.kooleh.app.Utility.Adapter.Dashboard.AdapterChooseWinnerOffer;
import ir.kooleh.app.View.Ui.Dialog.CustomDialogMessage;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.Utility.Class.NumberUtils;
import ir.kooleh.app.View.Ui.Dialog.CustomDialogUpdateStepAd;
import ir.kooleh.app.View.Ui.Fragment.FragmentMessageHandler;
import ir.kooleh.app.View.Ui.Fragment.FragmentSlider;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class AdDetailsChooseWinnerActivity extends AppCompatActivity implements
        InterfaceSlider,
        InterfaceAds.IInfo,
        View.OnClickListener,
        InterfaceDashboard.InterClassData.IntSendWinnerDataToActivity,
        InterfaceMessageHandler.FragmentResult,
        InterfaceDashboard.Apis.IntGetOffersMyAd,
        InterfaceDashboard.Apis.IntChooseWinner,
        InterfaceCustomMessage,
        InterfaceCustomDialogUpdateStepAd,
        InterfaceDashboard.Apis.IntUpdateStepAd,
        InterfaceCustomCreateComment,
        FragmentCommunicator
{

    // view
    private RelativeLayout toolbar;
    private TextView tvTitleToolbar;
    private ImageView ivBackAdDetails;
    private ImageView ivBookmarkToolbar;
    private ImageView ivShareToolbar;
    private CTextView btnCancelToolbar;
    private LinearLayout lnrContainer;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FrameLayout containerMessageHandler;

    private NestedScrollView nstAdsDetailsActivity;
    private FrameLayout fragmentContainer;
//    private SliderView sliderViewAds;
    private RecyclerView rvSpecialAds, rvItemAds;
    private CTextView tvAdTitle, tvAdTime, tvAdDescription;
    private FragmentMessageHandler fragmentMessageHandler;
    private ImageView imgNoPic;
    private ViewPager2 viewPager2Slider;
    private DotsIndicator dotsIndicator;

    private CTextView tvEmptyOfferList;
    private LinearLayout lnrContainerChooseBtn;

    private ExpandableLayout exLayoutAdDetails;
    private FrameLayout btnExpandAdDetails;
    private ImageView imgIconExpandCollapseAdDetails;

    private ExpandableLayout exLayoutOfferList;
    private FrameLayout btnExpandOfferList;
    private ImageView imgIconExpandCollapseOfferList;

    private CardView btnChooseWinner;

    private RecyclerView rvOfferList;

    private CardView containerItemWinnerOffer;
    private ImageView imgUserWinnerOffer;
    private CTextView tvFullnameWinnerOffer, tvPriceWinnerOffer, tvDateWinnerOffer;

    private LinearLayout containerOptionBtn;
    private CardView btnOptionOne, btnOptionTwo;
    private CTextView tvBtnOptionOne, tvBtnOptionTwo;
    private ImageView imgBtnOptionOne, imgBtnOptionTwo;

    private CTextView tvFullnameChosen, tvPriceChosen;


    // vars
//    private AdapterSlider adapterSlider;
    private AdapterSliderAdDetails adapterSliderAdDetails;
    private AdapterAdDetails adapterAdsDetailsItem;
    private AdapterAdDetailsSpecial adapterAdsDetailsSpecial;
    private AdapterChooseWinnerOffer adapterChooseWinnerOffer;
    private ApiAds apiAds;
    private ApiDashboard apiDashboard;
    private ViewLoading viewLoading;
    private CustomToast customToast;
    private ModelAd modelAd;
    private CustomDialogMessage customDialogMessage;
    private CustomDialogUpdateStepAd customDialogUpdateStepAd;

    private RotateAnimation rotateAnimationUp, rotateAnimationDown;

    private String adID = "";
    private String chosenWinnerID = "";
    private String chosenAuctionID = "";
    private int resultCode = 1;
    public static String Ad_ID = "ad_id";
    public static String ADS_TITLE = "ads_title";
    public static final String API_NAME = "api_name";
    private String apiName;

    private boolean isWinnerChosen = false;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_details_choose_winner);
        initViews();
        initVars();
        getIntentData();
    }

    private void initViews()
    {
        toolbar = findViewById(R.id.toolbar_slider);
        tvTitleToolbar = findViewById(R.id.tv_title_toolbar_slider);
        ivBackAdDetails = findViewById(R.id.iv_back_toolbar_slider);
        ivBookmarkToolbar = findViewById(R.id.iv_bookmark_toolbar_slider);
        ivShareToolbar = findViewById(R.id.iv_share_toolbar_slider);
        btnCancelToolbar = findViewById(R.id.btn_end_toolbar_slider);
        lnrContainer = findViewById(R.id.lnr_ad_details_choose_winner);
        containerMessageHandler = findViewById(R.id.container_message_handler_fragment);
        swipeRefreshLayout = findViewById(R.id.swipe_ad_details_choose_winner);

        exLayoutAdDetails = findViewById(R.id.ex_layout_ad_details);
        btnExpandAdDetails = findViewById(R.id.btn_expand_ad_details);
        imgIconExpandCollapseAdDetails = findViewById(R.id.img_icon_expand_collapse_ad_details);
        viewPager2Slider = findViewById(R.id.viewpater2_slider_ad_details_choose_winner);
        dotsIndicator = findViewById(R.id.dots_indicator_ad_details_choose_winner);


        btnExpandOfferList = findViewById(R.id.btn_expand_offer_list);
        exLayoutOfferList = findViewById(R.id.ex_layout_offer_list);
        imgIconExpandCollapseOfferList = findViewById(R.id.img_icon_expand_collapse_offer_list);

        fragmentContainer = findViewById(R.id.contaienr_fragment);
        rvItemAds = findViewById(R.id.rv_ad_items_choose_winner);
        rvSpecialAds = findViewById(R.id.rv_special_ad_items_choose_winner);

        tvAdTitle = findViewById(R.id.tv_ad_title_choose_winner);
        tvAdTime = findViewById(R.id.tv_ad_time_choose_winner);
        tvAdDescription = findViewById(R.id.tv_ad_description_choose_winner);
        nstAdsDetailsActivity = findViewById(R.id.nested_ads_details_choose_winner);
        imgNoPic = findViewById(R.id.img_no_pic);

        btnChooseWinner = findViewById(R.id.btn_choose_winner);
        tvEmptyOfferList = findViewById(R.id.tv_empty_offer_list);
        lnrContainerChooseBtn = findViewById(R.id.lnr_container_choose_btn);

        rvOfferList = findViewById(R.id.rv_choose_winner_offer);

        tvFullnameChosen = findViewById(R.id.tv_fullname_chosen_winner_offer);
        tvPriceChosen = findViewById(R.id.tv_price_chosen_winner_offer);

        containerItemWinnerOffer = findViewById(R.id.container_item_winner_offer);
        imgUserWinnerOffer = findViewById(R.id.img_user_winner_offer);
        tvFullnameWinnerOffer = findViewById(R.id.tv_fullname_winner_offer);
        tvPriceWinnerOffer = findViewById(R.id.tv_price_winner_offer);
        tvDateWinnerOffer = findViewById(R.id.tv_date_winner_offer);

        containerOptionBtn = findViewById(R.id.container_option_btn_ad_details_winner_offer);
        btnOptionOne = findViewById(R.id.btn_option_one_ad_details_winner_offer);
        tvBtnOptionOne = findViewById(R.id.tv_btn_option_one_ad_details_winner_offer);
        imgBtnOptionOne = findViewById(R.id.img_btn_option_one_ad_details_winner_offer);

        btnOptionTwo = findViewById(R.id.btn_option_two_ad_details_winner_offer);
        tvBtnOptionTwo = findViewById(R.id.tv_btn_option_two_ad_details_winner_offer);
        imgBtnOptionTwo = findViewById(R.id.img_btn_option_two_ad_details_winner_offer);

        tvPriceChosen.setVisibility(View.GONE);
        ivBookmarkToolbar.setVisibility(View.INVISIBLE);
        ivShareToolbar.setVisibility(View.INVISIBLE);

        btnOptionOne.setVisibility(View.GONE);
        btnOptionTwo.setVisibility(View.GONE);

        tvAdTitle.setSelected(true);

        ivBackAdDetails.setOnClickListener(this);

        btnExpandAdDetails.setOnClickListener(this);
        btnExpandOfferList.setOnClickListener(this);
        btnChooseWinner.setOnClickListener(this);
        btnCancelToolbar.setOnClickListener(this);

        swipeRefreshLayout.setOnRefreshListener(this::getAdInfo);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            nstAdsDetailsActivity.setOnScrollChangeListener((View.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) ->
            {
                Rect scrollBounds = new Rect();
                nstAdsDetailsActivity.getHitRect(scrollBounds);

                if (viewPager2Slider.getLocalVisibleRect(scrollBounds) && tvTitleToolbar.getVisibility() == View.VISIBLE)
                {
                    tvTitleToolbar.setVisibility(View.INVISIBLE);

                    AlphaAnimation alphaAnimation = new AlphaAnimation(1, 0);
                    alphaAnimation.setDuration(200);
                    tvTitleToolbar.startAnimation(alphaAnimation);

                    final ObjectAnimator backgroundColorAnimator = ObjectAnimator.ofObject(toolbar,
                            "backgroundColor",
                            new ArgbEvaluator(),
                            0xffF8F8F8,
                            0x55ffffff);
                    backgroundColorAnimator.setDuration(200);
                    backgroundColorAnimator.start();
                }
                else if (!viewPager2Slider.getLocalVisibleRect(scrollBounds) && tvTitleToolbar.getVisibility() == View.INVISIBLE)
                {
                    tvTitleToolbar.setVisibility(View.VISIBLE);

                    AlphaAnimation alphaAnimation = new AlphaAnimation(0, 1);
                    alphaAnimation.setDuration(200);
                    tvTitleToolbar.startAnimation(alphaAnimation);

                    final ObjectAnimator backgroundColorAnimator = ObjectAnimator.ofObject(toolbar,
                            "backgroundColor",
                            new ArgbEvaluator(),
                            0x55ffffff,
                            0xffF8F8F8);
                    backgroundColorAnimator.setDuration(200);
                    backgroundColorAnimator.start();
                }
            });
        }
    }

    private void initVars()
    {
        apiAds = new ApiAds(this);
        apiDashboard = new ApiDashboard(this);
        viewLoading = new ViewLoading(this);
        customDialogMessage = new CustomDialogMessage();

        customDialogUpdateStepAd = new CustomDialogUpdateStepAd();

        rvSpecialAds.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvSpecialAds.setAdapter(adapterAdsDetailsSpecial = new AdapterAdDetailsSpecial(this));

        rvItemAds.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvItemAds.setAdapter(adapterAdsDetailsItem = new AdapterAdDetails(this));

        adapterChooseWinnerOffer = new AdapterChooseWinnerOffer(this);
        rvOfferList.setAdapter(adapterChooseWinnerOffer);
        rvOfferList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        rotateAnimationUp = new RotateAnimation(0f, 180f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimationUp.setRepeatCount(0);
        rotateAnimationUp.setDuration(300);
        rotateAnimationUp.setFillAfter(true);

        rotateAnimationDown = new RotateAnimation(180f, 0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimationDown.setRepeatCount(0);
        rotateAnimationDown.setDuration(300);
        rotateAnimationDown.setFillAfter(true);
    }




    private void getIntentData()
    {
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        adID = bundle.getString(Ad_ID, "");
        String titleAd = bundle.getString(ADS_TITLE, "-1");
        apiName = bundle.getString(API_NAME, UrlAPI.GET_AD);


        if (adID.equals(""))
        {
            customToast = new CustomToast(this);
            customToast.showError(getString(R.string.error_send_data_intent));
            finish();
        }

        else if (titleAd.equals("-1"))
        {
            customToast = new CustomToast(this);
            customToast.showError(getString(R.string.error_send_data_intent));
            finish();
        }

        else
        {
            tvTitleToolbar.setText(String.format("%s", titleAd));
            getAdInfo();
        }

    }

    private void getAdInfo()
    {
        viewLoading.show();
        apiAds.getAd(this, adID, UrlAPI.GET_MY_AD);
    }

    private void getOfferMyAds()
    {
        apiDashboard.getOffersMyAd(this, adID);
    }


    private void setAdsInfo(ModelAd adsInfo)
    {

        tvAdTitle.setText(adsInfo.getBaseData().getTitleAd());
        tvAdTime.setText(adsInfo.getBaseData().getDateCreateAd());
        tvAdDescription.setText(adsInfo.getBaseData().getExplainAd());

        ModelAd.ListDataAd listDataAd = modelAd.getListDataAd();
        rvItemAds.setVisibility(View.GONE);
        rvSpecialAds.setVisibility(View.GONE);
        if (listDataAd != null)
        {
            if (listDataAd.getItemsAd().size() > 0)
            {
                adapterAdsDetailsItem.setListItemAds(listDataAd.getItemsAd());
                rvItemAds.setVisibility(View.VISIBLE);
            }

            if (listDataAd.getSpecialsAd().size() > 0)
            {
                adapterAdsDetailsSpecial.setListSpecialAds(listDataAd.getSpecialsAd());
                rvSpecialAds.setVisibility(View.VISIBLE);
            }
        }

        if (adsInfo.getSlider() != null && adsInfo.getSlider().size() >= 1)
        {
            adapterSliderAdDetails = new AdapterSliderAdDetails(this, adsInfo.getSlider());
            viewPager2Slider.setAdapter(adapterSliderAdDetails);
            dotsIndicator.setViewPager2(viewPager2Slider);
        }
        else
        {
            imgNoPic.setVisibility(View.VISIBLE);
        }

    }



    @Override
    public void onClick(View v)
    {
        if (v == ivBackAdDetails)
        {
            setResult(resultCode);
            finish();
        }
        else if (v == btnExpandAdDetails)
        {
            if (exLayoutAdDetails.isExpanded())
            {
                exLayoutAdDetails.collapse();
                imgIconExpandCollapseAdDetails.startAnimation(rotateAnimationDown);
            }
            else
            {
                exLayoutAdDetails.expand();
                imgIconExpandCollapseAdDetails.startAnimation(rotateAnimationUp);
            }
        }
        else if (v == btnExpandOfferList)
        {
            if (exLayoutOfferList.isExpanded())
            {
                exLayoutOfferList.collapse();
                imgIconExpandCollapseOfferList.startAnimation(rotateAnimationDown);
            }
            else
            {
                exLayoutOfferList.expand();
                imgIconExpandCollapseOfferList.startAnimation(rotateAnimationUp);
            }
        }
        else if (v == btnCancelToolbar)
        {
            customDialogUpdateStepAd.make(this, EnumBtnAdDetails.ActionAPIValueType.UPDATE_STEP_AD_TYPE.STEP_CANCEL_AUCTION);
            customDialogUpdateStepAd.show();
        }
        else if (v == btnChooseWinner)
        {
            if (!chosenWinnerID.equals(""))
            {
                String strExplain = String.format("آیا از انتخاب %s با قیمت %s به عنوان برنده مناقصه مطمئن هستید؟"
                        , tvFullnameChosen.getText(), tvPriceChosen.getText());

                customDialogMessage = new CustomDialogMessage();
                customDialogMessage.makeMessage(this, this)
                        .setTitle("انتخاب برنده")
                        .setCancelable(false)
                        .setExplain(strExplain)
                        .show();
            }
            else
            {
                customToast = new CustomToast(this);
                customToast.showError("لطفا یک برنده انتخاب کنید");
            }
        }

    }



    @Override
    public void onResponseGetAds(boolean response, ModelAd modelAd, String message)
    {
        viewLoading.close();
        lnrContainer.setVisibility(View.GONE);
        lnrContainerChooseBtn.setVisibility(View.GONE);
        btnCancelToolbar.setVisibility(View.GONE);
        containerMessageHandler.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
        if (response)
        {
            getOfferMyAds();
            lnrContainerChooseBtn.setVisibility(View.VISIBLE);
            lnrContainer.setVisibility(View.VISIBLE);
            btnCancelToolbar.setVisibility(View.VISIBLE);
            this.modelAd = modelAd;
            setAdsInfo(modelAd);
            setBtns(modelAd.getBtnAdList());

            if (modelAd.getWinnerData() != null)
            {
                setWinnerInfo(modelAd.getWinnerData());
            }
        }
        else
        {
            containerMessageHandler.setVisibility(View.VISIBLE);
            fragmentMessageHandler = new FragmentMessageHandler(this);
            Bundle args = new Bundle();
            args.putString("message", message);
            fragmentMessageHandler.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        }
    }

    private void setWinnerInfo(ModelOffersMyAd.ListOffer winnerData)
    {
        containerItemWinnerOffer.setVisibility(View.VISIBLE);
        containerOptionBtn.setVisibility(View.VISIBLE);
        Glide.with(this).load(winnerData.getImage()).placeholder(R.drawable.ic_avatar).into(imgUserWinnerOffer);
        tvFullnameWinnerOffer.setText(winnerData.getName());
        tvPriceWinnerOffer.setText(NumberUtils.digitDivider(winnerData.getPrice().toString()) + " ریال");
        tvDateWinnerOffer.setText(winnerData.getDate());
        lnrContainerChooseBtn.setVisibility(View.GONE);
        btnCancelToolbar.setVisibility(View.GONE);
    }


    private void setBtns(List<ModelAd.BtnAd> btnAdList)
    {
        if (btnAdList.size() >= 1)
        {
            containerOptionBtn.setVisibility(View.VISIBLE);
            btnOptionOne.setVisibility(View.VISIBLE);
            tvBtnOptionOne.setText(btnAdList.get(0).getFaName());
            tvBtnOptionOne.setTextColor(Color.parseColor(btnAdList.get(0).getTextColor()));
            btnOptionOne.setCardBackgroundColor(Color.parseColor(btnAdList.get(0).getBackColor()));
            Glide.with(this).load(btnAdList.get(0).getIcon()).into(imgBtnOptionOne);
        }
        else
        {
            containerOptionBtn.setVisibility(View.GONE);
        }

        if (btnAdList.size() >= 2)
        {
            btnOptionTwo.setVisibility(View.VISIBLE);
            btnOptionTwo.setCardBackgroundColor(Color.parseColor(btnAdList.get(1).getBackColor()));
            tvBtnOptionTwo.setText(btnAdList.get(1).getFaName());
            tvBtnOptionTwo.setTextColor(Color.parseColor(btnAdList.get(1).getTextColor()));
            Glide.with(this).load(btnAdList.get(1).getIcon()).into(imgBtnOptionTwo);
        }
    }

    @Override
    public void onResponseSendWinnerData(String userID, String auctionID, String username, String price)
    {
        if (userID.equals(""))
        {
            tvPriceChosen.setVisibility(View.GONE);
        }
        else
        {
            tvPriceChosen.setVisibility(View.VISIBLE);
        }
        tvFullnameChosen.setText(username);
        tvPriceChosen.setText(NumberUtils.digitDivider(price) + " ریال");
        chosenWinnerID = userID;
        chosenAuctionID = auctionID;
    }

    @Override
    public void onResponseMessageHandler()
    {
        getSupportFragmentManager().beginTransaction().remove(fragmentMessageHandler).commit();
        fragmentMessageHandler = null;
        getAdInfo();
    }

    @Override
    public void onClickedSlider(int currentItemPos)
    {
        if (modelAd != null && modelAd.getSlider() != null)
        {
            fragmentContainer.setVisibility(View.VISIBLE);
            getSupportFragmentManager().beginTransaction().replace(R.id.contaienr_fragment,
                    new FragmentSlider(this, modelAd.getSlider(), currentItemPos)).addToBackStack(null).commit();
        }
    }

    @Override
    public void onClosedSlider()
    {
        fragmentContainer.setVisibility(View.GONE);
    }

    @Override
    public void onResponseGetOffersMyAd(boolean response, ModelOffersMyAd modelOffersMyAd, String message)
    {
        if (response)
        {
            exLayoutOfferList.expand();
            imgIconExpandCollapseOfferList.startAnimation(rotateAnimationUp);
            tvEmptyOfferList.setVisibility(View.GONE);
            rvOfferList.setVisibility(View.GONE);
            tvFullnameChosen.setText("یکی از پیشنهاد ها را انتخاب کنید");
            tvPriceChosen.setText("");
            tvPriceChosen.setVisibility(View.GONE);

            if (modelOffersMyAd.getListOffer().size() == 0)
            {
                tvEmptyOfferList.setVisibility(View.VISIBLE);
            }
            else
            {
                rvOfferList.setVisibility(View.VISIBLE);
                if (modelAd.getWinnerData() == null)
                {
                    adapterChooseWinnerOffer.setList(modelOffersMyAd.getListOffer());
                }
                else
                {
                    adapterChooseWinnerOffer.setList(modelOffersMyAd.getListOffer(), modelAd.getWinnerData().getUserId());
                }
            }
        }
    }

    @Override
    public void onClickCustomDialog(EnCustomMessage enCustomMessage)
    {
        if (enCustomMessage == EnCustomMessage.YES)
        {
            viewLoading.show();
            apiDashboard.chooseWinner(this, chosenAuctionID, chosenWinnerID);
        }
    }

    @Override
    public void onResponseChooseWinner(boolean response, String message)
    {
        if (response)
        {
            customToast = new CustomToast(this);
            customToast.showSuccess(message);
            apiAds.getAd(this, adID, UrlAPI.GET_MY_AD);
            resultCode = 3;
            isWinnerChosen = true;
        }
        else
        {
            viewLoading.close();
            customToast = new CustomToast(this);
            customToast.showError(message);
        }
    }

    @Override
    public void onResponseDialogUpdateStepAd(String step, String actionID, String explain)
    {
        ApiAdDetails apiAdDetails = new ApiAdDetails(this);
        apiAdDetails.updateStepAd(this, modelAd.getBaseData().getId(), step, actionID, explain);
        viewLoading.show();
    }

    @Override
    public void onResponseUpdateStepAd(boolean response, String message)
    {
        viewLoading.close();
        if (response)
        {
            customToast = new CustomToast(this);
            customToast.showSuccess(message);
            customDialogUpdateStepAd.dismiss();
            Intent returnIntent = new Intent();
            setResult(resultCode, returnIntent);
            finish();
        }
        else
        {
            customToast = new CustomToast(this);
            customToast.showError(message);
        }
    }

    @Override
    protected void onDestroy()
    {
        if (isWinnerChosen)
        {
            setResult(resultCode);
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed()
    {
        if (isWinnerChosen)
        {
            setResult(resultCode);
        }
        super.onBackPressed();
    }

    @Override
    public void onClickCreateCommend(EnCustomMessage enCustomMessage, int adID, String massage, float rate)
    {

    }

    @Override
    public void onClickCreateCommend(EnCustomMessage enCustomMessage, int adID, String strValue, String massage, float rate)
    {

    }

    @Override
    public void fragmentDetached()
    {
        fragmentContainer.setVisibility(View.GONE);
    }
}