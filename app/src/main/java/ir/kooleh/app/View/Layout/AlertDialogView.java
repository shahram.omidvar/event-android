package ir.kooleh.app.View.Layout;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import java.text.MessageFormat;
import java.util.List;

import ir.kooleh.app.R;

import ir.kooleh.app.Structure.ListView.InterfaceView;

import ir.kooleh.app.Structure.Model.View.ModelViewValue;
import ir.kooleh.app.Utility.Adapter.View.AdapterDialogView;

public class AlertDialogView implements InterfaceView.Adapter.AlertDialogView
{

    private AlertDialog alertDialog;
    private TextView tvTitleAlterShowList, tvResultAlterDialog;
    private TextView tvBackAlertDialog;
    private RecyclerView rvAlertShowList;
    private ImageView ivCloseDialog;
    private String message;
    private List<ModelViewValue> listViewValue;

    private InterfaceView.AlertDialog.AlertDialogView interfaceAlertDialogView;
    private AdapterDialogView adapterDialogView;

    private int requestCode;
    private String chooseSequence = "";

    private static final String TAG = "alertdialogview";

    public AlertDialogView(InterfaceView.AlertDialog.AlertDialogView interfaceAlertDialogView)
    {
        this.interfaceAlertDialogView = interfaceAlertDialogView;
    }

    public void Create(Context context, String message, List<ModelViewValue> listViewValue)
    {
        this.listViewValue = listViewValue;

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View inflater = LayoutInflater.from(context).inflate(R.layout.layout_alert_dialog_show_list, null);
        builder.setView(inflater);
        alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        initView(inflater);
        createVar(message);
        this.message = message;

        alertDialog.setOnKeyListener((dialog, keyCode, event) ->
        {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP && !event.isCanceled())
            {
                dismiss();
                return true;
            }
            return false;
        });
        alertDialog.setCanceledOnTouchOutside(false);

    }




    private void initView(View view)
    {
        tvTitleAlterShowList = view.findViewById(R.id.tv_title_alter_dialog_show_list);
        tvBackAlertDialog = view.findViewById(R.id.tv_back_to_parent_alter_dialog_show_list);
        tvResultAlterDialog = view.findViewById(R.id.tv_result_alter_dialog_show_list);
        rvAlertShowList = view.findViewById(R.id.rv_alert_dialog_show_list);
        ivCloseDialog = view.findViewById(R.id.iv_close_alert_show_list);

        ivCloseDialog.setOnClickListener(v -> dismiss());

        tvBackAlertDialog.setOnClickListener(v -> {
            chooseSequence = "";
            createVar(message);
        });

    }

    private void createVar(String message)
    {
        setTitle(message);
        setTextResultAlter();

        rvAlertShowList.setAdapter(adapterDialogView = new AdapterDialogView(alertDialog.getContext(), this));
        rvAlertShowList.setLayoutManager(new LinearLayoutManager(alertDialog.getContext(), RecyclerView.VERTICAL, false));
        adapterDialogView.setListDialogView(listViewValue);
    }

    private void setTextResultAlter()
    {
        tvResultAlterDialog.setText(String.format(" تعداد نتایج : %s", listViewValue.size()));
    }


    public void setTitle(String massage)
    {
        tvTitleAlterShowList.setText(String.format("%s", massage));
    }

    public void show()
    {
        if (alertDialog != null)
        {
            dismiss();
            alertDialog.show();
        }
    }


    public void show(int requestCode)
    {
        this.requestCode = requestCode;
        if (alertDialog != null)
        {
            dismiss();
            alertDialog.show();
        }
    }


    public void dismiss()
    {
        if (alertDialog.isShowing())
        {
            alertDialog.dismiss();
            alertDialog = null;
            interfaceAlertDialogView = null;
            rvAlertShowList = null;
            adapterDialogView = null;
            listViewValue = null;
        }
    }


    @Override
    public void onClickItemAdapterDialogView(ModelViewValue value)
    {
        if (value.getChild() != null && value.getChild().size() > 0)
        {
            chooseSequence += value.getFaName() + " > ";
            tvResultAlterDialog.setText(String.format(" تعداد نتایج : %s", value.getChild().size()));
            tvTitleAlterShowList.setText(MessageFormat.format("{0} -> {1}", tvTitleAlterShowList.getText(), value.getFaName()));
            adapterDialogView.setListDialogView(value.getChild());
        }
        else
        {
            chooseSequence += value.getFaName();
            interfaceAlertDialogView.onResponseAlertDialogView(value, requestCode, chooseSequence);
            dismiss();
        }
    }
}
