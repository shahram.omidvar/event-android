package ir.kooleh.app.View.Ui.Activity.dashboard;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Dashboard.InterfaceDashboard;
import ir.kooleh.app.Utility.Adapter.Dashboard.AdapterViewPagerMyAds;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class MyAdsActivity extends AppCompatActivity
        implements View.OnClickListener,
        InterfaceDashboard.InterClassData.IntChangeTabPositionInMyAds
{

    private ViewPager2 viewpagerMyAds;
    private TabLayout tabsMyAds;
    private String ad_type;
    private CTextView tvTitleMyAd;
    private ImageView ivCloseActivity;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ads);
        initViews();

        ad_type = getIntent().getStringExtra("ad_type");

        if (ad_type.equals("my_ads"))
        {
            tvTitleMyAd.setText("آگهی های من");
            viewpagerMyAds.setAdapter(new AdapterViewPagerMyAds(this, 3, 9, false));
            new TabLayoutMediator(tabsMyAds, viewpagerMyAds,
                    (tab, position) ->
                    {
                        if (position == 0)
                        {
//                            View v = LayoutInflater.from(this).inflate(R.layout.custom_tab_layout, null, false);
//                            CTextView tvTitle = v.findViewById(R.id.tv_title_custom_tablayout);
//                            CTextView tvNumber = v.findViewById(R.id.tv_number_custom_tablayout);
//                            tvTitle.setText("فعال");
//                            FrameLayout frameLayout = new FrameLayout(this);
//                            frameLayout.addView(v);
//                            tab.setCustomView(v);
                            tab.setText("فعال");
                        }
                        else if (position == 1)
                        {
//                            View v = LayoutInflater.from(this).inflate(R.layout.custom_tab_layout, null, false);
//                            CTextView tvTitle = v.findViewById(R.id.tv_title_custom_tablayout);
//                            CTextView tvNumber = v.findViewById(R.id.tv_number_custom_tablayout);
//                            tvTitle.setText("در حال بررسی");
//                            FrameLayout frameLayout = new FrameLayout(this);
//                            frameLayout.addView(v);
//                            tab.setCustomView(v);
                            tab.setText("در حال بررسی");
                        }
                        else if (position == 2)
                        {
//                            View v = LayoutInflater.from(this).inflate(R.layout.custom_tab_layout, null, false);
//                            CTextView tvTitle = v.findViewById(R.id.tv_title_custom_tablayout);
//                            CTextView tvNumber = v.findViewById(R.id.tv_number_custom_tablayout);
//                            tvTitle.setText("بایگانی");
//                            FrameLayout frameLayout = new FrameLayout(this);
//                            frameLayout.addView(v);
//                            tab.setCustomView(v);
                            tab.setText("بایگانی");
                        }
                    }).attach();
        }
        else if (ad_type.equals("my_auctions"))
        {
            tabsMyAds.setTabMode(TabLayout.MODE_SCROLLABLE);
            tvTitleMyAd.setText("مناقصه های من");
            viewpagerMyAds.setAdapter(new AdapterViewPagerMyAds(this, 5, 10, false));
            new TabLayoutMediator(tabsMyAds, viewpagerMyAds,
                    (tab, position) ->
                    {
                        if (position == 0)
                        {
//                            View v = LayoutInflater.from(this).inflate(R.layout.custom_tab_layout, null, false);
//                            CTextView tvTitle = v.findViewById(R.id.tv_title_custom_tablayout);
//                            CTextView tvNumber = v.findViewById(R.id.tv_number_custom_tablayout);
//                            tvTitle.setText("مناقصه باز");
//                            FrameLayout frameLayout = new FrameLayout(this);
//                            frameLayout.addView(v);
//                            tab.setCustomView(v);
                            tab.setText("مناقصه باز");
                        }
                        else if (position == 1)
                        {
//                            View v = LayoutInflater.from(this).inflate(R.layout.custom_tab_layout, null, false);
//                            CTextView tvTitle = v.findViewById(R.id.tv_title_custom_tablayout);
//                            CTextView tvNumber = v.findViewById(R.id.tv_number_custom_tablayout);
//                            tvTitle.setText("انتخاب برنده");
//                            FrameLayout frameLayout = new FrameLayout(this);
//                            frameLayout.addView(v);
//                            tab.setCustomView(v);
                              tab.setText("انتخاب برنده");
                        }
                        else if (position == 2)
                        {
//                            View v = LayoutInflater.from(this).inflate(R.layout.custom_tab_layout, null, false);
//                            CTextView tvTitle = v.findViewById(R.id.tv_title_custom_tablayout);
//                            CTextView tvNumber = v.findViewById(R.id.tv_number_custom_tablayout);
//                            tvTitle.setText("فعال");
//                            FrameLayout frameLayout = new FrameLayout(this);
//                            frameLayout.addView(v);
//                            tab.setCustomView(v);
                            tab.setText("فعال");
                        }
                        else if (position == 3)
                        {
//                            View v = LayoutInflater.from(this).inflate(R.layout.custom_tab_layout, null, false);
//                            CTextView tvTitle = v.findViewById(R.id.tv_title_custom_tablayout);
//                            CTextView tvNumber = v.findViewById(R.id.tv_number_custom_tablayout);
//                            tvTitle.setText("در حال بررسی");
//                            FrameLayout frameLayout = new FrameLayout(this);
//                            frameLayout.addView(v);
//                            tab.setCustomView(v);
                            tab.setText("در حال بررسی");
                        }
                        else if (position == 4)
                        {
//                            View v = LayoutInflater.from(this).inflate(R.layout.custom_tab_layout, null, false);
//                            CTextView tvTitle = v.findViewById(R.id.tv_title_custom_tablayout);
//                            CTextView tvNumber = v.findViewById(R.id.tv_number_custom_tablayout);
//                            tvTitle.setText("بایگانی");
//                            FrameLayout frameLayout = new FrameLayout(this);
//                            frameLayout.addView(v);
//                            tab.setCustomView(v);
                            tab.setText("بایگانی");
                        }
                    }).attach();
        }


        else if (ad_type.equals("my_offers"))
        {
//            tabsMyAds.setTabMode(TabLayout.MODE_SCROLLABLE);
            tvTitleMyAd.setText("پیشنهاد های من");
            viewpagerMyAds.setAdapter(new AdapterViewPagerMyAds(this, 4, 11, false));
            new TabLayoutMediator(tabsMyAds, viewpagerMyAds,
                    (tab, position) ->
                    {
                        if (position == 0)
                        {
//                            View v = LayoutInflater.from(this).inflate(R.layout.custom_tab_layout, null, false);
//                            CTextView tvTitle = v.findViewById(R.id.tv_title_custom_tablayout);
//                            CTextView tvNumber = v.findViewById(R.id.tv_number_custom_tablayout);
//                            tvTitle.setText("برنده شده");
//                            FrameLayout frameLayout = new FrameLayout(this);
//                            frameLayout.addView(v);
//                            tab.setCustomView(v);
                            tab.setText("برنده شده");
                        }
                        else if (position == 1)
                        {
//                            View v = LayoutInflater.from(this).inflate(R.layout.custom_tab_layout, null, false);
//                            CTextView tvTitle = v.findViewById(R.id.tv_title_custom_tablayout);
//                            CTextView tvNumber = v.findViewById(R.id.tv_number_custom_tablayout);
//                            tvTitle.setText("مناقصه باز");
//                            FrameLayout frameLayout = new FrameLayout(this);
//                            frameLayout.addView(v);
//                            tab.setCustomView(v);
                            tab.setText("مناقصه باز");
                        }
                        else if (position == 2)
                        {
//                            View v = LayoutInflater.from(this).inflate(R.layout.custom_tab_layout, null, false);
//                            CTextView tvTitle = v.findViewById(R.id.tv_title_custom_tablayout);
//                            CTextView tvNumber = v.findViewById(R.id.tv_number_custom_tablayout);
//                            tvTitle.setText("در انتظار برنده");
//                            FrameLayout frameLayout = new FrameLayout(this);
//                            frameLayout.addView(v);
//                            tab.setCustomView(v);
                            tab.setText("در انتظار برنده");
                        }
                        else if (position == 3)
                        {
//                            View v = LayoutInflater.from(this).inflate(R.layout.custom_tab_layout, null, false);
//                            CTextView tvTitle = v.findViewById(R.id.tv_title_custom_tablayout);
//                            CTextView tvNumber = v.findViewById(R.id.tv_number_custom_tablayout);
//                            tvTitle.setText("بایگانی");
//                            FrameLayout frameLayout = new FrameLayout(this);
//                            frameLayout.addView(v);
//                            tab.setCustomView(v);
                            tab.setText("بایگانی");
                        }
                    }).attach();
        }
    }


    private void initViews()
    {
        viewpagerMyAds = findViewById(R.id.viewpager_my_auctions);
        tabsMyAds = findViewById(R.id.tabs_my_auctions);
        tvTitleMyAd = findViewById(R.id.tv_title_my_ads);
        ivCloseActivity = findViewById(R.id.iv_close_activity);

        viewpagerMyAds.setUserInputEnabled(false);


        ivCloseActivity.setOnClickListener(this);


        tabsMyAds.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                setTablayoutClickable(false);
                new Handler().postDelayed(() -> setTablayoutClickable(true), 1000);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) { }

            @Override
            public void onTabReselected(TabLayout.Tab tab) { }
        });
    }

    @Override
    public void onClick(View v)
    {
        if (v == ivCloseActivity)
        {
            finish();
        }
    }

    private void setTablayoutClickable(boolean clickable)
    {
        LinearLayout tabStrip = ((LinearLayout)tabsMyAds.getChildAt(0));
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener((v, event) -> !clickable);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResponseIntChangeTabPositionInMyAds(int tabPosition)
    {
        viewpagerMyAds.setAdapter(new AdapterViewPagerMyAds(this, 5, 10, true));
        new TabLayoutMediator(tabsMyAds, viewpagerMyAds,
                (tab, position) ->
                {
                    if (position == 0)
                        tab.setText("مناقصه باز");
                    else if (position == 1)
                        tab.setText("انتخاب برنده");
                    else if (position == 2)
                        tab.setText("فعال");
                    else if (position == 3)
                        tab.setText("در حال بررسی");
                    else if (position == 4)
                        tab.setText("بایگانی");
                }).attach();
        viewpagerMyAds.setCurrentItem(tabPosition);
    }

}