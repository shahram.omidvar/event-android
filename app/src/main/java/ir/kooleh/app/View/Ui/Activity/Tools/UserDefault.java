package ir.kooleh.app.View.Ui.Activity.Tools;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import ir.kooleh.app.Utility.Volley.Controller;

public class UserDefault {


    private static UserDefault storage;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private final String SHARED_PREFERENCE_NAME = "paralif data storage";

    public static UserDefault getInstance() {
        if (storage == null)
            storage = new UserDefault();
        return storage;
    }

    private UserDefault() {
        sharedPreferences = Controller.context.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }


    public boolean isFirstLunch() {
        return sharedPreferences.getBoolean("isFirstLunch", false);
    }

    public void setIsFirstLunch(boolean b) {
        editor.putBoolean("isFirstLunch", b);
        editor.commit();
    }

    public boolean isLogin() {
        return sharedPreferences.getBoolean("isLogin", false);
    }

    public void setIsLogin(boolean b) {
        editor.putBoolean("isLogin", b);
        editor.commit();
    }

    public boolean isWithoutLogin() {
        return sharedPreferences.getBoolean("isWithoutLogin", false);
    }

    public void setWithoutLogin(boolean b) {
        editor.putBoolean("isWithoutLogin", b);
        editor.commit();
    }

    public void saveUser(User user) {
        String userJson = new Gson().toJson(user);
        editor.putString("$@^*Use", userJson);
        editor.commit();
    }

    public void saveUser(String userJson) {
        editor.putString("$@^*Use", userJson);
        editor.commit();
    }

    public User getUser() {
        String userJson = sharedPreferences.getString("$@^*Use", "");

        try {
            if (!userJson.equals("")) {

                return new Gson().fromJson(userJson, User.class);
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

}
