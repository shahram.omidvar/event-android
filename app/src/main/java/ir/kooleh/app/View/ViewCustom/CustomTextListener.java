package ir.kooleh.app.View.ViewCustom;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;

import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Enum.ListPermission;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceGetPermissions;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceTextOnListener;
import ir.kooleh.app.Structure.StaticValue.SvInputTextAlignment;
import ir.kooleh.app.Utility.Class.ShowPermissionDialog;


public class CustomTextListener extends RelativeLayout implements View.OnClickListener, RecognitionListener, InterfaceGetPermissions
{
    // view
    EditText etSearch;
    ImageButton ibVoice, ibSearch;
    ProgressBar prSearch;


    //var
    private static final int MICROPHONE_REQUEST_CODE = 101;
    private static final int SEARCH_REGION_CODE = 1111;
    private Context context;
    private Intent recognizerIntent;
    private SpeechRecognizer speech;
    private static final String TAG = "SearchCustom";
    InterfaceTextOnListener interfaceTextOnListener;
    private Boolean isSearching = false;
    private ShowPermissionDialog permissionDialog;


    public CustomTextListener(Context context)
    {
        super(context);
        this.context = context;
        initialize(context);
    }

    public CustomTextListener(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.context = context;
        initialize(context);
        permissionDialog = new ShowPermissionDialog((Activity) context, this);

    }

    private void initialize(Context context)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_listener_text_custom, this, true);

        etSearch = view.findViewById(R.id.et_search_custom);
        ibVoice = view.findViewById(R.id.ib_mic_search_custom);
        ibSearch = view.findViewById(R.id.ib_search_custom);
        prSearch = view.findViewById(R.id.prb_search);

        ibVoice.setOnClickListener(this);
        ibSearch.setOnClickListener(this);

        setEditText();
    }

    public String getText()
    {
        return etSearch.getText().toString();
    }

    public void setText(String text)
    {
        etSearch.setText(text);
    }

    public void setInterface(InterfaceTextOnListener interfaceTextOnListener)
    {
        this.interfaceTextOnListener = interfaceTextOnListener;
    }

    public void setInputType(int inputType)
    {
        etSearch.setInputType(inputType);
    }

    public void setTextAlignment(int textAlignment)
    {
        if (textAlignment == SvInputTextAlignment.INHERIT)
            etSearch.setTextAlignment(TEXT_ALIGNMENT_INHERIT);

        if (textAlignment == SvInputTextAlignment.GRAVITY)
            etSearch.setTextAlignment(TEXT_ALIGNMENT_GRAVITY);


        if (textAlignment == SvInputTextAlignment.TEXT_END)
            etSearch.setTextAlignment(TEXT_ALIGNMENT_TEXT_END);


        if (textAlignment == SvInputTextAlignment.CENTER)
            etSearch.setTextAlignment(TEXT_ALIGNMENT_CENTER);

        if (textAlignment == SvInputTextAlignment.VIEW_START)
            etSearch.setTextAlignment(TEXT_ALIGNMENT_VIEW_START);

        if (textAlignment == SvInputTextAlignment.VIEW_END)
            etSearch.setTextAlignment(TEXT_ALIGNMENT_VIEW_END);


    }


    private void setEditText()
    {
        etSearch.setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {

                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode)
                    {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                        case KeyEvent.KEYCODE_SEARCH:
                        {
                            onResult();
                            return true;
                        }

                        default:
                            break;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v)
    {
        if (v == ibVoice)
        {
            Listener();
        }
        else if (v == ibSearch)
        {
            isSearching = false;
            onResult();

        }
    }

    private void onResult()
    {

        if (!isSearching)
        {
            if (interfaceTextOnListener != null)
                interfaceTextOnListener.onResultTextOnListener(true, etSearch.getText().toString().trim());
        }


    }

    private void Listener()
    {
        configRecognizer();
        checkMicrophoneEnable();
        speech.startListening(recognizerIntent);
    }

    private void configRecognizer()
    {
        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "fa-IR");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, context.getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "fa-IR");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "fa-IR");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, context.getPackageName());
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 0);
        speech = SpeechRecognizer.createSpeechRecognizer(context);
        speech.setRecognitionListener(this);
    }

    private void checkMicrophoneEnable()
    {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
        {
            permissionDialog.getPermissionFromUser(ListPermission.RECORD_AUDIO);
        }
    }

    @Override
    public void onReadyForSpeech(Bundle params)
    {
        isSearching = true;
        ibSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_close_black));
        etSearch.setHint("اکنون صحبت کنید ...");
        etSearch.setText("");
        Toast.makeText(context, "لطفا صحبت کنید", Toast.LENGTH_SHORT).show();
        ibVoice.setColorFilter(ContextCompat.getColor(context, R.color.blue_300), android.graphics.PorterDuff.Mode.SRC_IN);
    }

    @Override
    public void onBeginningOfSpeech()
    {
        Log.d(TAG, "onBeginningOfSpeech: ");
    }

    @Override
    public void onRmsChanged(float rmsdB)
    {

    }

    @Override
    public void onBufferReceived(byte[] buffer)
    {

    }

    @Override
    public void onEndOfSpeech()
    {
        ibVoice.setColorFilter(ContextCompat.getColor(context, R.color.gray_650), android.graphics.PorterDuff.Mode.SRC_IN);
        etSearch.setText("");
        etSearch.setHint("");
    }

    @Override
    public void onError(int error)
    {
        ibVoice.setColorFilter(ContextCompat.getColor(context, R.color.gray_650), android.graphics.PorterDuff.Mode.SRC_IN);
        Toast.makeText(context, getErrorTextFarsi(error), Toast.LENGTH_SHORT).show();
        etSearch.setText("");
        etSearch.setHint("");
    }

    @Override
    public void onResults(Bundle results)
    {
        ibVoice.setColorFilter(ContextCompat.getColor(context, R.color.gray_650), android.graphics.PorterDuff.Mode.SRC_IN);
        ArrayList<String> matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        String text = "";
        if (matches != null)
        {
            for (String result : matches)
            {
                if (result != null || result.equals("null"))
                    text += result;
                break;
            }
        }
        etSearch.setText(text);
        isSearching = false;
        ibSearch.setImageDrawable(getResources().getDrawable(R.drawable.ic_search));
    }

    @Override
    public void onPartialResults(Bundle partialResults)
    {

    }

    @Override
    public void onEvent(int eventType, Bundle params)
    {

    }

    public static String getErrorText(int errorCode)
    {
        String message;
        switch (errorCode)
        {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "ClientPageFragment side error";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }

    public String getErrorTextFarsi(int errorCode)
    {
        String message;
        switch (errorCode)
        {
            case SpeechRecognizer.ERROR_AUDIO:
                message = "خطا در ضبط صدا.";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                message = "خطای گوشی.";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "دسترسی ضبط صدا موجود نیست.";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                message = "خطای اینترنت.";
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "وقفه در اینترنت.";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                message = "متوجه نشدم. دوباره صحبت کنید.";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "سرور مشغول است.";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                message = "خطای سرور.";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "ورودی صدا پیدا نشد.";
                break;
            default:
                message = "متوجه نشدم. دوباره صحبت کنید.";
                break;
        }
        return message;
    }


    @Override
    public void onResultRequestPermissions(boolean isOk, String message, int CodePermission)
    {
        if (!isOk)
        {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }

        else
        {
            Toast.makeText(context, message + " لطفا دوباره انتخاب کنید", Toast.LENGTH_LONG).show();

        }
    }
}