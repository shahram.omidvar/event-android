package ir.kooleh.app.View.Ui.Dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceCustomPreviewAd;
import ir.kooleh.app.Structure.Model.Ads.ModelPreviewAd;
import ir.kooleh.app.Utility.Adapter.Ads.Create.AdapterPreviewAd;


public class CustomDialogPreviewAd implements View.OnClickListener
{
    private AlertDialog dialog;
    private CardView btnConfirm, btnClose;
    private RecyclerView rvPreviewAd;

    private InterfaceCustomPreviewAd interfaceCustomPreviewAd;
    private String jsonItem;

    public boolean isShowing;

    public void make(Activity activity, List<ModelPreviewAd> modelPreviewAdList, String jsonItem)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        View inflater = LayoutInflater.from(activity).inflate(R.layout.custom_dialog_preview_ad, null);
        builder.setView(inflater);
        dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        interfaceCustomPreviewAd = (InterfaceCustomPreviewAd) activity;
        initView(inflater, activity);
        initVars(activity, modelPreviewAdList, jsonItem);

        dialog.setOnKeyListener((dialog, keyCode, event) ->
        {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP && !event.isCanceled())
            {
                dismiss();
                return true;
            }
            return false;
        });

        dialog.setCanceledOnTouchOutside(false);
    }



    private void initView(View view, Activity activity)
    {
        btnConfirm = view.findViewById(R.id.btn_confirm_preview_ad);
        btnClose = view.findViewById(R.id.btn_close_preview_ad);
        rvPreviewAd = view.findViewById(R.id.rv_preview_ad);

        rvPreviewAd.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));

        btnConfirm.setOnClickListener(this);
        btnClose.setOnClickListener(this);
    }

    private void initVars(Activity activity, List<ModelPreviewAd> modelPreviewAdList, String jsonItem)
    {
        this.jsonItem = jsonItem;
        AdapterPreviewAd adapterPreviewAd = new AdapterPreviewAd(activity);
        rvPreviewAd.setAdapter(adapterPreviewAd);
        adapterPreviewAd.setList(modelPreviewAdList);
    }




    public void show()
    {
        if (dialog != null)
        {
            dismiss();
            dialog.show();
        }
    }


    public void dismiss()
    {
        if (dialog.isShowing())
        {
            dialog.dismiss();
            interfaceCustomPreviewAd = null;
            btnConfirm = null;
            dialog = null;
            btnClose = null;
        }
    }

    @Override
    public void onClick(View view)
    {

        if (view == btnConfirm)
        {
            interfaceCustomPreviewAd.onConfirmCustomPreviewAd(jsonItem);
        }
        else if (view == btnClose)
        {
            dismiss();
        }
    }
}
