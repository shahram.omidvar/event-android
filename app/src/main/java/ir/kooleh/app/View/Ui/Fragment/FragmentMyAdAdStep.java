package ir.kooleh.app.View.Ui.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.paginate.Paginate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Dashboard.ApiDashboard;
import ir.kooleh.app.Structure.Enum.EnumMessageType;
import ir.kooleh.app.Structure.Enum.PaginateValues;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceTryAgainPaginate;
import ir.kooleh.app.Structure.Interface.Dashboard.InterfaceDashboard;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Dashboard.ModelMyAd;
import ir.kooleh.app.Utility.Adapter.Dashboard.AdapterMyAdStep;
import ir.kooleh.app.Utility.Adapter.Dashboard.AdapterViewPagerMyAds;
import ir.kooleh.app.Utility.Class.CustomLayoutPaginate;
import ir.kooleh.app.Utility.Class.DashboardUtils;
import ir.kooleh.app.Utility.Class.InterClassDataPassKeys;
import ir.kooleh.app.Utility.Class.MessageHandlerUtils;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class FragmentMyAdAdStep extends Fragment implements
        InterfaceDashboard.Apis.IntGetMyAds,
        InterfaceDashboard.Apis.IntGetMyOffers,
        InterfaceMessageHandler.FragmentResult,
        InterfaceTryAgainPaginate,
        Paginate.Callbacks
{
    private View view;
    private RecyclerView rvMyAdStep;
    private AdapterMyAdStep adapterMyAdStep;
    private ApiDashboard apiDashboard;
    private FragmentMessageHandler fragmentMessageHandler;
    private FrameLayout containerFragmentMessageHandler;
    private SwipeRefreshLayout swipeRefresh;

    private InterfaceDashboard.InterClassData.IntChangeTabPositionInMyAds intChangeTabPositionInMyAds;

    private ViewLoading viewLoading;

    private int position;
    private int adType;

    private List<ModelMyAd> myAdList = new ArrayList<>();
    private ModelPaginate modelPaginate;
    private Paginate paginate;
    private boolean loading = false;
    private int nextPage = 1;

    private Parcelable recylerViewState;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_my_ad_step, container, false);
        initViews();
        initVars();

        return view;
    }

    private void initViews()
    {
        rvMyAdStep = view.findViewById(R.id.rv_my_ad_step);
        containerFragmentMessageHandler = view.findViewById(R.id.container_message_handler_fragment);
        swipeRefresh = view.findViewById(R.id.swipe_refresh_fragment_my_step_ad);
        swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.primary), getResources().getColor(R.color.color_primary));
    }

    private void initVars()
    {
        apiDashboard = new ApiDashboard(getContext());
        viewLoading = new ViewLoading(getContext());

        intChangeTabPositionInMyAds = (InterfaceDashboard.InterClassData.IntChangeTabPositionInMyAds) getActivity();

        position = getArguments().getInt(AdapterViewPagerMyAds.POSITION, 0);
        adType = getArguments().getInt(AdapterViewPagerMyAds.AD_TYPE_VALUE, 9);

        rvMyAdStep.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        adapterMyAdStep = new AdapterMyAdStep(getContext(), this, adType, position);
        rvMyAdStep.setAdapter(adapterMyAdStep);

        swipeRefresh.setOnRefreshListener(() ->
        {
            resetList();
        });

    }


    private void getListByPageType()
    {
        if (adType == 9 || adType == 10)
        {
            apiDashboard.getMyAds(this,
                    adType,
                    DashboardUtils.getAdStepByViewpagerPositionAndType(adType, position),
                    DashboardUtils.getArchiveByViewpagerPositionAndType(adType, position),
                    nextPage,
                    PaginateValues.itemsPerPage);
        }
        else if (adType == 11)
        {
            apiDashboard.getMyOffers(this,
                    DashboardUtils.getArchiveByViewpagerPositionAndType(adType, position),
                    DashboardUtils.getAdStepByViewpagerPositionAndType(adType, position),
                    nextPage,
                    PaginateValues.itemsPerPage);
        }
    }


    private void checkForEmptyList()
    {
        if (myAdList != null && myAdList.size() == 0)
        {
            fragmentMessageHandler = new FragmentMessageHandler(this);
            Bundle args = new Bundle();
            args.putString("message", "آیتمی برای نمایش پیدا نشد");
            args.putSerializable(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE, EnumMessageType.EMPTY);
            fragmentMessageHandler.setArguments(args);
            getActivity().getSupportFragmentManager().beginTransaction().replace(
                    R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
            containerFragmentMessageHandler.setVisibility(View.VISIBLE);
        }
    }

    private void showError(String message)
    {
        fragmentMessageHandler = new FragmentMessageHandler(this);
        Bundle args = new Bundle();
        args.putString("message", message);
        args.putSerializable(InterClassDataPassKeys.FragmentMessageHandler.MESSAGE_TYPE, MessageHandlerUtils.getMessageTypeByMessage(message));
        fragmentMessageHandler.setArguments(args);
        getActivity().getSupportFragmentManager().beginTransaction().replace(
                    R.id.container_message_handler_fragment, fragmentMessageHandler).commitAllowingStateLoss();
        containerFragmentMessageHandler.setVisibility(View.VISIBLE);
    }


    @Override
    public void setMenuVisibility(boolean menuVisible)
    {
        super.setMenuVisibility(menuVisible);
        if (menuVisible)
        {
            if (paginate != null)
            {
                paginate.unbind();
                paginate = null;
            }
            nextPage = 1;
            myAdList = new ArrayList<>();
            adapterMyAdStep = new AdapterMyAdStep(getContext(), this, adType, position);
            rvMyAdStep.setAdapter(adapterMyAdStep);
            fragmentMessageHandler = null;
            recylerViewState = null;
            viewLoading.show();
            getListByPageType();
        }
    }

    @Override
    public void onResponseGetMyAds(boolean response, List<ModelMyAd> myAdList, ModelPaginate modelPaginate, String message)
    {
        viewLoading.close();
        containerFragmentMessageHandler.setVisibility(View.GONE);
        swipeRefresh.setRefreshing(false);

        if (response)
        {
            loading = false;
            this.myAdList.addAll(myAdList);
            this.modelPaginate = modelPaginate;
            if (adType == 10 && position == 0 && containsChooseWinner(myAdList, "انتخاب برنده"))
            {
                resetList();
            }
            checkForEmptyList();
            adapterMyAdStep.setList(myAdList);
            if (paginate == null)
            {
                paginate = Paginate.with(rvMyAdStep, this)
                        .setLoadingTriggerThreshold(PaginateValues.threshold)
                        .addLoadingListItem(true)
                        .setLoadingListItemCreator(new CustomLayoutPaginate())
                        .setLoadingListItemSpanSizeLookup(() -> PaginateValues.itemsPerPage)
                        .build();
                rvMyAdStep.getLayoutManager().onRestoreInstanceState(recylerViewState);
            }
        }
        else
        {
            if (nextPage == 1)
            {
                showError(message);
            }
            else
            {
                adapterMyAdStep.showBtnTryAgain();
                if (paginate != null)
                {
                    paginate.unbind();
                }
            }
        }
    }


    public static boolean containsChooseWinner(Collection<ModelMyAd> c, String messageImage) {
        for (ModelMyAd o : c) {
            if(o != null && o.getMessageImage().equals(messageImage)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onResponseGetMyOffers(boolean response, List<ModelMyAd> myAdList, ModelPaginate modelPaginate, String message)
    {
        viewLoading.close();
        containerFragmentMessageHandler.setVisibility(View.GONE);
        swipeRefresh.setRefreshing(false);

        if (response)
        {
            loading = false;
            this.myAdList.addAll(myAdList);
            this.modelPaginate = modelPaginate;
            checkForEmptyList();
            adapterMyAdStep.setList(myAdList);
            if (paginate == null)
            {
                paginate = Paginate.with(rvMyAdStep, this)
                        .setLoadingTriggerThreshold(PaginateValues.threshold)
                        .addLoadingListItem(true)
                        .setLoadingListItemCreator(new CustomLayoutPaginate())
                        .setLoadingListItemSpanSizeLookup(() -> PaginateValues.itemsPerPage)
                        .build();
                rvMyAdStep.getLayoutManager().onRestoreInstanceState(recylerViewState);
            }
        }
        else
        {
            if (nextPage == 1)
            {
                showError(message);
            }
            else
            {
                adapterMyAdStep.showBtnTryAgain();
                if (paginate != null)
                {
                    paginate.unbind();
                }
            }
        }
    }

    @Override
    public void onResponseMessageHandler()
    {
        getActivity().getSupportFragmentManager().beginTransaction().remove(fragmentMessageHandler).commit();
        fragmentMessageHandler = null;
        viewLoading.show();
        getListByPageType();
    }


    @Override
    public void onDetach()
    {
        fragmentMessageHandler = null;
        super.onDetach();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1)
        {
            resetList();
        }
        if (resultCode == 2)
        {
            getListByPageTypeAndChangePage(1);
        }
        if (resultCode == 3)
        {
            getListByPageTypeAndChangePage(2);
        }
    }

    private void getListByPageTypeAndChangePage(int tabPosition)
    {
        intChangeTabPositionInMyAds.onResponseIntChangeTabPositionInMyAds(tabPosition);
    }

    @Override
    public void onLoadMore()
    {
        Log.i("paginateOnloadMore", String.valueOf(myAdList.size()));
        loading = true;
        nextPage++;
        getListByPageType();
    }

    @Override
    public boolean isLoading()
    {
        Log.i("paginateisloading", String.valueOf(loading));
        return loading;
    }

    @Override
    public boolean hasLoadedAllItems()
    {
        if (modelPaginate != null)
        {
            Log.i("paginateHasloadedAll", String.valueOf(myAdList.size() == Integer.parseInt(modelPaginate.getTotal())));
            return myAdList.size() == Integer.parseInt(modelPaginate.getTotal());
        }
        return  false;
    }

    private void resetList()
    {
        adapterMyAdStep = new AdapterMyAdStep(getContext(), this, adType, position);
        rvMyAdStep.setAdapter(adapterMyAdStep);
        myAdList = new ArrayList<>();
        recylerViewState = rvMyAdStep.getLayoutManager().onSaveInstanceState();

        if (paginate != null)
        {
            paginate.unbind();
            paginate = null;
        }
        nextPage = 1;
        viewLoading.show();
        getListByPageType();
    }

    @Override
    public void onResponseTryAgainPaginate()
    {
        paginate = null;
        recylerViewState = rvMyAdStep.getLayoutManager().onSaveInstanceState();
        getListByPageType();
    }
}
