package ir.kooleh.app.View.Ui.Activity.Tools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.kooleh.app.R;

public class GroupTypeAdapter extends RecyclerView.Adapter<GroupTypeAdapter.AdapterHolder> {

    private Context context;
    private List<Group> list;
    private OnClickItemListener itemListener ;

    public GroupTypeAdapter(Context context , List<Group> list , OnClickItemListener itemListener) {

        this.context = context;
        this.list = list;
        this.itemListener = itemListener;
    }

    @Override
    public AdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        final AdapterHolder viewHolder = new AdapterHolder(LayoutInflater.from(context).inflate(R.layout.item_group_service, parent , false));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final AdapterHolder holder, int position) {
        final Group item = list.get(position);
        if (item.getImage()!=null && !item.getImage().equals(""))
            Glide.with(context).load(item.getImage()).error(R.drawable.img_splash_gibar).placeholder(R.drawable.img_splash_gibar).into(holder.ivGroup);
        else
            Glide.with(context).load(R.drawable.img_splash_gibar).into(holder.ivGroup);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, item.getImage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class AdapterHolder extends RecyclerView.ViewHolder {

        private ImageView ivGroup;

        public AdapterHolder(View itemView) {
            super(itemView);
            ivGroup = (ImageView) itemView.findViewById(R.id.iv_group_service_item);
        }
    }

    public interface OnClickItemListener{
        void onClickItem(View view, String item);
    }
}
