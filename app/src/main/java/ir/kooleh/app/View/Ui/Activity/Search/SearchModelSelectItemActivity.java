package ir.kooleh.app.View.Ui.Activity.Search;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Search.ApiSearchModel;
import ir.kooleh.app.Structure.Interface.Search.InterfaceSearch;
import ir.kooleh.app.Structure.Model.Search.ModelSearchItem;
import ir.kooleh.app.Utility.Adapter.Search.AdapterSearchModelSelectItem;
import ir.kooleh.app.Utility.Class.CustomToast;

public class SearchModelSelectItemActivity extends AppCompatActivity implements InterfaceSearch.SelectItem, View.OnClickListener, View.OnFocusChangeListener, TextWatcher, TextView.OnEditorActionListener, AdapterView.OnItemClickListener
{

    public static final String SELECT_TABLE = "SELECT_TABLE";
    public static final String SELECT_TITLE = "SELECT_TITLE";
    public static final String JSON_RESULT = "JSON_RESULT";


    // view
    private Intent intent;
    private CoordinatorLayout coordinatorLayoutSearchModelSelectItem;
    private TextView tvToolbarSelectItem, tvResultSelectItem;
    private EditText etSearchSelectItem;
    private ImageView ivBackSelectItem, ivSearchSelectItem;
    private RecyclerView rvSelectItem;
//    private ProgressWheel prbLoadingSelectItem;

    //ver
    private AdapterSearchModelSelectItem adapterSearchModelSelectItem;
    private String table = "-1", title = "-1";
    private ApiSearchModel apiSearch;
    private CustomToast customToast;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_model_select_item);
        getIntentDate();
        initView();
        createVar();

        tvToolbarSelectItem.setText(title);
        tvResultSelectItem.setText("");
        searchText("");

    }

    private void initView()
    {
        coordinatorLayoutSearchModelSelectItem = (CoordinatorLayout) findViewById(R.id.coordinator_select_item);
        tvToolbarSelectItem = findViewById(R.id.tv_title_toolbar);
        tvResultSelectItem = findViewById(R.id.tv_result_select_item);

        etSearchSelectItem = findViewById(R.id.et_search_select_item);
        ivSearchSelectItem = findViewById(R.id.iv_search_select_item);
        ivBackSelectItem = findViewById(R.id.iv_back_toolbar);

        rvSelectItem = findViewById(R.id.rv_select_item);
//        prbLoadingSelectItem = findViewById(R.id.prb_loading_select_item);

        ivBackSelectItem.setOnClickListener(this);
        ivSearchSelectItem.setOnClickListener(this);

        etSearchSelectItem.addTextChangedListener(this);
    }

    private void createVar()
    {
        context = SearchModelSelectItemActivity.this;
        apiSearch = new ApiSearchModel(context);

        rvSelectItem.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rvSelectItem.setAdapter(adapterSearchModelSelectItem = new AdapterSearchModelSelectItem(this, this));

    }


    private void getIntentDate()
    {
        intent = getIntent();
        Bundle bundle = getIntent().getExtras();

        title = bundle.getString(SELECT_TITLE, "-1");
        table = bundle.getString(SELECT_TABLE, "-1");

        if (table.equals("-1") || title.equals("-1"))
        {
            Toast.makeText(this, "خطا در ارسال دیتا ها", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

    }

    @Override
    public void onClick(View v)
    {
        if (v == ivBackSelectItem)
        {
            setResult(RESULT_CANCELED);
            finish();
        }
        else if (v == ivSearchSelectItem)
        {
            searchText(etSearchSelectItem.getText().toString());
        }
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {

    }

    @Override
    public void afterTextChanged(Editable s)
    {
        if (s.length() >= 1)
        {
            searchText(s.toString());
        }
        else
            searchText("");

    }


    //
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
    {
        if (actionId == 3 && !etSearchSelectItem.getText().toString().equals(""))
        {
            etSearchSelectItem.clearFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etSearchSelectItem.getWindowToken(), 0);
            searchText(etSearchSelectItem.getText().toString());
        }
        return false;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus)
    {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {

    }


    private void searchText(final String text)
    {
//        prbLoadingSelectItem.setVisibility(View.VISIBLE);
        rvSelectItem.setVisibility(View.GONE);
        apiSearch.SearchModel
                (
                        this,
                        table,
                        text
                );
    }


    @Override
    public void onResponseSearchModelItem(boolean response, List<ModelSearchItem.Result> listSearch, String message)
    {
//        prbLoadingSelectItem.setVisibility(View.GONE);

        if (response)
        {
            if (listSearch.size() == 0)
                tvResultSelectItem.setText("نتیجه ای یافت نشد");
            else
            {
                rvSelectItem.setVisibility(View.VISIBLE);
                tvResultSelectItem.setText(String.format("%s نتیجه پیدا شد ", listSearch.size()));
                adapterSearchModelSelectItem.setListSearchItem(listSearch);
            }

        }
        else
        {
            customToast = new CustomToast(this);
            customToast.showError(message);
        }
    }

    @Override
    public void onClickListSearchSelectItem(ModelSearchItem.Result searchItem)
    {
        intent.putExtra(JSON_RESULT, new Gson().toJson(searchItem));
        setResult(RESULT_OK, intent);
        onBackPressed();
    }


}