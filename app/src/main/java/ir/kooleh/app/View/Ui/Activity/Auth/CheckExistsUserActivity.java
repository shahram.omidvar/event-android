package ir.kooleh.app.View.Ui.Activity.Auth;

import androidx.appcompat.app.AppCompatActivity;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.os.Handler;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.User.ApiUser;

import ir.kooleh.app.Structure.Enum.EnumListCommendApi;
import ir.kooleh.app.Structure.Enum.EnumVerifyCode;

import ir.kooleh.app.Structure.Interface.User.InterfaceUser;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.Utility.Class.SettingKeyboard;
import ir.kooleh.app.View.Ui.Activity.general.WebViewActivity;
import ir.kooleh.app.View.ViewCustom.CButton;
import ir.kooleh.app.View.ViewCustom.CEditText;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;


public class CheckExistsUserActivity extends AppCompatActivity implements
        View.OnClickListener,
        InterfaceUser.checkExistsUser
{


    private CEditText etPhoneNumber;
    private CButton btnNext;
    private TextInputLayout txiCheckPhoneNumber;
    private CTextView tvPrivacyPolicy;

    private ViewLoading viewLoading;
    private CustomToast customToast;
    private ApiUser apiUser;
    private Intent intent;

    private int backCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_exists_user);

        findView();
        createVar();

        SettingKeyboard.showKeyboard(this, etPhoneNumber);
    }


    @SuppressLint("ClickableViewAccessibility")
    private void findView()
    {
        etPhoneNumber = findViewById(R.id.et_phone_number_check_exists_user);
        btnNext = findViewById(R.id.btn_check_user);
        txiCheckPhoneNumber = findViewById(R.id.txi_check_phone_number);
        tvPrivacyPolicy = findViewById(R.id.tv_privacy_policy);
        btnNext.setOnClickListener(this);

        String text = "با ورود یا ثبت نام در سامانه راهنما، ضمن مطالعه ";
        String text3 = getResources().getString(R.string.kooleh_privacy_policy);
        String text2 = text + text3 + " آن ها را میپذیرید";

        Spannable spannable = new SpannableString(text2);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent = new Intent(CheckExistsUserActivity.this, WebViewActivity.class);
                intent.putExtra("value", "https://kooleh.ir");
                intent.putExtra(WebViewActivity.KEY_TITLE_TOOLBAR, "قوانین و شرایط راهنما");
                intent.putExtra(WebViewActivity.KEY_NO_TOOLBAR, true);
                startActivity(intent);
            }
        };
        spannable.setSpan(clickableSpan, text.length(), (text.length() + text3.length()), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.blue_500)), text.length(), (text.length() + text3.length()), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new UnderlineSpan(), text.length(), (text.length() + text3.length()), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        tvPrivacyPolicy.setText(spannable, TextView.BufferType.SPANNABLE);
        tvPrivacyPolicy.setMovementMethod(LinkMovementMethod.getInstance());


        etPhoneNumber.setOnTouchListener((v, event) -> { txiCheckPhoneNumber.setErrorEnabled(false);return false; });

        etPhoneNumber.addTextChangedListener(new TextWatcher()
        {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override public void afterTextChanged(Editable s) { if (s.length() > 0) { txiCheckPhoneNumber.setErrorEnabled(false); } }
        });


        etPhoneNumber.setOnEditorActionListener((v, actionId, event) ->
        {
            if (actionId == EditorInfo.IME_ACTION_DONE)
            {
                if (checkPhoneNumber())
                {
                    loginToApp();
                }
                return true;
            }
            return false;
        });

    }


    private void createVar()
    {
        Context context = CheckExistsUserActivity.this;
        viewLoading = new ViewLoading(context);
        apiUser = new ApiUser(context);
    }


    @Override
    public void onBackPressed()
    {
        backCount++;
        if (backCount == 2){
            finish();}
        else
        {
            customToast = new CustomToast(this);
            customToast.showHelp("برای خروج از برنامه دوباره دکمه بازگشت را کلیک کنید");
            new Handler().postDelayed(() -> backCount = 0, 3500);
        }
    }

    @Override
    public void onClick(View view)
    {
        if (view == btnNext)
        {
            if (checkPhoneNumber())
            {
                loginToApp();
            }
        }

    }

    private void loginToApp()
    {
        SettingKeyboard.hideKeyboard(this);
        viewLoading.show();
        apiUser.checkExistsUser(this, etPhoneNumber.getText().toString());
    }

    private boolean checkPhoneNumber()
    {
        if (etPhoneNumber.getText() != null)
        {
            String phoneNumber = etPhoneNumber.getText().toString();
            if (phoneNumber.length() != 11)
            {
                customToast = new CustomToast(this);
                customToast.showError("لطفا شماره همراه خود را وارد کنید (11 رقم)");
                txiCheckPhoneNumber.setError("لطفا شماره همراه خود را وارد کنید (11 رقم)");
                return false;
            }

            return true;
        }
        return false;
    }


    @Override
    public void onResponseCheckExistsUser(boolean ok, String message, String commend, int lengthCode)
    {
        viewLoading.close();
        if (ok)
        {
            customToast = new CustomToast(this);
            customToast.showSuccess(message);
            if (commend.equals(EnumListCommendApi.Login))
            {
                intent = new Intent(CheckExistsUserActivity.this, LoginActivity.class);
                intent.putExtra(LoginActivity.PHONE_NUMBER, etPhoneNumber.getText().toString());
                startActivity(intent);
            }
            else if (commend.equals(EnumListCommendApi.VerifyRegister))
            {
                intent = new Intent(CheckExistsUserActivity.this, VerifyCodeActivity.class);
                intent.putExtra(VerifyCodeActivity.PHONE_NUMBER, etPhoneNumber.getText().toString());
                intent.putExtra(VerifyCodeActivity.TYPE, EnumVerifyCode.Register);
                intent.putExtra(VerifyCodeActivity.LENGTH_CODE, lengthCode);
                startActivity(intent);
            }

            else
            {
                customToast = new CustomToast(this);
                customToast.showError("خطا این ساختار پیدا نشد");
            }
        }

        else
        {
            customToast = new CustomToast(this);
            customToast.showError(message);
        }

    }

}