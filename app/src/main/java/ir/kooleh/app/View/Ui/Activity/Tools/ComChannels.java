
package ir.kooleh.app.View.Ui.Activity.Tools;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ComChannels {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("telegram")
    @Expose
    private Object telegram;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("fk")
    @Expose
    private String fk;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("insta")
    @Expose
    private String insta;
    @SerializedName("tel_number")
    @Expose
    private String telNumber = null;//todo fix tel iS sTRING NO LIST
    @SerializedName("other")
    @Expose
    private Object other;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getTelegram() {
        return telegram;
    }

    public void setTelegram(Object telegram) {
        this.telegram = telegram;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getFk() {
        return fk;
    }

    public void setFk(String fk) {
        this.fk = fk;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getInsta() {
        return insta;
    }

    public void setInsta(String insta) {
        this.insta = insta;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    public Object getOther() {
        return other;
    }

    public void setOther(Object other) {
        this.other = other;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
