package ir.kooleh.app.View.Ui.Activity.Ads;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.MultiSelection.ApiMultiSelectionActivity;
import ir.kooleh.app.Structure.Interface.MultiSelection.InterfaceMultiSelection;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Model.MultiSelection.ModelMultiSelection;
import ir.kooleh.app.Utility.Adapter.MultiSelection.AdapterMultiSelection;
import ir.kooleh.app.View.Ui.Fragment.FragmentMessageHandler;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class MultiSelectionActivity extends AppCompatActivity
        implements View.OnClickListener,
        InterfaceMultiSelection.Server,
        InterfaceMultiSelection.Adapter,
        InterfaceMessageHandler.FragmentResult
{

    public static final String MULTI_SELECTION_RESULT = "result";
    public static final String KEY_ACTIVITY_NAME = "activityName";
    public static final String HOME_ACTIVITY_NAME = "homeActivity";

    // view
    private CTextView tvToolbar;
    private ImageView ivBackToolbar;
    private RecyclerView rvMultiSelection;
    private ImageView ivBackground;
    private FragmentMessageHandler fragmentMessageHandler;
    // private Intent intent;


    //var
    private ViewLoading viewLoading;
    private ApiMultiSelectionActivity apiMultiSelectionActivity;
    private Context context;
    private AdapterMultiSelection adapterMultiSelection;

    private String parentId = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_selection);

        findView();
        crateVar();
        getListMultiSelection(parentId);
    }

    private void findView()
    {
        tvToolbar = findViewById(R.id.tv_title_toolbar);
        ivBackToolbar = findViewById(R.id.iv_back_toolbar);
        rvMultiSelection = findViewById(R.id.rv_multi_selection);
        ivBackground = findViewById(R.id.iv_background_multi_selection);

        ivBackToolbar.setOnClickListener(this);
    }

    private void crateVar()
    {
        this.context = MultiSelectionActivity.this;
        tvToolbar.setText("دسته بندی ها");
        viewLoading = new ViewLoading(context);
        apiMultiSelectionActivity = new ApiMultiSelectionActivity(context);
        adapterMultiSelection = new AdapterMultiSelection(context, this);

        rvMultiSelection.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        rvMultiSelection.setAdapter(adapterMultiSelection = new AdapterMultiSelection(context, this));
    }


    private void getListMultiSelection(String parentId)
    {
        this.parentId = parentId;
        viewLoading.show();
        apiMultiSelectionActivity.getListMultiSelection(this, parentId);
    }


    @Override
    public void onClick(View v)
    {
        if (v == ivBackToolbar)
        {
            finish();
        }
    }


    @Override
    public void onResponseListMultiSelection(boolean response, List<ModelMultiSelection> listMultiSelection, String message, String backgroundImage)
    {

        rvMultiSelection.setVisibility(View.GONE);
        viewLoading.close();
        if (response)
        {
            Glide.with(this).load(backgroundImage).into(ivBackground);
            adapterMultiSelection.setListAlert(listMultiSelection);
            rvMultiSelection.setVisibility(View.VISIBLE);
        }

        else
        {
            fragmentMessageHandler = new FragmentMessageHandler(this);
            Bundle args = new Bundle();
            args.putString("message", message);
            fragmentMessageHandler.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.container_message_handler_fragment, fragmentMessageHandler).commit();
        }
    }

    @Override
    public void onClickItemMultiSelection(ModelMultiSelection multiSelection)
    {
        if (!multiSelection.getParentId().equals("0"))
        {
            getListMultiSelection(multiSelection.getParentId());
        }
        else
        {
            startActivity(new Intent(this, CreateAdActivity.class).putExtra(MULTI_SELECTION_RESULT, new Gson().toJson(multiSelection)));
            finish();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onResponseMessageHandler()
    {
        getSupportFragmentManager().beginTransaction().remove(fragmentMessageHandler).commit();
        fragmentMessageHandler = null;
        getListMultiSelection(parentId);
    }
}