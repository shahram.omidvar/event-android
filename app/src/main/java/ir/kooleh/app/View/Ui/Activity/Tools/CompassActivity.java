package ir.kooleh.app.View.Ui.Activity.Tools;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ValueAnimator;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import ir.kooleh.app.R;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class CompassActivity extends AppCompatActivity implements View.OnClickListener
{
    private Compass compass;
    private ImageView arrowView;
    private ImageView baseView;
    private TextView sotwLabel;
    private CTextView tvTitle;
    private ImageView ivBack;
    // SOTW is for "side of the world"
    private static final String TAG = "Cannot invoke method";

    private float currentAzimuth;
    private SOTWFormatter sotwFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compass);

        sotwFormatter = new SOTWFormatter(this);

        arrowView = findViewById(R.id.main_image_hands);
        baseView = findViewById(R.id.main_image_base);
        sotwLabel = findViewById(R.id.sotw_label);
        tvTitle = findViewById(R.id.tv_title_toolbar);
        ivBack = findViewById(R.id.iv_back_toolbar);
        ivBack.setOnClickListener(this);
        tvTitle.setText("قطب نما");

        setupCompass();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "start compass");
        compass.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        compass.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        compass.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "stop compass");
        compass.stop();
    }

    private void setupCompass() {
        compass = new Compass(this);
        Compass.CompassListener cl = getCompassListener();
        compass.setListener(cl);
    }

    private void adjustArrow(float azimuth) {
        Log.d(TAG, "will set rotation from " + currentAzimuth + " to "
                + azimuth);

        Animation an = new RotateAnimation(-currentAzimuth, -azimuth,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        currentAzimuth = azimuth;

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(-currentAzimuth, - azimuth);
        valueAnimator.setInterpolator(null);

        valueAnimator.addUpdateListener(valueAnimator1 -> {
            baseView.setRotation((float) valueAnimator1.getAnimatedValue());
            baseView.requestLayout();
        });

        valueAnimator.setDuration(1);
        valueAnimator.start();

        an.setDuration(10);
        an.setRepeatCount(0);
        an.setFillAfter(true);

//        arrowView.startAnimation(an);
    }

    private void adjustSotwLabel(float azimuth) {
        sotwLabel.setText(sotwFormatter.format(azimuth));
    }

    private Compass.CompassListener getCompassListener() {
        return new Compass.CompassListener() {
            @Override
            public void onNewAzimuth(final float azimuth) {
                // UI updates only in UI thread
                // https://stackoverflow.com/q/11140285/444966
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adjustArrow(azimuth);
                        adjustSotwLabel(azimuth);
                    }
                });
            }
        };
    }

    @Override
    public void onClick(View v)
    {
        if (v == ivBack)
        {
            finish();
        }
    }
}