package ir.kooleh.app.View.Ui.Dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import androidx.cardview.widget.CardView;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Enum.EnCustomMessage;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceDialogVersionChecking;
import ir.kooleh.app.View.Ui.Activity.SplashScreen.SplashActivity;
import ir.kooleh.app.View.ViewCustom.CTextView;

public class CustomDialogVersionChecking implements View.OnClickListener
{
    private Activity activity;
    private AlertDialog.Builder dialogBuilder;
    private InterfaceDialogVersionChecking interfaceDialogVersionChecking;
    private AlertDialog alertDialog;
    private CardView btnOk, btnCancel;
    private CTextView tvBtnOk, tvBtnCancel;
    private CTextView tvMessage;
    private int requestCode = -1;
    private boolean closeDialog = true;


    public CustomDialogVersionChecking(Activity activity, InterfaceDialogVersionChecking interfaceDialogVersionChecking)
    {
        this.activity = activity;
        dialogBuilder = new AlertDialog.Builder(activity);
        this.interfaceDialogVersionChecking = interfaceDialogVersionChecking;
    }


    private void initView(View view)
    {
        btnOk = view.findViewById(R.id.btn_confirm_dialog_version_checking);
        btnCancel = view.findViewById(R.id.btn_refuse_dialog_version_checking);
        tvMessage = view.findViewById(R.id.tv_message_dialog_version_checking);
        tvBtnOk = view.findViewById(R.id.tv_btn_confirm_dialog_version_checking);
        tvBtnCancel = view.findViewById(R.id.tv_btn_refuse_dialog_version_checking);

    }


    public void showDialog(String message, String titleButtonOk, boolean showButtonCancel, int requestCode, boolean closeDialog)
    {
        this.requestCode = requestCode;
        this.closeDialog = closeDialog;
        View inflater = LayoutInflater.from(activity).inflate(R.layout.custom_dialog_version_checking, null);
        dialogBuilder.setView(inflater);
        alertDialog = dialogBuilder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogBuilder.setCancelable(closeDialog);
        initView(inflater);

        tvBtnOk.setText(titleButtonOk);
        tvMessage.setText(message);

        dialogBuilder.setOnKeyListener((dialog, keyCode, event) ->
        {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP && !event.isCanceled())
            {
                if (closeDialog)
                {
                    interfaceDialogVersionChecking.onClickDialogVersionChecking("", EnCustomMessage.NO, -1);
                    alertDialog.dismiss();
                }
                else
                {
                    interfaceDialogVersionChecking.onClickDialogVersionChecking("", EnCustomMessage.NO, SplashActivity.REQUEST_CODE_CLOSE_APP);
                }
                return true;
            }
            return false;
        });

        btnOk.setOnClickListener(this);
        if (showButtonCancel)
        {
            btnCancel.setVisibility(View.VISIBLE);
            btnCancel.setOnClickListener(this);
        }

        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }


    public void showDialogOK(String message,  String titleButtonOk, int requestCode ,  boolean closeDialog)

    {
        this.requestCode = requestCode;
        this.closeDialog = closeDialog;
        View inflater = LayoutInflater.from(activity).inflate(R.layout.custom_dialog_version_checking, null);
        dialogBuilder.setView(inflater);
        alertDialog = dialogBuilder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogBuilder.setCancelable(closeDialog);
        initView(inflater);

        tvBtnOk.setText(titleButtonOk);
        tvMessage.setText(message);

        dialogBuilder.setOnKeyListener((dialog, keyCode, event) ->
        {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP && !event.isCanceled())
            {
                if (closeDialog)
                {
                    interfaceDialogVersionChecking.onClickDialogVersionChecking("", EnCustomMessage.NO, -1);
                    alertDialog.dismiss();
                }
                else
                {
                    interfaceDialogVersionChecking.onClickDialogVersionChecking("", EnCustomMessage.NO, SplashActivity.REQUEST_CODE_CLOSE_APP);
                }
                return true;
            }
            return false;
        });

        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnCancel.setVisibility(View.GONE);

        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }


    @Override
    public void onClick(View v)
    {
        if (v == btnOk)
        {
            interfaceDialogVersionChecking.onClickDialogVersionChecking("", EnCustomMessage.YES, requestCode);
            if (closeDialog || requestCode == SplashActivity.REQUEST_CODE_TRY_AGAIN)
            {
                alertDialog.dismiss();
            }
        }
        else if (v == btnCancel)
        {
            interfaceDialogVersionChecking.onClickDialogVersionChecking("", EnCustomMessage.NO, requestCode);
            alertDialog.dismiss();
        }
    }

    public void setMessage(String message)
    {
        if (message != null && message.length() == 0)
            message = "متن ارسال شده نامعتبر می باشد یا خالی است";
        tvMessage.setText(message);
    }
}
