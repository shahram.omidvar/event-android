package ir.kooleh.app.View.Ui.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Structure.Interface.Custom.FragmentCommunicator;
import ir.kooleh.app.Structure.Model.Slider.ModelSlider;
import ir.kooleh.app.Utility.Adapter.Ads.Details.AdapterFragmentSlider;

public class FragmentSlider extends Fragment implements View.OnClickListener
{
    View view;
    private List<ModelSlider> listSlider;
    private FragmentCommunicator fragmentCommunicator;
    private ImageView ivClose;
    private DotsIndicator dotsIndicator;
    private int currentItemPos;


    public FragmentSlider(FragmentCommunicator fragmentCommunicator, List<ModelSlider> listSlider, int currentItemPos)

    {
        this.listSlider = listSlider;
        this.fragmentCommunicator = fragmentCommunicator;
        this.currentItemPos = currentItemPos;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_slider, container, false);
        ivClose = view.findViewById(R.id.iv_close_fragment_slider);
        dotsIndicator = view.findViewById(R.id.dots_indicator);

        ViewPager2 viewPager2 = view.findViewById(R.id.view_pager);
        AdapterFragmentSlider adapterFragmentSlider = new AdapterFragmentSlider(getActivity(), listSlider);
        viewPager2.setAdapter(adapterFragmentSlider);
        viewPager2.setCurrentItem(currentItemPos, false);
        dotsIndicator.setViewPager2(viewPager2);

        ivClose.setOnClickListener(this);

        return view;
    }



    @Override
    public void onDetach()
    {
        super.onDetach();
        fragmentCommunicator.fragmentDetached();
    }

    @Override
    public void onClick(View v)
    {
        if (v == ivClose)
        {
            getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }
}
