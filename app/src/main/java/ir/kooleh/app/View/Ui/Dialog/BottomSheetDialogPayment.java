package ir.kooleh.app.View.Ui.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Wallet.ApiBuys;
import ir.kooleh.app.Server.Api.Wallet.ApiWallet;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceBottomSheetPaymentAd;
import ir.kooleh.app.Structure.Interface.Wallet.InterfaceBuys;
import ir.kooleh.app.Structure.Interface.Wallet.InterfaceWallet;
import ir.kooleh.app.Structure.Model.Wallet.ModelInfoWallet;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.Utility.Class.NumberUtils;
import ir.kooleh.app.View.Ui.Activity.general.PaymentActivity;
import ir.kooleh.app.View.ViewCustom.CEditText;
import ir.kooleh.app.View.ViewCustom.CTextView;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class BottomSheetDialogPayment extends BottomSheetDialogFragment implements
        View.OnClickListener,
        InterfaceWallet.Wallet.IntGetInfoWalletUser,
        InterfaceBuys.Buys.IntBuyWalletApp
{
    private CTextView tvWalletAmount;
    private ImageView ivClose;
    private CardView btnPayByWallet, btnPayByGateway;
    private Context context;

    private final ApiBuys apiBuys;
    private final ApiWallet apiWallet;

    private ViewLoading viewLoading;
    private CustomToast customToast;
    private final InterfaceBottomSheetPaymentAd interfaceBottomSheetPaymentAd;

    private final String extraMessage;
    private final String price;
    private final String adOrPackageID;
    private final String type;
    private String gatewayName;

    public BottomSheetDialogPayment(@NonNull Context context, String type,  String extraMessage, String price, String adOrPackageID)
    {
        this.extraMessage = extraMessage;
        this.price = price;
        this.adOrPackageID = adOrPackageID;
        this.type = type;
        this.context = context;
        interfaceBottomSheetPaymentAd = (InterfaceBottomSheetPaymentAd) context;

        viewLoading = new ViewLoading(context);
        apiWallet = new ApiWallet(context);
        apiBuys = new ApiBuys(context);
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.bottom_sheet_dialog_payment_ad, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        CTextView tvTitle = view.findViewById(R.id.tv_title_bottom_sheet_payment_ad);
        ivClose = view.findViewById(R.id.iv_close_bottom_sheet_payment_ad);
        tvWalletAmount = view.findViewById(R.id.tv_wallet_amount_bottom_sheet_payment_ad);
        CEditText etAdPaymentAmount = view.findViewById(R.id.et_ad_payment_amount_bottom_sheet_payment_ad);
        btnPayByWallet = view.findViewById(R.id.btn_pay_by_wallet_bottom_sheet_payment_ad);
        btnPayByGateway = view.findViewById(R.id.btn_pay_by_gatewey_bottom_sheet_payment_ad);

        tvTitle.setText(extraMessage);
        etAdPaymentAmount.requestFocus();
        etAdPaymentAmount.setFocusable(false);
        etAdPaymentAmount.setEnabled(false);
        etAdPaymentAmount.setText(NumberUtils.digitDivider(String.valueOf(price)));

        ivClose.setOnClickListener(this);
        btnPayByWallet.setOnClickListener(this);
        btnPayByGateway.setOnClickListener(this);

        apiWallet.getInfoWalletuser(this);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState)
    {
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.BottomSheetTransparent);
        return super.onCreateDialog(savedInstanceState);
    }


    @Override
    public void onResponseGetInfoWalletUser(boolean response, String message, ModelInfoWallet infoWallet)
    {
        if (response)
        {
            tvWalletAmount.setText(NumberUtils.digitDivider(String.valueOf(infoWallet.getAmount())) + " ریال");
        }
    }

    @Override
    public void onClick(View v)
    {
        if (v == ivClose)
        {
            this.dismiss();
        }
        else if (v == btnPayByWallet)
        {
            viewLoading.show();
            apiBuys.BuyWalletApp(this, type, adOrPackageID);
        }
        else if (v == btnPayByGateway)
        {
            dismiss();
            Intent intent = new Intent(context, PaymentActivity.class);
            intent.putExtra(PaymentActivity.KEY_PRICE_PAYMENT_ACTIVITY, price);
            intent.putExtra(PaymentActivity.KEY_TITLE_TOOLBAR_PAYMENT_ACTIVITY, extraMessage);
            intent.putExtra(PaymentActivity.KEY_MODEL_PAYMENT_ACTIVITY, type);
            intent.putExtra(PaymentActivity.KEY_AD_OR_PACKAGE_ID_PAYMENT_ACTIVITY,  adOrPackageID);
            startActivity(intent);
        }
    }

    @Override
    public void onResponseBuyWalletApp(boolean response, String message)
    {
        viewLoading.close();
        if (response)
        {
            this.dismiss();
            interfaceBottomSheetPaymentAd.onResponseBottomSheetPaymentAd();
            customToast = new CustomToast(this.getContext());
            customToast.showSuccess(message);
        }
        else
        {
            customToast = new CustomToast(this.getContext());
            customToast.showError(message);
        }
    }

}
