package ir.kooleh.app.View.Ui.Fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Utility.Class.CustomToast;
import ir.kooleh.app.Utility.Volley.Controller;
import ir.kooleh.app.View.Ui.Activity.Tools.Comment;
import ir.kooleh.app.View.Ui.Activity.Tools.CommentsServiceAdapter;
import ir.kooleh.app.View.Ui.Activity.Tools.CustomSnackbar;
import ir.kooleh.app.View.Ui.Activity.Tools.PercentagePoints;
import ir.kooleh.app.View.Ui.Activity.Tools.ProgressDialog;
import ir.kooleh.app.View.Ui.Activity.Tools.ServiceApi;
import ir.kooleh.app.View.Ui.Activity.Tools.UserDefault;
import ir.kooleh.app.View.ViewCustom.CButton;


public class CommentServiceFragment extends Fragment implements View.OnClickListener {

    private String serviceId = "";
    private ServiceApi serviceApi ;
        public List<Comment> comments ;
    public PercentagePoints percentagePoints;
    private CommentsServiceAdapter commentsServiceAdapter ;
    private UserDefault userDefault ;

    private static final String TAG = "Cannot invoke method";

    private LinearLayout llContentCommentService;
    private TextView tvTotalRankComment;
    private TextView tvNumberOfCommentService;
    private LinearLayout llCommentProgress;
    private ProgressBar pbGreenComments;
    private TextView tvPercentageGreenComment;
    private ProgressBar pbYellowComments;
    private TextView tvPercentageGreenYellow;
    private ProgressBar pbCommentsRed;
    private TextView tvPercentageRedComment;
    private RecyclerView rvCommentService;

    private ImageButton ibRate5CommentService;
    private ImageButton ibRate4CommentService;
    private ImageButton ibRate3CommentService;
    private ImageButton ibRate2CommentService;
    private ImageButton ibRate1CommentService;
    private Button btnSendCommentService;
    private List<ImageButton> emojiButtos;
    private ProgressDialog progressDialog;


    public CommentServiceFragment() {
        // Required empty public constructor
        serviceApi = ServiceApi.getInstance(getContext());
        progressDialog = new ProgressDialog();
    }

    // TODO: Rename and change types and number of parameters
    public static CommentServiceFragment newInstance(String serviceId) {
        CommentServiceFragment fragment = new CommentServiceFragment();
        Bundle args = new Bundle();
        args.putString("1",serviceId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            serviceId = getArguments().getString("1");
            userDefault = UserDefault.getInstance();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_comment_service, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        llContentCommentService = (LinearLayout)view.findViewById( R.id.ll_content_comment_service );
        tvTotalRankComment = (TextView)view.findViewById( R.id.tv_total_rank_comment );
        tvNumberOfCommentService = (TextView)view.findViewById( R.id.tv_number_of_comment_service );
        llCommentProgress = (LinearLayout)view.findViewById( R.id.ll_comment_progress );
        pbGreenComments = (ProgressBar)view.findViewById( R.id.pb_green_comments );
        tvPercentageGreenComment = (TextView)view.findViewById( R.id.tv_percentage_green_comment );
        pbYellowComments = (ProgressBar)view.findViewById( R.id.pb_yellow_comments );
        tvPercentageGreenYellow = (TextView)view.findViewById( R.id.tv_percentage_yellow_comment);
        pbCommentsRed = (ProgressBar)view.findViewById( R.id.pb_comments_red );
        tvPercentageRedComment = (TextView)view.findViewById( R.id.tv_percentage_red_comment );
        btnSendCommentService = (CButton)view.findViewById( R.id.btn_send_comment_service );
        rvCommentService = (RecyclerView)view.findViewById( R.id.rv_comment_service );
        ibRate5CommentService = (ImageButton)view.findViewById( R.id.ib_rate_5_comment_service );
        ibRate4CommentService = (ImageButton)view.findViewById( R.id.ib_rate_4_comment_service );
        ibRate3CommentService = (ImageButton)view.findViewById( R.id.ib_rate_3_comment_service );
        ibRate2CommentService = (ImageButton)view.findViewById( R.id.ib_rate_2_comment_service );
        ibRate1CommentService = (ImageButton)view.findViewById( R.id.ib_rate_1_comment_service );
        btnSendCommentService = (Button)view.findViewById( R.id.btn_send_comment_service );
        emojiButtos = new ArrayList<>();
        emojiButtos.add(ibRate1CommentService);
        emojiButtos.add(ibRate2CommentService);
        emojiButtos.add(ibRate3CommentService);
        emojiButtos.add(ibRate4CommentService);
        emojiButtos.add(ibRate5CommentService);

        ibRate5CommentService.setOnClickListener( this );
        ibRate4CommentService.setOnClickListener( this );
        ibRate3CommentService.setOnClickListener( this );
        ibRate2CommentService.setOnClickListener( this );
        ibRate1CommentService.setOnClickListener( this );
        btnSendCommentService.setOnClickListener( this );


    }
    @Override
    public void onClick(View v) {
        if ( v == ibRate5CommentService ) {
            onImageButton(emojiButtos,ibRate5CommentService);
            showSendCommentDialog(4);

            // Handle clicks for ibRate5CommentService
        } else if ( v == ibRate4CommentService ) {
            onImageButton(emojiButtos,ibRate4CommentService);
            showSendCommentDialog(3);

            // Handle clicks for ibRate4CommentService
        } else if ( v == ibRate3CommentService ) {
            onImageButton(emojiButtos,ibRate3CommentService);
            showSendCommentDialog(2);

            // Handle clicks for ibRate3CommentService
        } else if ( v == ibRate2CommentService ) {
            onImageButton(emojiButtos,ibRate2CommentService);
            showSendCommentDialog(1);

            // Handle clicks for ibRate2CommentService
        } else if ( v == ibRate1CommentService ) {
            onImageButton(emojiButtos,ibRate1CommentService);
            showSendCommentDialog(0);

            // Handle clicks for ibRate1CommentService
        } else if ( v == btnSendCommentService ) {
            showSendCommentDialog(-1);

        }
    }
    private void onImageButton(List<ImageButton> imageButtons,View v){
        for (int i = 0; i <imageButtons.size() ; i++) {
            ImageButton ib = imageButtons.get(i);
            if (ib != v)
            {
                ib.setColorFilter(ContextCompat.getColor(getContext(), R.color.gray_400));
            }else {
                switch (i){
                    case  0:
                        ib.setColorFilter(ContextCompat.getColor(getContext(), R.color.red_400));
                        break;
                    case  1:
                        ib.setColorFilter(ContextCompat.getColor(getContext(), R.color.orange_400));
                        break;
                    case  2:
                        ib.setColorFilter(ContextCompat.getColor(getContext(), R.color.yellow_400));
                        break;
                    case 3:
                        ib.setColorFilter(ContextCompat.getColor(getContext(), R.color.green_400));;
                        break;
                    case 4:
                        ib.setColorFilter(ContextCompat.getColor(getContext(), R.color.teal_400));;
                        break;
                }
            }
        }

    }

    private void showSendCommentDialog(final int i) {

        if (userDefault.getUser() == null || userDefault.getUser().getApiToken() == null)
        {
//            //todo use other pages
//            CustomSnackbar.makeAction(Controller.context,getView(),"برای ثبت نظر ابتدا وارد شوید","ورود", Snackbar.LENGTH_INDEFINITE,new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    startActivity(new Intent(getContext(), LoginRegisterActivity.class));
//                    getActivity().finish();
//                }
//            }).show();
            return;
        }

        AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
        View view= LayoutInflater.from(getContext()).inflate(R.layout.dialog_send_comment,null);
        builder.setView(view);
        final AlertDialog dialog=builder.create();
        dialog.setCancelable(false);

        final int[] rate = {0};
        final List<ImageButton> imageButtons = new ArrayList<>();
        final ImageButton ibRate1 = (ImageButton) view.findViewById(R.id.ib_rate_1_comment_dialog);
        ImageButton ibRate2 = (ImageButton) view.findViewById(R.id.ib_rate_2_comment_dialog);
        ImageButton ibRate3 = (ImageButton) view.findViewById(R.id.ib_rate_3_comment_dialog);
        ImageButton ibRate4 = (ImageButton) view.findViewById(R.id.ib_rate_4_comment_dialog);
        ImageButton ibRate5 = (ImageButton) view.findViewById(R.id.ib_rate_5_comment_dialog);
        final EditText etContent = (EditText)view.findViewById(R.id.et_content_comment_dialog);
        Button btnSend = (Button)view.findViewById(R.id.btn_send_comment_dialog);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rate[0] == 0)
                {
                    CustomToast customToast = new CustomToast(getContext());
                    customToast.showInfo("لطفا احساس خود را از برنامه براساس یکی از ایموجی ها نشان دهید");
                    return;
                }

                if (etContent.getText().toString().length() == 0)
                {
                    CustomToast customToast = new CustomToast(getContext());

                    customToast.showInfo("لطفا متن کوتاهی وارد کنید");
                    return;
                }

                progressDialog.show(getFragmentManager(), TAG);

                serviceApi.sendComment(userDefault.getUser().getApiToken(), serviceId, etContent.getText().toString(), rate[0] + "", new ServiceApi.CompletionSendComment() {
                    @Override
                    public void response(boolean isOk, String message) {

                        progressDialog.dismiss();
                        CustomSnackbar.make(Controller.context,getView(),message, Snackbar.LENGTH_LONG).show();
                        if (isOk)
                        {
                            dialog.dismiss();
                        }else {

                        }
                    }
                });

            }
        });
        Button btnCancel = (Button) view.findViewById(R.id.btn_cancel_comment_dialog);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int j = 0; j < emojiButtos.size(); j++) {
                    emojiButtos.get(j).setColorFilter(ContextCompat.getColor(getContext(), R.color.gray_500));;
                }
                dialog.dismiss();
            }
        });

        imageButtons.add(ibRate1);
        imageButtons.add(ibRate2);
        imageButtons.add(ibRate3);
        imageButtons.add(ibRate4);
        imageButtons.add(ibRate5);

        if (i != -1) {
            rate[0] = i + 1;
            onImageButton(imageButtons, imageButtons.get(i));
        }

        ibRate1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onImageButton(imageButtons, v);
                rate[0] = 1 ;
            }
        });

        ibRate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onImageButton(imageButtons, v);
                rate[0] = 2 ;
            }
        });
        ibRate3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onImageButton(imageButtons, v);
                rate[0] = 3 ;
            }
        });
        ibRate4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onImageButton(imageButtons, v);
                rate[0] = 4 ;
            }
        });
        ibRate5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onImageButton(imageButtons, v);
                rate[0] = 5;
            }
        });


        dialog.show();
    }


    public void getPercentagePoints() {
        if (percentagePoints != null && comments != null) {
            return;
        }

        serviceApi.getPercentagePoints(serviceId, new ServiceApi.CompletionPercentagePoints() {
            @Override
            public void onPercentagePointsResponse(boolean isOk, PercentagePoints percentagePoints, String message) {
                Log.d(TAG, "onPercentagePointsResponse: "+isOk);
                if (isOk)
                {
                  //  llContentCommentService.setVisibility(View.VISIBLE);
                    setPercentPoint(percentagePoints);
                    getServicCommnets();

                }else
                {
                    CustomSnackbar.makeAction(Controller.context,getView(),message,"تلاش مجدد", Snackbar.LENGTH_INDEFINITE,new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getPercentagePoints();
                        }
                    }).show();
                }
            }
        });
    }
    private void setPercentPoint(PercentagePoints percentPoint)
    {
        this.percentagePoints = percentPoint;
        tvTotalRankComment.setText(""+percentagePoints.getCountPoints());
        tvNumberOfCommentService.setText(percentagePoints.getCountPoints()+" نظر ");
        tvPercentageGreenComment.setText("%"+percentagePoints.getGood());
        pbGreenComments.setProgress(percentagePoints.getGood());
        tvPercentageGreenYellow.setText("%"+percentagePoints.getMedium());
        pbYellowComments.setProgress(percentagePoints.getMedium());
        tvPercentageRedComment.setText("%"+percentagePoints.getWeak());
        pbCommentsRed.setProgress(percentagePoints.getWeak());
        tvPercentageGreenComment.setText("%"+percentagePoints.getGood());
    }
    private void getServicCommnets()
    {
//        serviceApi.getServiceComments(serviceId, new ServiceApi.CompletionServiceComments() {
//            @Override
//            public void onCommentResponse(boolean isOk, List<Comment> comments, String message)
//            {
//                if(isOk) {
//                    CommentServiceFragment.this.comments = comments;
//                    commentsServiceAdapter = new CommentsServiceAdapter(getContext(), comments, new CommentsServiceAdapter.OnClickItemListener() {
//                        @Override
//                        public void onClickItem(View view, Comment item) {
//
//                        }
//                    });
//                    rvCommentService.setLayoutManager(new LinearLayoutManager(getContext()));
//                    rvCommentService.setAdapter(commentsServiceAdapter);
//                }else{
//                    CustomSnackbar.makeAction(Controller.context,getView(),message,"تلاش مجدد", Snackbar.LENGTH_INDEFINITE,new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            getServicCommnets();
//                        }
//                    }).show();
//                }
//            }
//        });
    }


}
