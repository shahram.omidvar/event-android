
package ir.kooleh.app.View.Ui.Activity.Tools;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("telnif")
    @Expose
    private Integer telnif;
//    @SerializedName("services")
//    @Expose
//    private Services services;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("anif_code")
    @Expose
    private String anifCode;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("TM")
    @Expose
    private String tM;
    @SerializedName("all_TM")
    @Expose
    private String allTM;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("image")
    @Expose
    private Object image;
    @SerializedName("api_token")
    @Expose
    private String apiToken;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getTelnif() {
        return telnif;
    }

    public void setTelnif(Integer telnif) {
        this.telnif = telnif;
    }

//    public Services getServices() {
//        return services;
//    }
//
//    public void setServices(Services services) {
//        this.services = services;
//    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAnifCode() {
        return anifCode;
    }

    public void setAnifCode(String anifCode) {
        this.anifCode = anifCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTM() {
        return tM;
    }

    public void setTM(String tM) {
        this.tM = tM;
    }

    public String getAllTM() {
        return allTM;
    }

    public void setAllTM(String allTM) {
        this.allTM = allTM;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Object getImage() {
        return image;
    }

    public void setImage(Object image) {
        this.image = image;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

}
