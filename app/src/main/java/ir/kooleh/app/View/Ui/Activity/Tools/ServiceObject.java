
package ir.kooleh.app.View.Ui.Activity.Tools;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServiceObject {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("story")
    @Expose
    private String story;
    @SerializedName("s_type")
    @Expose
    private Object sType;
    @SerializedName("buy_status")
    @Expose
    private String buyStatus;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("off")
    @Expose
    private Integer off;
    @SerializedName("buy_icon")
    @Expose
    private Object buyIcon;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("icon_rate")
    @Expose
    private String iconRate;
    @SerializedName("rate")
    @Expose
    private String rate;
    @SerializedName("groups")
    @Expose
    private List<Group> groups = null;
    @SerializedName("source_name")
    @Expose
    private String sourceName;
    @SerializedName("source_icon")
    @Expose
    private String sourceIcon;
    @SerializedName("for_color")
    @Expose
    private String forColor;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("location_name")
    @Expose
    private String locationName;

    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("created_at")
    @Expose
    private String createdAt ;

    @SerializedName("next_link")
    @Expose
    private String nextLink;

    @SerializedName("link")
    @Expose
    private String link;

    public ServiceObject() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public Object getSType() {
        return sType;
    }

    public void setSType(Object sType) {
        this.sType = sType;
    }

    public String getBuyStatus() {
        return buyStatus;
    }

    public void setBuyStatus(String buyStatus) {
        this.buyStatus = buyStatus;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getOff() {
        return off;
    }

    public void setOff(Integer off) {
        this.off = off;
    }

    public Object getBuyIcon() {
        return buyIcon;
    }

    public void setBuyIcon(Object buyIcon) {
        this.buyIcon = buyIcon;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getIconRate() {
        return iconRate;
    }

    public void setIconRate(String iconRate) {
        this.iconRate = iconRate;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getSourceIcon() {
        return sourceIcon;
    }

    public void setSourceIcon(String sourceIcon) {
        this.sourceIcon = sourceIcon;
    }

    public String getForColor() {
        return forColor;
    }

    public void setForColor(String forColor) {
        this.forColor = forColor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(CreatedAt String) {
        this.createdAt = createdAt;
    }

    public String getNextLink() {
        return nextLink;
    }

    public void setNextLink(String nextLink) {
        this.nextLink = nextLink;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }


}

