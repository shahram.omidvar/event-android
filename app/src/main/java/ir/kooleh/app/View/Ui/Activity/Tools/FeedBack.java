package ir.kooleh.app.View.Ui.Activity.Tools;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeedBack {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("service_id")
@Expose
private String serviceId;
@SerializedName("user_id")
@Expose
private String userId;
@SerializedName("point")
@Expose
private String point;
@SerializedName("is_recommend")
@Expose
private Object isRecommend;
@SerializedName("created_at")
@Expose
private String createdAt;
@SerializedName("updated_at")
@Expose
private String updatedAt;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getServiceId() {
return serviceId;
}

public void setServiceId(String serviceId) {
this.serviceId = serviceId;
}

public String getUserId() {
return userId;
}

public void setUserId(String userId) {
this.userId = userId;
}

public String getPoint() {
return point;
}

public void setPoint(String point) {
this.point = point;
}

public Object getIsRecommend() {
return isRecommend;
}

public void setIsRecommend(Object isRecommend) {
this.isRecommend = isRecommend;
}

public String getCreatedAt() {
return createdAt;
}

public void setCreatedAt(String createdAt) {
this.createdAt = createdAt;
}

public String getUpdatedAt() {
return updatedAt;
}

public void setUpdatedAt(String updatedAt) {
this.updatedAt = updatedAt;
}

}