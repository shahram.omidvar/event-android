package ir.kooleh.app.View.Ui.Fragment;

import android.content.Intent;
import android.os.Bundle;

import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager2.widget.ViewPager2;


import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.paginate.Paginate;

import java.util.ArrayList;
import java.util.List;

import ir.kooleh.app.R;
import ir.kooleh.app.Server.Api.Load.ApiLoadFragment;
import ir.kooleh.app.Structure.Enum.EnumMessageType;
import ir.kooleh.app.Structure.Enum.PaginateValues;
import ir.kooleh.app.Structure.Interface.Custom.InterfaceTryAgainPaginate;
import ir.kooleh.app.Structure.Interface.Load.InterfaceLoad;
import ir.kooleh.app.Structure.Interface.View.InterfaceMessageHandler;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Load.ModelListLoad;
import ir.kooleh.app.Utility.Adapter.Dashboard.AdapterViewPagerMyAds;
import ir.kooleh.app.Utility.Adapter.Dashboard.AdapterViewPagerTripsTours;
import ir.kooleh.app.Utility.Adapter.Load.AdapterFragmentLoad;
import ir.kooleh.app.Utility.Class.CustomLayoutPaginate;
import ir.kooleh.app.Utility.Class.InterClassDataPassKeys;
import ir.kooleh.app.View.Ui.Activity.Home.HomeActivity;
import ir.kooleh.app.View.Ui.Activity.Load.LoadActivity;
import ir.kooleh.app.View.ViewCustom.ViewLoading;

public class FragmentLoad extends Fragment
{

    //View
    private View view;
    private ViewPager2 viewpagerMyAds;
    private TabLayout tabsMyAds;

    public static final String ID_AD = "id_ad";

    private String keyOne;
    private String keyTwo;
    private String titleOne;
    private String titleTwo;

    public FragmentLoad(String keyOne, String keyTwo, String titleOne, String titleTwo)
    {
        this.keyOne = keyOne;
        this.keyTwo = keyTwo;
        this.titleOne = titleOne;
        this.titleTwo = titleTwo;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_loads, container, false);

        initView(view);

        viewpagerMyAds.setAdapter(new AdapterViewPagerTripsTours(getActivity(), 2, keyOne, keyTwo));
        new TabLayoutMediator(tabsMyAds, viewpagerMyAds,
                (tab, position) ->
                {
                    if (position == 0)
                    {
                        tab.setText(titleOne);
                    }
                    else if (position == 1)
                    {
                        tab.setText(titleTwo);
                    }
                }).attach();

        return view;
    }

    public void initView(View view)
    {
        viewpagerMyAds = view.findViewById(R.id.viewpager_trips_tours);
        tabsMyAds = view.findViewById(R.id.tabs_trips_tours);

        viewpagerMyAds.setUserInputEnabled(false);

    }

    private void setTablayoutClickable(boolean clickable)
    {
        LinearLayout tabStrip = ((LinearLayout)tabsMyAds.getChildAt(0));
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener((v, event) -> !clickable);
        }
    }

}