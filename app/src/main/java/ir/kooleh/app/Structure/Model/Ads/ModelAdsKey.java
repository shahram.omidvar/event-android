
package ir.kooleh.app.Structure.Model.Ads;

import android.os.CountDownTimer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelAdsKey {

    @SerializedName("list_type_ad")
    @Expose
    private String listTypeAd;
    @SerializedName("list_ad")
    @Expose
    private List<ListAd> listAd = null;

    public String getListTypeAd() {
        return listTypeAd;
    }

    public void setListTypeAd(String listTypeAd) {
        this.listTypeAd = listTypeAd;
    }

    public List<ListAd> getListAd() {
        return listAd;
    }

    public void setListAd(List<ListAd> listAd) {
        this.listAd = listAd;
    }


    public class ListAd
    {

        @SerializedName("id_ad")
        @Expose
        private String idAd;
        @SerializedName("category_ad_id")
        @Expose
        private String categoryAdId;
        @SerializedName("type_ad")
        @Expose
        private String typeAd;
        @SerializedName("story")
        @Expose
        private String story;
        @SerializedName("explain")
        @Expose
        private String explain;
        @SerializedName("explain_two")
        @Expose
        private String explainTwo;
        @SerializedName("icon_type_ad")
        @Expose
        private String iconTypeAd;
        @SerializedName("rate")
        @Expose
        private String rate;
        @SerializedName("icon_rate")
        @Expose
        private String iconRate;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("message_image")
        @Expose
        private String messageImage;

        private CountDownTimer countDownTimer;

        public String getIdAd()
        {
            return idAd;
        }

        public void setIdAd(String idAd)
        {
            this.idAd = idAd;
        }

        public String getCategoryAdId()
        {
            return categoryAdId;
        }

        public void setCategoryAdId(String categoryAdId)
        {
            this.categoryAdId = categoryAdId;
        }

        public String getTypeAd()
        {
            return typeAd;
        }

        public void setTypeAd(String typeAd)
        {
            this.typeAd = typeAd;
        }

        public String getStory()
        {
            return story;
        }

        public void setStory(String story)
        {
            this.story = story;
        }

        public String getExplain()
        {
            return explain;
        }

        public void setExplain(String explain)
        {
            this.explain = explain;
        }

        public String getExplainTwo()
        {
            return explainTwo;
        }

        public void setExplainTwo(String explainTwo)
        {
            this.explainTwo = explainTwo;
        }

        public String getIconTypeAd()
        {
            return iconTypeAd;
        }

        public void setIconTypeAd(String iconTypeAd)
        {
            this.iconTypeAd = iconTypeAd;
        }

        public String getRate()
        {
            return rate;
        }

        public void setRate(String rate)
        {
            this.rate = rate;
        }

        public String getIconRate()
        {
            return iconRate;
        }

        public void setIconRate(String iconRate)
        {
            this.iconRate = iconRate;
        }

        public String getImage()
        {
            return image;
        }

        public void setImage(String image)
        {
            this.image = image;
        }

        public String getMessageImage()
        {
            return messageImage;
        }

        public void setMessageImage(String messageImage)
        {
            this.messageImage = messageImage;
        }

        public CountDownTimer getCountDownTimer()
        {
            return countDownTimer;
        }

        public void setCountDownTimer(CountDownTimer countDownTimer)
        {
            this.countDownTimer = countDownTimer;
        }
    }

}



//public class ModelAdsKey
//{
//
//    @SerializedName("id_ad")
//    @Expose
//    private String idAd;
//    @SerializedName("category_ad_id")
//    @Expose
//    private String categoryAdId;
//    @SerializedName("type_ad")
//    @Expose
//    private String adType;
//    @SerializedName("story")
//    @Expose
//    private String story;
//    @SerializedName("explain")
//    @Expose
//    private String explain;
//    @SerializedName("explain_two")
//    @Expose
//    private String explainTwo;
//    @SerializedName("icon_type_ad")
//    @Expose
//    private String iconTypeAd;
//    @SerializedName("rate")
//    @Expose
//    private Double rate;
//    @SerializedName("icon_rate")
//    @Expose
//    private String iconRate;
//    @SerializedName("image")
//    @Expose
//    private String image;
//    @SerializedName("message_image")
//    @Expose
//    private String messageImage;
//
//    private CountDownTimer countDownTimer;
//
//    public String getIdAd() { return idAd; }
//
//    public void setIdAd(String idAd)
//    {
//        this.idAd = idAd;
//    }
//
//    public String getCategoryAdId()
//    {
//        return categoryAdId;
//    }
//
//    public void setCategoryAdId(String categoryAdId)
//    {
//        this.categoryAdId = categoryAdId;
//    }
//
//    public String getStory()
//    {
//        return story;
//    }
//
//    public void setStory(String story)
//    {
//        this.story = story;
//    }
//
//    public String getExplain()
//    {
//        return explain;
//    }
//
//    public void setExplain(String explain)
//    {
//        this.explain = explain;
//    }
//
//    public String getExplainTwo()
//    {
//        return explainTwo;
//    }
//
//    public void setExplainTwo(String explainTwo)
//    {
//        this.explainTwo = explainTwo;
//    }
//
//    public String getIconTypeAd()
//    {
//        return iconTypeAd;
//    }
//
//    public void setIconTypeAd(String iconTypeAd)
//    {
//        this.iconTypeAd = iconTypeAd;
//    }
//
//    public Double getRate()
//    {
//        return rate;
//    }
//
//    public void setRate(Double rate)
//    {
//        this.rate = rate;
//    }
//
//    public String getIconRate()
//    {
//        return iconRate;
//    }
//
//    public void setIconRate(String iconRate)
//    {
//        this.iconRate = iconRate;
//    }
//
//    public String getImage()
//    {
//        return image;
//    }
//
//    public void setImage(String image)
//    {
//        this.image = image;
//    }
//
//    public String getMessageImage()
//    {
//        return messageImage;
//    }
//
//    public void setMessageImage(String messageImage)
//    {
//        this.messageImage = messageImage;
//    }
//
//
//    public String getAdType()
//    {
//        return adType;
//    }
//
//    public void setAdType(String adType)
//    {
//        this.adType = adType;
//    }
//
//    public CountDownTimer getCountDownTimer() { return countDownTimer; }
//
//    public void setCountDownTimer(CountDownTimer countDownTimer) { this.countDownTimer = countDownTimer; }
//}




