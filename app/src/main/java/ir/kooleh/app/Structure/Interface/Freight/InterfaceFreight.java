package ir.kooleh.app.Structure.Interface.Freight;

import ir.kooleh.app.Structure.Model.Freight.ModelFreight;

public class InterfaceFreight
{
    public interface Info
    {
        public void onResponseGetFreight(boolean response, ModelFreight freight, String message);
    }
}
