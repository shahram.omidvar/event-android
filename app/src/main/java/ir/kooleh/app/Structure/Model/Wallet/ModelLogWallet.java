package ir.kooleh.app.Structure.Model.Wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelLogWallet
{
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("price_before")
    @Expose
    private String priceBefore;
    @SerializedName("price_after")
    @Expose
    private String priceAfter;
    @SerializedName("explain")
    @Expose
    private String explain;
    @SerializedName("create")
    @Expose
    private String createdTime;


    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getAction() { return action; }

    public void setAction(String action) { this.action = action; }

    public String getColor() { return color; }

    public void setColor(String color) { this.color = color; }

    public String getPrice() { return price; }

    public void setPrice(String price) { this.price = price; }

    public String getPriceBefore() { return priceBefore; }

    public void setPriceBefore(String priceBefore) { this.priceBefore = priceBefore; }

    public String getPriceAfter() { return priceAfter; }

    public void setPriceAfter(String priceAfter) { this.priceAfter = priceAfter; }

    public String getExplain() { return explain; }

    public void setExplain(String explain) { this.explain = explain; }

    public String getCreatedTime() { return createdTime; }

    public void setCreatedTime(String createdTime) { this.createdTime = createdTime; }
}
