package ir.kooleh.app.Structure.Interface.Driver;

import ir.kooleh.app.Structure.Model.Driver.ModelDriver;

public class InterfaceDriver
{
    public interface Info
    {
        public void onResponseGetDriver(boolean response, ModelDriver driver, String message);
    }
}
