
package ir.kooleh.app.Structure.Model.User.Profile;

import ir.kooleh.app.Structure.Model.Driver.ModelDriver;
import ir.kooleh.app.Structure.Model.Freight.ModelFreight;

public class ModelProfileAllTypeUser
{


    //-----------------------------------------------Base

    private ModelProfile modelProfile;

    public ModelProfile getBaseModelProfile()
    {
        if (modelProfile == null)
            modelProfile = new ModelProfile();
        return modelProfile;
    }

    public void setBaseModelProfile(ModelProfile modelProfile)
    {
        this.modelProfile = modelProfile;
    }


    //------------------------------------------Driver

    private ModelDriver modelDriver;

    public ModelDriver getModelDriver()
    {
        if (modelDriver == null)
            modelDriver = new ModelDriver();
        return modelDriver;
    }

    public void setModelDriver(ModelDriver modelDriver)
    {
        this.modelDriver = modelDriver;
    }


    //------------------------------------------Freight
    private ModelFreight modelFreight;

    public ModelFreight getModelFreight()
    {
        if (modelFreight == null)
            modelFreight = new ModelFreight();
        return modelFreight;
    }

    public void setModelFreight(ModelFreight modelFreight)
    {
        this.modelFreight = modelFreight;
    }
}
