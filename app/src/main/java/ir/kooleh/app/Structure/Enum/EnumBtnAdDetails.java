package ir.kooleh.app.Structure.Enum;

public class EnumBtnAdDetails
{
    public static final String NONE = "none";

    public static class ActionType
    {
        public static final String DIALOG = "dialog";
        public static final String API = "api";
        public static final String CALL = "call";
        public static final String OPEN_ACTIVITY = "openActivity";
    }

    public static class ActionAPIValueType
    {
        public static final String UPDATE_STEP_AD = "updateAd";
        public static final String SET_ACTION_AD = "setActionAd";

        public static class SET_ACTION_AD_TYPE
        {
            public static final String DELETE_MY_OFFER = "deleteMyOffer";
            public static final String CANCEL_MY_OFFER = "cancelMyOffer";
            public static final String FAST = "fast";
            public static final String SHOW_INFO_AD = "showInfoAd";
            public static final String END_FAST_AUCTION = "endFastAuction";
        }


        public static class UPDATE_STEP_AD_TYPE
        {
            public static final String STEP_CANCEL_PENDING= "14";
            public static final String STEP_CANCEL_AUCTION = "26";
            public static final String STEP_CLOSE_AD= "15";
        }
    }

    public static class ActionDialogValueType
    {
        public static final String CREATE_OFFER = "createOffer";
    }

    public static class ActionCallValueType
    {
        public static final String CALL = "call";
    }

}
