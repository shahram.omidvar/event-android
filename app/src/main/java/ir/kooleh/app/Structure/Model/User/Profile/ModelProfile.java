
package ir.kooleh.app.Structure.Model.User.Profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelProfile
{

    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("national_code")
    @Expose
    private String nationalCode;
    @SerializedName("smart_phone")
    @Expose
    private String SmartPhone;
    @SerializedName("notification")
    @Expose
    private String notification;
    @SerializedName("type_user")
    @Expose
    private String typeUser;

    public String getProfileImage()
    {
        return profileImage;
    }

    public void setProfileImage(String profileImage)
    {
        this.profileImage = profileImage;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getNationalCode()
    {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode)
    {
        this.nationalCode = nationalCode;
    }

    public String getNotification()
    {
        return notification;
    }

    public void setNotification(String notification)
    {
        this.notification = notification;
    }

    public String getTypeUser()
    {
        return typeUser;
    }

    public void setTypeUser(String typeUser)
    {
        this.typeUser = typeUser;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getSmartPhone()
    {
        return SmartPhone;
    }

    public void setSmartPhone(String smartPhone)
    {
        SmartPhone = smartPhone;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }
}
