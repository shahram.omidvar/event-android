package ir.kooleh.app.Structure.Interface.MultiSelection;

import java.util.List;

import ir.kooleh.app.Structure.Model.MultiSelection.ModelMultiSelection;

public class InterfaceMultiSelection
{

    public interface Server
    {
        void onResponseListMultiSelection(boolean response, List<ModelMultiSelection> listMultiSelection, String message, String backgroundImage);
    }

    public interface Adapter
    {
        void onClickItemMultiSelection(ModelMultiSelection multiSelection);
    }
}
