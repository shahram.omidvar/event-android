package ir.kooleh.app.Structure.Model.Bills;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelBillInquiry
{
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("bill_id")
    @Expose
    private String billId;
    @SerializedName("payment_id")
    @Expose
    private String paymentId;
    @SerializedName("payment_status_name")
    @Expose
    private String paymentStatusName;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("cycle")
    @Expose
    private List<Cycle> cycle = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentStatusName() {
        return paymentStatusName;
    }

    public void setPaymentStatusName(String paymentStatusName) {
        this.paymentStatusName = paymentStatusName;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPrice() { return price; }

    public void setPrice(String price) { this.price = price; }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public List<Cycle> getCycle() {
        return cycle;
    }

    public void setCycle(List<Cycle> cycle) {
        this.cycle = cycle;
    }


    public class Base {

        @SerializedName("left")
        @Expose
        private String left;
        @SerializedName("right")
        @Expose
        private String right;

        public String getLeft() {
            return left;
        }

        public void setLeft(String left) {
            this.left = left;
        }

        public String getRight() {
            return right;
        }

        public void setRight(String right) {
            this.right = right;
        }

    }

    public class Cycle {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("fa_name")
        @Expose
        private String faName;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("bill_id")
        @Expose
        private String billId;
        @SerializedName("cycle")
        @Expose
        private String cycle;
        @SerializedName("payment_id")
        @Expose
        private String paymentId;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFaName() {
            return faName;
        }

        public void setFaName(String faName) {
            this.faName = faName;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getBillId() {
            return billId;
        }

        public void setBillId(String billId) {
            this.billId = billId;
        }

        public String getCycle() {
            return cycle;
        }

        public void setCycle(String cycle) {
            this.cycle = cycle;
        }

        public String getPaymentId() {
            return paymentId;
        }

        public void setPaymentId(String paymentId) {
            this.paymentId = paymentId;
        }

    }

    public class Data {

        @SerializedName("base")
        @Expose
        private List<Base> base = null;
        @SerializedName("extra_info")
        @Expose
        private List<Base> extraInfo = null;

        public List<Base> getBase() {
            return base;
        }

        public void setBase(List<Base> base) {
            this.base = base;
        }

        public List<Base> getExtraInfo() {
            return extraInfo;
        }

        public void setExtraInfo(List<Base> extraInfo) {
            this.extraInfo = extraInfo;
        }

    }

}
