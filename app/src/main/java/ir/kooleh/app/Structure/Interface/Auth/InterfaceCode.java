package ir.kooleh.app.Structure.Interface.Auth;


import ir.kooleh.app.Structure.Model.User.ModelUser;

public class InterfaceCode
{
    public interface VerifyCode
    {
        public void onResponseVerifyCode(boolean response, String message, String commend, ModelUser user);
    }


    public interface SendCode
    {
        public void onResponseSendCode(boolean response, String message, String commend, int lengthCode);
    }

}
