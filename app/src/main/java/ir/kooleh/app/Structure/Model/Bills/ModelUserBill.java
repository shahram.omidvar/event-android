package ir.kooleh.app.Structure.Model.Bills;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelUserBill
{
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("receipt_image")
    @Expose
    private String receiptImage;
    @SerializedName("receipt_id")
    @Expose
    private String receiptId;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("chose")
    @Expose
    private String chose;

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getReceiptImage() { return receiptImage; }

    public void setReceiptImage(String receiptImage) { this.receiptImage = receiptImage; }

    public String getReceiptId() { return receiptId; }

    public void setReceiptId(String receiptId) { this.receiptId = receiptId; }

    public String getSlug() { return slug; }

    public void setSlug(String slug) { this.slug = slug; }

    public String getValue() { return value; }

    public void setValue(String value) { this.value = value; }

    public String getChose() { return chose; }

    public void setChose(String chose) { this.chose = chose; }
}
