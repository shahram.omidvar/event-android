package ir.kooleh.app.Structure.Interface.Comment;



import java.util.List;

import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Comment.ModelComment;
import ir.kooleh.app.Structure.Model.Comment.ModelUpdateVoteComment;

public interface InterfaceComment
{

    void onResponseListComment(boolean response, List<ModelComment> listComment, ModelPaginate modelPaginate, String message);

    void onResponseVoteComment(boolean response, ModelUpdateVoteComment.Result updateVoteComment, String message, int position);

    void onResponseSavePointAndComment(boolean response, String message);
}
