
package ir.kooleh.app.Structure.Model.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelAdvanceProfile
{

    @SerializedName("type_user")
    @Expose
    private String typeUser;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("name_father")
    @Expose
    private String nameFather;
    @SerializedName("smart_code")
    @Expose
    private String smartCode;
    @SerializedName("fleet_id")
    @Expose
    private String fleetId;
    @SerializedName("fleet_name")
    @Expose
    private String fleetName;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("active_account")
    @Expose
    private String activeAccount;
    @SerializedName("smart_phone")
    @Expose
    private String smartPhone;
    @SerializedName("status_sms")
    @Expose
    private String statusSms;

    public String getTypeUser() { return typeUser; }

    public void setTypeUser(String typeUser) { this.typeUser = typeUser; }

    public String getPhoneNumber() { return phoneNumber; }

    public void setPhoneNumber(String phoneNumber) { this.phoneNumber = phoneNumber; }

    public String getNameFather() { return nameFather; }

    public void setNameFather(String nameFather) { this.nameFather = nameFather; }

    public String getSmartCode() { return smartCode; }

    public void setSmartCode(String smartCode) { this.smartCode = smartCode; }

    public String getFleetId() { return fleetId; }

    public void setFleetId(String fleetId) { this.fleetId = fleetId; }

    public String getFleetName() { return fleetName; }

    public void setFleetName(String fleetName) { this.fleetName = fleetName; }

    public String getCityId() { return cityId; }

    public void setCityId(String cityId) { this.cityId = cityId; }

    public String getCityName() { return cityName; }

    public void setCityName(String cityName) { this.cityName = cityName; }

    public String getActiveAccount() { return activeAccount; }

    public void setActiveAccount(String activeAccount) { this.activeAccount = activeAccount; }

    public String getSmartPhone() { return smartPhone; }

    public void setSmartPhone(String smartPhone) { this.smartPhone = smartPhone; }

    public String getStatusSms() { return statusSms; }

    public void setStatusSms(String statusSms)
    {
        this.statusSms = statusSms;
    }
}
