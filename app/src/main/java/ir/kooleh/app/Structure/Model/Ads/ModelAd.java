package ir.kooleh.app.Structure.Model.Ads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import ir.kooleh.app.Structure.Model.Dashboard.ModelOffersMyAd;
import ir.kooleh.app.Structure.Model.Slider.ModelSlider;


public class ModelAd
{

    @SerializedName("slider")
    @Expose
    private List<ModelSlider> slider = null;
    @SerializedName("base_data")
    @Expose
    private BaseData baseData;
    @SerializedName("list_btn")
    @Expose
    private List<BtnAd> btnAdList;
    @SerializedName("list_data_ad")
    @Expose
    private ListDataAd listDataAd;
    @SerializedName("winner_data")
    @Expose
    private ModelOffersMyAd.ListOffer winnerData;

    public List<ModelSlider> getSlider()
    {
        return slider;
    }

    public void setSlider(List<ModelSlider> slider)
    {
        this.slider = slider;
    }

    public BaseData getBaseData()
    {
        return baseData;
    }

    public void setBaseData(BaseData baseData)
    {
        this.baseData = baseData;
    }

    public List<BtnAd> getBtnAdList() { return btnAdList; }

    public void setBtnAdList(List<BtnAd> btnAdList) { this.btnAdList = btnAdList; }

    public ListDataAd getListDataAd()
    {
        return listDataAd;
    }

    public void setListDataAd(ListDataAd listDataAd)
    {
        this.listDataAd = listDataAd;
    }

    public ModelOffersMyAd.ListOffer getWinnerData()
    {
        return winnerData;
    }

    public void setWinnerData(ModelOffersMyAd.ListOffer winnerData)
    {
        this.winnerData = winnerData;
    }

    public class BaseData
    {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("type_ad")
        @Expose
        private String typeAd;
        @SerializedName("title_ad")
        @Expose
        private String titleAd;
        @SerializedName("date_create_ad")
        @Expose
        private String dateCreateAd;
        @SerializedName("explain_ad")
        @Expose
        private String explainAd;
        @SerializedName("count_down")
        @Expose
        private String countDown;
        @SerializedName("phone_number")
        @Expose
        private String phoneNumber;
        @SerializedName("my_ad")
        @Expose
        private String isMyAd;


        public String getId() { return id; }

        public void setId(String id) { this.id = id; }

        public String getTypeAd() { return typeAd; }

        public void setTypeAd(String type_ad) { this.typeAd = type_ad; }

        public String getTitleAd()
        {
            return titleAd;
        }

        public void setTitleAd(String titleAd)
        {
            this.titleAd = titleAd;
        }

        public String getDateCreateAd()
        {
            return dateCreateAd;
        }

        public void setDateCreateAd(String dateCreateAd)
        {
            this.dateCreateAd = dateCreateAd;
        }

        public String getExplainAd()
        {
            return explainAd;
        }

        public void setExplainAd(String explainAd)
        {
            this.explainAd = explainAd;
        }

        public String getCountDown() { return countDown; }

        public void setCountDown(String countDown) { this.countDown = countDown; }

        public String getPhoneNumber() { return phoneNumber; }

        public void setPhoneNumber(String phoneNumber) { this.phoneNumber = phoneNumber; }

        public String getIsMyAd() { return isMyAd; }

        public void setIsMyAd(String isMyAd) { this.isMyAd = isMyAd; }
    }


    public static class Slider
    {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("model")
        @Expose
        private String model;

        public String getId()
        {
            return id;
        }

        public void setId(String id)
        {
            this.id = id;
        }

        public String getImage()
        {
            return image;
        }

        public void setImage(String image)
        {
            this.image = image;
        }

        public String getModel()
        {
            return model;
        }

        public void setModel(String model)
        {
            this.model = model;
        }

    }


    public class BtnAd
    {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("fa_name")
        @Expose
        private String faName;
        @SerializedName("action")
        @Expose
        private String action;
        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("back_color")
        @Expose
        private String backColor;
        @SerializedName("text_color")
        @Expose
        private String textColor;
        @SerializedName("icon")
        @Expose
        private String icon;

        public String getName() { return name; }

        public void setName(String name) { this.name = name; }

        public String getFaName() { return faName; }

        public void setFaName(String faName) { this.faName = faName; }

        public String getAction() { return action; }

        public void setAction(String action) { this.action = action; }

        public String getValue() { return value; }

        public void setValue(String value) { this.value = value; }

        public String getBackColor() { return backColor; }

        public void setBackColor(String backColor) { this.backColor = backColor; }

        public String getTextColor() { return textColor; }

        public void setTextColor(String textColor) { this.textColor = textColor; }

        public String getIcon() { return icon; }

        public void setIcon(String icon) { this.icon = icon; }

    }



    public class ListDataAd
    {

        @SerializedName("items_ad")
        @Expose
        private List<ModelItemAd> itemsAd = null;
        @SerializedName("specials_ad")
        @Expose
        private List<SpecialsAd> specialsAd = null;

        public List<ModelItemAd> getItemsAd()
        {
            return itemsAd;
        }

        public void setItemsAd(List<ModelItemAd> itemsAd)
        {
            this.itemsAd = itemsAd;
        }

        public List<SpecialsAd> getSpecialsAd()
        {
            return specialsAd;
        }

        public void setSpecialsAd(List<SpecialsAd> specialsAd)
        {
            this.specialsAd = specialsAd;
        }


        public class SpecialsAd
        {

            @SerializedName("top_text")
            @Expose
            private String topText;
            @SerializedName("bottom_text")
            @Expose
            private String bottomText;

            public String getTopText()
            {
                return topText;
            }

            public void setTopText(String topText)
            {
                this.topText = topText;
            }

            public String getBottomText()
            {
                return bottomText;
            }

            public void setBottomText(String bottomText)
            {
                this.bottomText = bottomText;
            }

        }

    }


}

