
package ir.kooleh.app.Structure.Model.App;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ModelApp
{

    @SerializedName("info_user")
    @Expose
    private InfoUser infoUser;

    @SerializedName("menu")
    @Expose
    private List<Menu> menu = null;

    @SerializedName("key_one")
    @Expose
    private String key_one = null;

    public String getKey_one()
    {
        return key_one;
    }

    public void setKey_one(String key_one)
    {
        this.key_one = key_one;
    }

    public String getKey_two()
    {
        return key_two;
    }

    public void setKey_two(String key_two)
    {
        this.key_two = key_two;
    }

    @SerializedName("key_two")
    @Expose
    private String key_two = null;

    @SerializedName("title_one")
    @Expose
    private String title_one = null;

    @SerializedName("title_two")
    @Expose
    private String title_two = null;

    public String getTitle_one()
    {
        return title_one;
    }

    public void setTitle_one(String title_one)
    {
        this.title_one = title_one;
    }

    public String getTitle_two()
    {
        return title_two;
    }

    public void setTitle_two(String title_two)
    {
        this.title_two = title_two;
    }

    public InfoUser getInfoUser()
    {
        return infoUser;
    }

    public void setInfoUser(InfoUser infoUser)
    {
        this.infoUser = infoUser;
    }

    public List<Menu> getMenu()
    {
        return menu;
    }

    public void setMenu(List<Menu> menu)
    {
        this.menu = menu;
    }

    public class InfoUser
    {

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("group")
        @Expose
        private String group;
        @SerializedName("support_number")
        @Expose
        private String supportNumber;
        @SerializedName("invite_message")
        @Expose
        private String inviteMessage;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;


        public String getStatus()
        {
            return status;
        }

        public void setStatus(String status)
        {
            this.status = status;
        }

        public String getImage()
        {
            return image;
        }

        public void setImage(String image)
        {
            this.image = image;
        }

        public String getGroup()
        {
            return group;
        }

        public void setGroup(String group)
        {
            this.group = group;
        }

        public String getSupportNumber() { return supportNumber; }

        public void setSupportNumber(String supportNumber) { this.supportNumber = supportNumber; }

        public String getInviteMessage() { return inviteMessage; }

        public void setInviteMessage(String inviteMessage) { this.inviteMessage = inviteMessage; }

        public String getUsername() { return username; }

        public void setUsername(String username) { this.username = username; }

        public String getFirstName() { return firstName; }

        public void setFirstName(String firstName) { this.firstName = firstName; }

        public String getLastName() { return lastName; }

        public void setLastName(String lastName) { this.lastName = lastName; }
    }


    public class Menu
    {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("fa_name")
        @Expose
        private String faName;
        @SerializedName("icon")
        @Expose
        private String icon;
        @SerializedName("action")
        @Expose
        private String action;
        @SerializedName("value")
        @Expose
        private String value;

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getFaName()
        {
            return faName;
        }

        public void setFaName(String faName)
        {
            this.faName = faName;
        }

        public String getIcon()
        {
            return icon;
        }

        public void setIcon(String icon)
        {
            this.icon = icon;
        }

        public String getAction()
        {
            return action;
        }

        public void setAction(String action)
        {
            this.action = action;
        }

        public String getValue()
        {
            return value;
        }

        public void setValue(String value)
        {
            this.value = value;
        }

    }

}
