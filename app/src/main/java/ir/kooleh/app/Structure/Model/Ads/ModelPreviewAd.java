package ir.kooleh.app.Structure.Model.Ads;

public class ModelPreviewAd
{
    private int id;
    private String field;
    private String value;

    public ModelPreviewAd() { }

    public ModelPreviewAd(String field, String value)
    {
        this.field = field;
        this.value = value;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getField()
    {
        return field;
    }

    public void setField(String field)
    {
        this.field = field;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
}
