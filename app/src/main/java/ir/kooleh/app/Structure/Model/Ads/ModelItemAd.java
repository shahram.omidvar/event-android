package ir.kooleh.app.Structure.Model.Ads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelItemAd
{

    @SerializedName("right_text")
    @Expose
    private String rightText;
    @SerializedName("left_text")
    @Expose
    private String leftText;

    public String getRightText()
    {
        return rightText;
    }

    public void setRightText(String rightText)
    {
        this.rightText = rightText;
    }

    public String getLeftText()
    {
        return leftText;
    }

    public void setLeftText(String leftText)
    {
        this.leftText = leftText;
    }


}
