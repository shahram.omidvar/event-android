package ir.kooleh.app.Structure.Model.App;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelInfoVersion
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("show")
    @Expose
    private Boolean show;
    @SerializedName("logout")
    @Expose
    private Boolean logout;
    @SerializedName("update_server")
    @Expose
    private Boolean updateServer;
    @SerializedName("update_app")
    @Expose
    private Boolean updateApp;
    @SerializedName("close_dialog")
    @Expose
    private Boolean closeDialog;
    @SerializedName("link_app")
    @Expose
    private String linkApp;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public Boolean getShow()
    {
        return show;
    }

    public void setShow(Boolean show)
    {
        this.show = show;
    }

    public Boolean getLogout()
    {
        return logout;
    }

    public void setLogout(Boolean logout)
    {
        this.logout = logout;
    }

    public Boolean getUpdateServer()
    {
        return updateServer;
    }

    public void setUpdateServer(Boolean updateServer)
    {
        this.updateServer = updateServer;
    }

    public Boolean getUpdateApp()
    {
        return updateApp;
    }

    public void setUpdateApp(Boolean updateApp)
    {
        this.updateApp = updateApp;
    }

    public Boolean getCloseDialog()
    {
        return closeDialog;
    }

    public void setCloseDialog(Boolean closeDialog)
    {
        this.closeDialog = closeDialog;
    }

    public String getLinkApp()
    {
        return linkApp;
    }

    public void setLinkApp(String linkApp)
    {
        this.linkApp = linkApp;
    }
}
