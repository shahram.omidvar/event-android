package ir.kooleh.app.Structure.Interface.Bill;

import java.util.List;

import ir.kooleh.app.Structure.Model.Bills.ModelUserBill;

public class InterfaceUserBill
{
    public static class UserBill
    {
        public interface IntGetUserBills
        {
            void onResponseGetUserBills(boolean response, String message, List<ModelUserBill> userBillList);
        }
    }

    public static class BillInquiry
    {
        public interface IntInquireBill
        {
            void onResponseInquireBill(boolean response, String message, String billInquiryResult);
        }

        public interface IntClickBillItem
        {
            void onResponseIntClickBillItem(String billID);
        }
    }

    public static class PayBill
    {
        public interface IntPayBill
        {
            void onResponsePayBill(boolean response, String tokenOne, String tokenTwo, String message);
        }
    }

    public static class BarcodeScannerFragment
    {
        public interface IntCloseFragmnetBarcodeScanner
        {
            void onResponseCloseFragment();
        }
    }

}
