package ir.kooleh.app.Structure.Interface.Custom;

public interface InterfaceGetPermissions
{
    void onResultRequestPermissions(boolean isOk, String message, int CodePermission);
}
