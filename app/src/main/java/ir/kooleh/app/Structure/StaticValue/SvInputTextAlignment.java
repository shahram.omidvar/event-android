package ir.kooleh.app.Structure.StaticValue;

public class SvInputTextAlignment
{
    public static final int INHERIT = 0;
    public static final int GRAVITY = 1;
    public static final int TEXT_START = 2;
    public static final int TEXT_END = 3;
    public static final int CENTER = 4;
    public static final int VIEW_START = 5;
    public static final int VIEW_END = 6;

}
