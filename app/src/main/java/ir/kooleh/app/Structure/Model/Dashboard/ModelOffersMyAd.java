 package ir.kooleh.app.Structure.Model.Dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ModelOffersMyAd {

    @SerializedName("count_down")
    @Expose
    private String countDown;
    @SerializedName("count_offer")
    @Expose
    private String countOffer;
    @SerializedName("auction_id")
    @Expose
    private String auctionId;
    @SerializedName("list_offer")
    @Expose
    private List<ListOffer> listOffer = null;

    public String getCountDown() {
        return countDown;
    }

    public void setCountDown(String countDown) {
        this.countDown = countDown;
    }

    public String getCountOffer() {
        return countOffer;
    }

    public void setCountOffer(String countOffer) {
        this.countOffer = countOffer;
    }

    public String getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(String auctionId) {
        this.auctionId = auctionId;
    }

    public List<ListOffer> getListOffer() {
        return listOffer;
    }

    public void setListOffer(List<ListOffer> listOffer) {
        this.listOffer = listOffer;
    }


    public static class ListOffer {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("auction_id")
        @Expose
        private String auctionId;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("explain")
        @Expose
        private String explain;
        @SerializedName("price")
        @Expose
        private Long price;
        @SerializedName("date")
        @Expose
        private String date;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getAuctionId() {
            return auctionId;
        }

        public void setAuctionId(String auctionId) {
            this.auctionId = auctionId;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getExplain() {
            return explain;
        }

        public void setExplain(String explain) {
            this.explain = explain;
        }

        public Long getPrice() {
            return price;
        }

        public void setPrice(Long price) {
            this.price = price;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

    }

}