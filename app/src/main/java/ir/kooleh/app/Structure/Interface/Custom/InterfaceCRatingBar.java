package ir.kooleh.app.Structure.Interface.Custom;

public interface InterfaceCRatingBar
{
    void onChangeRating(float rating);
}
