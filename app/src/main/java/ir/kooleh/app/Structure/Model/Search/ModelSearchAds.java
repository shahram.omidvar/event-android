package ir.kooleh.app.Structure.Model.Search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelSearchAds
{


    @SerializedName("category")
    @Expose
    private List<Category> category = null;
    @SerializedName("ads")
    @Expose
    private List<Ads> ads = null;

    public List<Category> getCategory()
    {
        return category;
    }

    public void setCategory(List<Category> category)
    {
        this.category = category;
    }

    public List<Ads> getAds()
    {
        return ads;
    }

    public void setAds(List<Ads> ads)
    {
        this.ads = ads;
    }

    public class Ads
    {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("model")
        @Expose
        private String model;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("name_category")
        @Expose
        private String nameCategory;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("rate")
        @Expose
        private String rate;
        @SerializedName("icon_rate")
        @Expose
        private String iconRate;
        @SerializedName("explain")
        @Expose
        private String explain;
        @SerializedName("price")
        @Expose
        private String price;

        public String getId()
        {
            return id;
        }

        public void setId(String id)
        {
            this.id = id;
        }

        public String getModel()
        {
            return model;
        }

        public void setModel(String model)
        {
            this.model = model;
        }

        public String getImage()
        {
            return image;
        }

        public void setImage(String image)
        {
            this.image = image;
        }

        public String getNameCategory()
        {
            return nameCategory;
        }

        public void setNameCategory(String nameCategory)
        {
            this.nameCategory = nameCategory;
        }

        public String getTitle()
        {
            return title;
        }

        public void setTitle(String title)
        {
            this.title = title;
        }

        public String getRate()
        {
            return rate;
        }

        public void setRate(String rate)
        {
            this.rate = rate;
        }

        public String getIconRate()
        {
            return iconRate;
        }

        public void setIconRate(String iconRate)
        {
            this.iconRate = iconRate;
        }

        public String getExplain()
        {
            return explain;
        }

        public void setExplain(String explain)
        {
            this.explain = explain;
        }

        public String getPrice()
        {
            return price;
        }

        public void setPrice(String price)
        {
            this.price = price;
        }

    }


    public class Category
    {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;

        public String getId()
        {
            return id;
        }

        public void setId(String id)
        {
            this.id = id;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

    }

}


