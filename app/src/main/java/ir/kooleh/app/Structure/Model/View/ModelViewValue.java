package ir.kooleh.app.Structure.Model.View;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelViewValue
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("fa_name")
    @Expose
    private String faName;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("child")
    @Expose
    private List<ModelViewValue> child = null;

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getModel() { return model; }

    public void setModel(String model) { this.model = model; }

    public String getFaName() { return faName; }

    public void setFaName(String faName) { this.faName = faName; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getImage() { return image; }

    public void setImage(String image) { this.image = image; }

    public List<ModelViewValue> getChild() { return child; }

    public void setChild(List<ModelViewValue> child) { this.child = child; }
}
