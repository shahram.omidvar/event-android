package ir.kooleh.app.Structure.Model.Ads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelAuctionOffer
{
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("explain")
    @Expose
    private String explain;
    @SerializedName("date")
    @Expose
    private String date;

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getImage() { return image; }

    public void setImage(String image) { this.image = image; }

    public String getExplain() { return explain; }

    public void setExplain(String explain) { this.explain = explain; }

    public String getDate() { return date; }

    public void setDate(String date) { this.date = date; }
}
