package ir.kooleh.app.Structure.Interface.App;

import ir.kooleh.app.Structure.Model.App.ModelApp;

public class InterfaceApp
{

    public interface ServerApp
    {
        void onResponseInfoApp(boolean response, ModelApp modelApp, String message);
    }
}
