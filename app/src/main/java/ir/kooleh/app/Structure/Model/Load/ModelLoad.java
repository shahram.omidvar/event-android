
package ir.kooleh.app.Structure.Model.Load;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import ir.kooleh.app.Structure.Model.Ads.ModelItemAd;
import ir.kooleh.app.Structure.Model.Comment.ModelComment;
import ir.kooleh.app.Structure.Model.Slider.ModelSlider;


public class ModelLoad
{

    @SerializedName("id_ad")
    @Expose
    private String idAd;
    @SerializedName("rate")
    @Expose
    private Float rate;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;

    @SerializedName("title")
    @Expose
    private String title;


    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("count_comment")
    @Expose
    private String countComment;
    @SerializedName("list_attribute")
    @Expose
    private List<ModelItemAd> listAttribute = null;
    @SerializedName("list_slider")
    @Expose
    private List<ModelSlider> listSlider = null;
    @SerializedName("list_comment")
    @Expose
    private List<ModelComment> listComment = null;

    public String getIdAd()
    {
        return idAd;
    }

    public void setIdAd(String idAd)
    {
        this.idAd = idAd;
    }

    public Float getRate()
    {
        return rate;
    }

    public void setRate(Float rate)
    {
        this.rate = rate;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getCountComment()
    {
        return countComment;
    }

    public void setCountComment(String countComment)
    {
        this.countComment = countComment;
    }

    public List<ModelItemAd> getListAttribute()
    {
        return listAttribute;
    }

    public void setListAttribute(List<ModelItemAd> listAttribute)
    {
        this.listAttribute = listAttribute;
    }

    public List<ModelSlider> getListSlider()
    {
        return listSlider;
    }

    public void setListSlider(List<ModelSlider> listSlider)
    {
        this.listSlider = listSlider;
    }

    public List<ModelComment> getListComment()
    {
        return listComment;
    }

    public void setListComment(List<ModelComment> listComment)
    {
        this.listComment = listComment;
    }


    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }
}

