package ir.kooleh.app.Structure.Interface.User;

import ir.kooleh.app.Structure.Model.User.Profile.ModelProfile;
import ir.kooleh.app.Structure.Model.User.Profile.ModelTargetProfile;

public class InterfaceProfile
{

    public interface FragmentProfile
    {
        // دریافت اطلاعات پروفایلی
        void onResponseGetProfileFragment(boolean response, ModelTargetProfile profileFragment, String message);
    }

    public interface ActivityProfile
    {
        void onResponseGetProfile(boolean response, ModelProfile profile, String message);
        void onResponseUpdateProfile(boolean response, String message);
    }
}
