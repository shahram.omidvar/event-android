package ir.kooleh.app.Structure.Interface.App;

import ir.kooleh.app.Structure.Model.App.ModelInfoVersion;

public class InterfaceVersion
{

    public interface IntInfoVersion
    {
        void onResponseInfoVersion(boolean response, String message, ModelInfoVersion modelInfoVersion);
    }
}
