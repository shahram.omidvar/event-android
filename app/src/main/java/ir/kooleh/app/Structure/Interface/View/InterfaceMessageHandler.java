package ir.kooleh.app.Structure.Interface.View;

import android.os.Bundle;

public class InterfaceMessageHandler
{

    public interface FragmentResult
    {
        void onResponseMessageHandler();
    }

    public interface HomeFragmentsResponse
    {
        void onResponseHomeFragments(Bundle bundle);
        void onResponseMessageHandler(String fragmentType);
    }
}
