package ir.kooleh.app.Structure.Enum;

public class EnumWalletModule
{
    public static class BuyModels
    {
        public static final String MODEL_WALLET = "wallet";
        public static final String MODEL_PACKAGE = "package";
        public static final String MODEL_AD = "ad";
        public static final String MODEL_BILL = "bill";
    }

    public static class PaymentMethod
    {
        public static final String METHOD_WALLET = "buyWalletApp";
        public static final String METHOD_GATEWAY = "buyApp";
    }
}
