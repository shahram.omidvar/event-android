package ir.kooleh.app.Structure.Interface.Wallet;

import java.util.List;

import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Wallet.ModelBuy;
import ir.kooleh.app.Structure.Model.Wallet.ModelGateway;
import ir.kooleh.app.Structure.Model.Wallet.ModelTransactionDetails;

public class InterfaceBuys
{

    public static class Buys
    {
        public interface IntBuyApp
        {
            void onResponseBuyApp(boolean response, String tokenOne, String tokenTwo, String message);
        }

        public interface IntBuyWalletApp
        {
            void onResponseBuyWalletApp(boolean response, String message);
        }
        public interface IntGetBuysUser
        {
            void onResponseGetBuysUser(boolean response, String message, List<ModelBuy> buyList, ModelPaginate modelPaginate);
        }
        public interface IntOpenMoreDetailsTransaction
        {
            void onResponseOpenMoreDetailsTransaction(String transactionID);
        }
        public interface IntGetTransactionDetails
        {
            void onResponseGetTransactionDetails(boolean response, String message, ModelTransactionDetails modelTransactionDetails);
        }
        public interface IntGetListGateWay
        {
            void onResponseGetListGateWay(boolean response, String message, List<ModelGateway> gatewayList);
        }
    }

}
