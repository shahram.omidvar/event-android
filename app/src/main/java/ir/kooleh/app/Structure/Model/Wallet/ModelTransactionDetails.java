package ir.kooleh.app.Structure.Model.Wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelTransactionDetails
{
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("buy_id")
    @Expose
    private String buyId;
    @SerializedName("gateway_name")
    @Expose
    private String gatewayName;
    @SerializedName("gateway_image")
    @Expose
    private String gatewayImage;
    @SerializedName("gateway_message")
    @Expose
    private String gatewayMessage;
    @SerializedName("status_message")
    @Expose
    private String statusMessage;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("numberCart")
    @Expose
    private String numberCart;
    @SerializedName("time_buy")
    @Expose
    private String timeBuy;
    @SerializedName("date_create")
    @Expose
    private String dateCreate;
    @SerializedName("expalin")
    @Expose
    private String explain;
    @SerializedName("other_data")
    @Expose
    private String otherData;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getBuyId()
    {
        return buyId;
    }

    public void setBuyId(String buyId)
    {
        this.buyId = buyId;
    }

    public String getGatewayName()
    {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName)
    {
        this.gatewayName = gatewayName;
    }

    public String getGatewayImage()
    {
        return gatewayImage;
    }

    public void setGatewayImage(String gatewayImage)
    {
        this.gatewayImage = gatewayImage;
    }

    public String getGatewayMessage()
    {
        return gatewayMessage;
    }

    public void setGatewayMessage(String gatewayMessage)
    {
        this.gatewayMessage = gatewayMessage;
    }

    public String getStatusMessage()
    {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage)
    {
        this.statusMessage = statusMessage;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getNumberCart()
    {
        return numberCart;
    }

    public void setNumberCart(String numberCart)
    {
        this.numberCart = numberCart;
    }

    public String getTimeBuy()
    {
        return timeBuy;
    }

    public void setTimeBuy(String timeBuy)
    {
        this.timeBuy = timeBuy;
    }

    public String getDateCreate()
    {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate)
    {
        this.dateCreate = dateCreate;
    }

    public String getExplain()
    {
        return explain;
    }

    public void setExplain(String explain)
    {
        this.explain = explain;
    }

    public String getOtherData()
    {
        return otherData;
    }

    public void setOtherData(String otherData)
    {
        this.otherData = otherData;
    }
}
