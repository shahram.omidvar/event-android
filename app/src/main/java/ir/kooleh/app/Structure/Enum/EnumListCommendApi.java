package ir.kooleh.app.Structure.Enum;

public class EnumListCommendApi
{

    public static String Login = "login";
    public static String Error = "error";
    public static String ViewCheckExistsUser = "check_exists_user";
    public static String Register = "register";
    public static String VerifyRegister = "verify_register";
    public static String VerifyResetPassword = "verify_reset_pass";
    public static String VerifyLoginByCode = "verify_login_by_code";
    public static String ViewVerify = "verify";

    public static String Home = "home";
}
