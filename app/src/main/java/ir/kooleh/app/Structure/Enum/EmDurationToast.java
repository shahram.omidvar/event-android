package ir.kooleh.app.Structure.Enum;

public enum EmDurationToast
{
    SHORT,// 3 S
    MEDIUM,// 6 S
    LONG, // 9 S
    VeryLONG, // 12 S
    INDEFINITE,//NO TIME
}
