package ir.kooleh.app.Structure.Interface.Fleet;

import java.util.List;
import ir.kooleh.app.Structure.Model.Fleet.ModelFleet;

public class InterfaceFleet
{

    public interface ServerFleet
    {
        void onResponseListFleet(boolean response, List<ModelFleet> listFleet, String message);
    }

}
