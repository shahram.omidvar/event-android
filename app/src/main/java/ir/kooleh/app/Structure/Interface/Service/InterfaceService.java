package ir.kooleh.app.Structure.Interface.Service;

import ir.kooleh.app.Structure.Model.Service.ModelIndexService;

public class InterfaceService
{

    public interface FragmentService
    {
        void onResponseIndexData(boolean response, ModelIndexService indexService, String message);
    }
}
