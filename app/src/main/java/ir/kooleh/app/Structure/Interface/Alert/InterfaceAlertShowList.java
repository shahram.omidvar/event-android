package ir.kooleh.app.Structure.Interface.Alert;

import ir.kooleh.app.Structure.Model.Alert.ModelAlertShowList;

public class InterfaceAlertShowList
{

    public interface Adapter
    {
        public void OnClickItemAlertShowDialog(ModelAlertShowList modelAlertShowList);
    }

    public interface Response
    {
        public void OnResponseAlterShowList(ModelAlertShowList modelAlertShowList);
    }


}
