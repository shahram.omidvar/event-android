package ir.kooleh.app.Structure.Interface.Wallet;

import java.util.List;

import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Wallet.ModelPackage;
import ir.kooleh.app.Structure.Model.Wallet.ModelUserPackage;

public class InterfacePackage
{

    public static class Package
    {
        public interface IntGetPackages
        {
            void onResponseGetPackages(boolean response, String message, List<ModelPackage> packageList);
        }

        public interface IntGetUserPackages
        {
            void onResponseGetUserPackages(boolean response, String message, List<ModelUserPackage> userPackageList, ModelPaginate modelPaginate);
        }

        public interface IntBuyPackageByWallet
        {
            void onResponseBuyPackageByWallet(boolean response, String message);
        }
        public interface IntBuyPackageByGateway
        {
            void onResponseBuyPackageByGateway(boolean response, String message);
        }

        public interface IntCallbackBtnBuyPackageItem
        {
            void onResponseCallbackBtnBuyPackageItem(ModelPackage modelPackage);
        }
    }

}
