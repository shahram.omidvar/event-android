package ir.kooleh.app.Structure.Model.View;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelOption
{
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name_option")
    @Expose
    private String nameOption;
    @SerializedName("value")
    @Expose
    private String value;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNameOption()
    {
        return nameOption;
    }

    public void setNameOption(String nameOption)
    {
        this.nameOption = nameOption;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
}
