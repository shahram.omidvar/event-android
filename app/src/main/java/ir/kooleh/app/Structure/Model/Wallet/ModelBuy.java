package ir.kooleh.app.Structure.Model.Wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelBuy
{

    @SerializedName("buy_id")
    @Expose
    private String buyId;
    @SerializedName("title_nav")
    @Expose
    private String titleNav;
    @SerializedName("explain_nav")
    @Expose
    private String explainNav;
    @SerializedName("icon_nav")
    @Expose
    private String iconNav;
    @SerializedName("color_nav")
    @Expose
    private String colorNav;
    @SerializedName("right_one")
    @Expose
    private String rightOne;
    @SerializedName("left_one")
    @Expose
    private String leftOne;
    @SerializedName("right_two")
    @Expose
    private String rightTwo;
    @SerializedName("left_two")
    @Expose
    private String leftTwo;
    @SerializedName("explain")
    @Expose
    private String explain;
    @SerializedName("transaction")
    @Expose
    private Transaction transaction = null;

    public String getBuyId() {
        return buyId;
    }

    public void setBuyId(String buyId) {
        this.buyId = buyId;
    }

    public String getTitleNav() {
        return titleNav;
    }

    public void setTitleNav(String titleNav) {
        this.titleNav = titleNav;
    }

    public String getExplainNav() {
        return explainNav;
    }

    public void setExplainNav(String explainNav) {
        this.explainNav = explainNav;
    }

    public String getIconNav() {
        return iconNav;
    }

    public void setIconNav(String iconNav) {
        this.iconNav = iconNav;
    }

    public String getColorNav() {
        return colorNav;
    }

    public void setColorNav(String colorNav) {
        this.colorNav = colorNav;
    }

    public String getRightOne() {
        return rightOne;
    }

    public void setRightOne(String rightOne) {
        this.rightOne = rightOne;
    }

    public String getLeftOne() {
        return leftOne;
    }

    public void setLeftOne(String leftOne) {
        this.leftOne = leftOne;
    }

    public String getRightTwo() {
        return rightTwo;
    }

    public void setRightTwo(String rightTwo) {
        this.rightTwo = rightTwo;
    }

    public String getLeftTwo() {
        return leftTwo;
    }

    public void setLeftTwo(String leftTwo) {
        this.leftTwo = leftTwo;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }





    public class Transaction {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("transaction_id")
        @Expose
        private String transactionId;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("color")
        @Expose
        private String color;
        @SerializedName("status_explain")
        @Expose
        private String statusExplain;
        @SerializedName("gateway_message")
        @Expose
        private String gatewayMessage;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getStatusExplain() {
            return statusExplain;
        }

        public void setStatusExplain(String statusExplain) {
            this.statusExplain = statusExplain;
        }

        public String getGatewayMessage() {
            return gatewayMessage;
        }

        public void setGatewayMessage(String gatewayMessage) {
            this.gatewayMessage = gatewayMessage;
        }
    }
}
