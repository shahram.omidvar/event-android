package ir.kooleh.app.Structure.Model.Bills;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelListSuggestion
{

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("suggestions")
        @Expose
        private List<Suggestion> suggestions = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public List<Suggestion> getSuggestions() {
            return suggestions;
        }

        public void setSuggestions(List<Suggestion> suggestions) {
            this.suggestions = suggestions;
        }


    public class Suggestion {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("message_image")
        @Expose
        private String messageImage;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("story")
        @Expose
        private String story;
        @SerializedName("explain")
        @Expose
        private String explain;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getMessageImage() {
            return messageImage;
        }

        public void setMessageImage(String topText) {
            this.messageImage = topText;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getStory() {
            return story;
        }

        public void setStory(String story) {
            this.story = story;
        }

        public String getExplain() {
            return explain;
        }

        public void setExplain(String explain) {
            this.explain = explain;
        }

    }
}
