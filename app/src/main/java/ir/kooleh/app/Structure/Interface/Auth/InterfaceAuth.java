package ir.kooleh.app.Structure.Interface.Auth;

import ir.kooleh.app.Structure.Model.User.ModelUser;

public class InterfaceAuth
{

    public interface Register
    {
        public void OnResponseRegister(boolean ok, ModelUser user, String message);
    }


    public interface Login
    {
        public void onResponseLogin(boolean response, String message, ModelUser user);
    }
}
