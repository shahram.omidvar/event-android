package ir.kooleh.app.Server.Api.Ads;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.Ads.InterfaceAds;
import ir.kooleh.app.Structure.Model.Ads.ModelAuctionOffer;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiAuction
{
    private static final String TAG = "ApiAuction";
    private final ControllerError controlEroAndExp;
    private final Gson gson;
    private final UserShared userShared;

    public ApiAuction(Context context)
    {
        controlEroAndExp = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }


    public void getOfferList(final InterfaceAds.AuctionAd.intApiGetOfferList intApiGetOfferList, final String adID)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_AUCTION_OFFER_LIST,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intApiGetOfferList.onResponseGetOfferList(
                                    true,
                                    Arrays.asList(gson.fromJson(
                                            jsonResponse.getJSONArray("result").toString(),
                                            ModelAuctionOffer[].class)),
                                    jsonResponse.getString("message")
                            );
                        }
                        else
                        {
                            intApiGetOfferList.onResponseGetOfferList(
                                    false,
                                    null,
                                    jsonResponse.getString("message")
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intApiGetOfferList.onResponseGetOfferList(
                                    false,
                                    null,
                                    controlEroAndExp.checkException(TAG, ex)
                            );
                    }

                },
                error ->
                    intApiGetOfferList.onResponseGetOfferList(
                                    false,
                                    null,
                                    controlEroAndExp.checkVolleyError(error)
                            )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {

                Map<String, String> params = new HashMap<>();

                    params.put("ad_id", adID);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }



    public void createOffer(
            final InterfaceAds.AuctionAd.intApiCreateOffer intApiCreateOffer,
            final String adID,
            final long price
                            )
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.CREATE_AUCTION_OFFER,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        intApiCreateOffer.onResponseCreateOffer(
                                jsonResponse.getBoolean("ok"),
                                jsonResponse.getString("message")
                        );
                    }
                    catch (Exception ex)
                    {
                        intApiCreateOffer.onResponseCreateOffer(
                                false,
                                controlEroAndExp.checkException(TAG, ex)
                        );
                    }

                },
                error ->
                        intApiCreateOffer.onResponseCreateOffer(
                                false,
                                controlEroAndExp.checkVolleyError(error)
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();

                params.put("ad_id", adID);
                params.put("user_offer_id", userShared.getID() + "");
                params.put("price", price + "");
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

}