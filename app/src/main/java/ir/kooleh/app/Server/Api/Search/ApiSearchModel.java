package ir.kooleh.app.Server.Api.Search;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.Search.InterfaceSearch;
import ir.kooleh.app.Structure.Model.Search.ModelSearchItem;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiSearchModel
{


    private static final String TAG = "ApiSearch";
    private Context context;
    private final ControllerError controllerError;
    private Gson gson;
    private UserShared userShared;

    public ApiSearchModel(Context context)
    {
        this.context = context;
        controllerError = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }


    // دریافت اطلاعات پروفایل
    public void SearchModel(final InterfaceSearch.SelectItem interfaceSearchSelectItem, final String table, final String text)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.SEARCH_MODEL,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {

                            List<ModelSearchItem.Result> listSearchModel =
                                    Arrays.asList(gson.fromJson(
                                            jsonResponse.getJSONArray("result").toString(),
                                            ModelSearchItem.Result[].class));

                            interfaceSearchSelectItem.onResponseSearchModelItem(
                                    true,
                                    listSearchModel,
                                    jsonResponse.getString("message")

                            );
                        }
                        else
                        {
                            interfaceSearchSelectItem.onResponseSearchModelItem(
                                    false,
                                    null,
                                    jsonResponse.getString("message")
                            );

                        }


                    }
                    catch (Exception ex)
                    {
                        interfaceSearchSelectItem.onResponseSearchModelItem
                                (
                                        false,
                                        null,
                                        controllerError.checkException(TAG, ex)
                                );
                    }

                }
                ,
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        interfaceSearchSelectItem.onResponseSearchModelItem
                                (
                                        false,
                                        null,
                                        controllerError.checkVolleyError(error)
                                );
                    }
                }
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {

                Map<String, String> params = new HashMap<>();
                params.put("text", text);
                params.put("table", table);
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


}
