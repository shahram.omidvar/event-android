package ir.kooleh.app.Server.Base;

public class UrlAPI
{


    //auth
    public static final String CHECK_EXISTS_USER = BaseServer.ApiV1 + "checkExistsUser";// چک برای وجود داشتن کاربر
    public static final String VERIFY_CODE = BaseServer.ApiV1 + "verifyCode";// بررسی کد فعال سازی
    public static final String SEND_CODE = BaseServer.ApiV1 + "sendCode";
    public static final String REGISTER = BaseServer.ApiV1 + "register";
    public static final String LOGIN = BaseServer.ApiV1 + "login";


    //profile
    public static final String GETP_ROFILE_User = BaseServer.ApiV1 + "getInfoTargetUser";
    public static final String GET_ADV_PROFILE = BaseServer.ApiV1 + "geAdvProfile";
    public static final String UPDATE_PROFILE = BaseServer.ApiV1 + "updateProfileUser";


    //service
    public static final String SERVICE = BaseServer.ApiV1 + "indexService";


    //search

    public static final String SEARCH_MODEL = BaseServer.ApiV1 + "searchModel";
    public static final String SEARCH_ADS = BaseServer.ApiV1 + "searchAds";

    //load

    public static final String LIST_LOAD_FRAGMENT = BaseServer.ApiV1 + "getListLoad";
    public static final String GET_LOAD = BaseServer.ApiV1 + "getLoad";


    // comment
    public static final String GET_LIST_COMMENT_LOAD = BaseServer.ApiV1 + "getListCommentLoad";
    public static final String VOTE_COMMENT = BaseServer.ApiV1 + "voteComment";
    public static final String SAVE_POINT_COMMENT = BaseServer.ApiV1 + "savePointAndComment";


    //ads
    public static final String GET_AD = BaseServer.ApiV1 + "getAd";
    public static final String GET_MY_AD = BaseServer.ApiV1 + "getMyAd";
    public static final String GET_MY_OFFER_AD = BaseServer.ApiV1 + "getMyOfferAd";
    public static final String CREATE_AD = BaseServer.ApiV1 + "createAd";
    public static final String GET_ADS_KEY = BaseServer.ApiV1 + "getAdsByKey";


    //driver

    public static final String GET_DRIVER = BaseServer.ApiV1 + "getDriver";
    
    //freight
    public static final String GET_FREIGHT = BaseServer.ApiV1 + "getFreight";


    // fleet


    public static final String GET_LIST_FLEET = BaseServer.ApiV1 + "getListFleet";


    //multi selection

    public static final String GET_LIST_CATEGORY_ADS = BaseServer.ApiV1 + "getCategoryAds";


    //App

    public static final String GET_INFO_APP = BaseServer.ApiV1 + "getInfoApp";
    public static final String GET_INFO_VERSION = BaseServer.ApiV1 + "getInfoVersion";

    //get
    public static final String GET_ATTRIBUTE_CATEGORY_AD = BaseServer.ApiV1 + "getAttributeCategoryAd";

    public static final String GET_LIST_VALUE_VIEW = BaseServer.ApiV1 + "getListValueView";


    public static final String GET_AUCTION_OFFER_LIST = BaseServer.ApiV1 + "getOffersAd";
    public static final String CREATE_AUCTION_OFFER = BaseServer.ApiV1 + "createAuctionOffer";

    // dashboard
    public static final String GET_PROFILE = BaseServer.ApiV1 + "getProfile";
    public static final String GET_MY_ADS = BaseServer.ApiV1 + "getMyAds";
    public static final String GET_MY_AD_AUCTIONS = BaseServer.ApiV1 + "getMyAdAuctions";
    public static final String GET_OFFERS_MY_AD = BaseServer.ApiV1 + "getOffersMyAd";
    public static final String WINNER_AUCTION = BaseServer.ApiV1 + "winnerAuction";
    public static final String GET_MY_OFFERS = BaseServer.ApiV1 + "getMyOffers";
    public static final String UPDATE_STEP_AD = BaseServer.ApiV1 + "updateStepAd";
    public static final String SET_ACTION_AD = BaseServer.ApiV1 + "setActionAd";
    public static final String SHOW_INFO_AUCTION_OWNER = BaseServer.ApiV1 + "showInfoAuctionOwner";


    // Wallet
    public static final String BUY_APP = BaseServer.ApiV1 + "buyApp";
    public static final String BUY_WALLET_APP = BaseServer.ApiV1 + "buyWalletApp";
    public static final String GET_INFO_WALLET_USER = BaseServer.ApiV1 + "getInfoWalletUser";
    public static final String GET_LOGS_WALLET_USER = BaseServer.ApiV1 + "getLogsWalletUser";
    public static final String GET_USER_PACKAGES = BaseServer.ApiV1 + "getPackagesUser";
    public static final String GET_PACKAGES = BaseServer.ApiV1 + "getListPackage";
    public static final String GET_BUYS_USER = BaseServer.ApiV1 + "getBuysUser";
    public static final String GET_TRANSACTION = BaseServer.ApiV1 + "getTransaction";
    public static final String GET_LIST_GATEWAY = BaseServer.ApiV1 + "getListGateway";


    // Bill
    public static final String GET_RECEIPTS = BaseServer.ApiV1 + "getReceipts";
    public static final String GET_LIST_SUGGESTIONS = BaseServer.ApiV1 + "getListSuggestions";
    public static final String GET_RECEIPTS_USER = BaseServer.ApiV1 + "getReceiptsUser";
    public static final String RECEIPT = BaseServer.ApiV1 + "receipt";
    public static final String PAY_BILL = BaseServer.ApiV1 + "payBill";
}

