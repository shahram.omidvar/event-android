package ir.kooleh.app.Server.Api.Wallet;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.Wallet.InterfacePackage;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Wallet.ModelPackage;
import ir.kooleh.app.Structure.Model.Wallet.ModelUserPackage;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiPackage
{
    private final ControllerError controlEroAndExp;
    private final Gson gson;
    private final UserShared userShared;
    private static final String TAG = "Api Wallet";

    public ApiPackage(Context context)
    {
        controlEroAndExp = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }


    public void getPackages(InterfacePackage.Package.IntGetPackages intGetPackages, int nextPage, int itemsPerPage)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_PACKAGES,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intGetPackages.onResponseGetPackages(
                                    true,
                                    jsonResponse.getString("message"),
                                    Arrays.asList(gson.fromJson(
                                            jsonResponse.getJSONObject("result").getJSONArray("data").toString(),
                                            ModelPackage[].class))
                            );
                        }
                        else
                        {
                            intGetPackages.onResponseGetPackages(
                                    false,
                                    jsonResponse.getString("message"),
                                    null
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intGetPackages.onResponseGetPackages(
                                false,
                                controlEroAndExp.checkException(TAG, ex),
                                null
                        );
                    }

                },
                error ->
                        intGetPackages.onResponseGetPackages(
                                false,
                                controlEroAndExp.checkVolleyError(error),
                                null
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("page", nextPage + "");
                params.put("per_page", itemsPerPage + "");
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }



    public void getUserPackages(InterfacePackage.Package.IntGetUserPackages intGetUserPackages, int nextPage, int itemsPerPage)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_USER_PACKAGES,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intGetUserPackages.onResponseGetUserPackages(
                                    true,
                                    jsonResponse.getString("message"),
                                    Arrays.asList(gson.fromJson(
                                            jsonResponse.getJSONObject("result").getJSONArray("data").toString(),
                                            ModelUserPackage[].class)),
                                    gson.fromJson(
                                            jsonResponse.getJSONObject("result").getJSONObject("paginate").toString(),
                                            ModelPaginate.class)
                            );
                        }
                        else
                        {
                            intGetUserPackages.onResponseGetUserPackages(
                                    false,
                                    jsonResponse.getString("message"),
                                    null,
                                    null
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intGetUserPackages.onResponseGetUserPackages(
                                false,
                                controlEroAndExp.checkException(TAG, ex),
                                null,
                                null
                        );
                    }

                },
                error ->
                        intGetUserPackages.onResponseGetUserPackages(
                                false,
                                controlEroAndExp.checkVolleyError(error),
                                null,
                                null
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("page", nextPage + "");
                params.put("per_page", itemsPerPage + "");
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

}
