package ir.kooleh.app.Server.Api.Bill;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.Bill.InterfaceBillType;
import ir.kooleh.app.Structure.Interface.Bill.InterfaceOfferType;
import ir.kooleh.app.Structure.Interface.Bill.InterfaceUserBill;
import ir.kooleh.app.Structure.Model.Bills.ModelBillCategories;
import ir.kooleh.app.Structure.Model.Bills.ModelListSuggestion;
import ir.kooleh.app.Structure.Model.Bills.ModelUserBill;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiBills
{
    private static final String TAG = "ApiBills";
    private final Context context;
    private final ControllerError controlEroAndExp;
    private final Gson gson;
    private final UserShared userShared;

    public ApiBills(Context context)
    {
        this.context = context;
        controlEroAndExp = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }


    public void getBillTypes(final InterfaceBillType.BillType.IntGetBillTypes intGetBillType)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_RECEIPTS,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intGetBillType.onResponseGetBillTypes(
                                    true,
                                    jsonResponse.getString("message"),
                                    Arrays.asList(gson.fromJson(jsonResponse.getJSONObject("result").getJSONArray("data").toString(), ModelBillCategories[].class)),
                                    jsonResponse.getJSONObject("result").getString("back_image")
                            );
                        }
                        else
                        {
                            intGetBillType.onResponseGetBillTypes(
                                    false,
                                    jsonResponse.getString("message"),
                                    null,
                                    ""
                            );

                        }

                    }
                    catch (Exception ex)
                    {
                        intGetBillType.onResponseGetBillTypes(
                                        false,
                                        controlEroAndExp.checkException(TAG, ex),
                                        null,
                                ""
                                );
                    }

                }
                ,
                error ->
                        intGetBillType.onResponseGetBillTypes(
                                false,
                                controlEroAndExp.checkVolleyError(error),
                                null,
                                ""
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                return null;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


    public void getOffersType(final InterfaceOfferType.OfferType.IntGetOfferType intGetOfferType)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_LIST_SUGGESTIONS,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intGetOfferType.onResponseGetOfferType(
                                    true,
                                    jsonResponse.getString("message"),
                                    Arrays.asList(gson.fromJson(jsonResponse.getJSONArray("result").toString(), ModelListSuggestion[].class))
                            );
                        }
                        else
                        {
                            intGetOfferType.onResponseGetOfferType(
                                    false,
                                    jsonResponse.getString("message"),
                                    null
                            );

                        }

                    }
                    catch (Exception ex)
                    {
                        intGetOfferType.onResponseGetOfferType(
                                false,
                                controlEroAndExp.checkException(TAG, ex),
                                null
                        );
                    }

                }
                ,
                error ->
                        intGetOfferType.onResponseGetOfferType(
                                false,
                                controlEroAndExp.checkVolleyError(error),
                                null
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                return null;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }



    public void getUserBills(final InterfaceUserBill.UserBill.IntGetUserBills intGetUserBills, String billTypeID, int nextPage, int itemsPerPage)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_RECEIPTS_USER,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intGetUserBills.onResponseGetUserBills(
                                    true,
                                    jsonResponse.getString("message"),
                                    Arrays.asList(
                                            gson.fromJson(jsonResponse.getJSONArray("result").toString(),
                                            ModelUserBill[].class))
                            );
                        }
                        else
                        {
                            intGetUserBills.onResponseGetUserBills
                                    (
                                    false,
                                    jsonResponse.getString("message"),
                                    null
                                    );
                        }

                    }
                    catch (Exception ex)
                    {
                        intGetUserBills.onResponseGetUserBills
                                (
                                false,
                                controlEroAndExp.checkException(TAG, ex),
                                null
                                );
                    }

                }
                ,
                error ->
                        intGetUserBills.onResponseGetUserBills
                                (
                                false,
                                controlEroAndExp.checkVolleyError(error),
                                null
                                )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                HashMap<String, String> params = new HashMap<>();
                params.put("receipt_id", billTypeID + "");
//                params.put("page", nextPage + "");
//                params.put("per_page", itemsPerPage + "");
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }



    public void inquireBill(final InterfaceUserBill.BillInquiry.IntInquireBill inquireBill, String billName, String billID)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.RECEIPT,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            inquireBill.onResponseInquireBill(
                                    true,
                                    jsonResponse.getString("message"),
                                    jsonResponse.getJSONObject("result").toString()
                            );
                        }
                        else
                        {
                            inquireBill.onResponseInquireBill(
                                    false,
                                    jsonResponse.getString("message"),
                                    null
                            );

                        }

                    }
                    catch (Exception ex)
                    {
                        inquireBill.onResponseInquireBill(
                                        false,
                                        controlEroAndExp.checkException(TAG, ex),
                                        null
                                );
                    }

                }
                ,
                error ->
                        inquireBill.onResponseInquireBill(
                                        false,
                                        controlEroAndExp.checkVolleyError(error),
                                        null
                                )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                HashMap<String, String> params = new HashMap<>();
                params.put("receipt_id", billName);
                params.put("bill_id", billID);
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }



    public void inquirePhoneBill(final InterfaceUserBill.BillInquiry.IntInquireBill inquireBill, String bill_id, String mobileNumber)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.RECEIPT,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            inquireBill.onResponseInquireBill(
                                    true,
                                    jsonResponse.getString("message"),
                                    jsonResponse.getJSONObject("result").toString()
                            );
                        }
                        else
                        {
                            inquireBill.onResponseInquireBill(
                                    false,
                                    jsonResponse.getString("message"),
                                    null
                            );

                        }

                    }
                    catch (Exception ex)
                    {
                        inquireBill.onResponseInquireBill(
                                false,
                                controlEroAndExp.checkException(TAG, ex),
                                null
                        );
                    }

                }
                ,
                error ->
                        inquireBill.onResponseInquireBill(
                                false,
                                controlEroAndExp.checkVolleyError(error),
                                null
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                HashMap<String, String> params = new HashMap<>();
                params.put("receipt_id", bill_id);
                params.put("mobile_number", mobileNumber);
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


    public void payBill(final InterfaceUserBill.PayBill.IntPayBill intPayBill, String bill_id, String paymentID, String gatewayName)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.PAY_BILL,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intPayBill.onResponsePayBill(
                                    true,
                                    jsonResponse.getJSONObject("result").getString("token_one"),
                                    jsonResponse.getJSONObject("result").getString("token_two"),
                                    jsonResponse.getString("message")
                            );
                        }
                        else
                        {
                            intPayBill.onResponsePayBill(
                                    false,
                                    "",
                                    "",
                                    jsonResponse.getString("message")
                            );
                        }

                    }
                    catch (Exception ex)
                    {
                        intPayBill.onResponsePayBill(
                                false,
                                "",
                                "",
                                controlEroAndExp.checkException(TAG, ex)
                        );
                    }

                }
                ,
                error ->
                        intPayBill.onResponsePayBill(
                                false,
                                "",
                                "",
                                controlEroAndExp.checkVolleyError(error)
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                HashMap<String, String> params = new HashMap<>();
                params.put("id_log_user_receipt", bill_id);
                params.put("gateway_name", gatewayName);
                params.put("payment_id", paymentID);
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

}
