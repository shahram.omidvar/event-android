package ir.kooleh.app.Server.Api.auth;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.Auth.InterfaceAuth;
import ir.kooleh.app.Structure.Model.User.ModelUser;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.Class.InfoAppAndDevice;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiAuth
{

    private static final String TAG = "ApiLogin";

    private final Gson gson;
    private final UserShared userShared;
    private final ControllerError controllerError;
    private Context context;

    public ApiAuth(Context context)
    {
        this.context = context;
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
        controllerError = new ControllerError(context);

    }


    // ثبت نام
    public void Register(
            InterfaceAuth.Register interfaceRegister,
            String phoneNumber,
            String firstName,
            String lastName,
            String password,
            String typeUser,
            String androidId

    )
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.REGISTER,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            ModelUser modelUser = gson.fromJson(jsonResponse.getString("result"), ModelUser.class);
                            interfaceRegister.OnResponseRegister(
                                    true,
                                    modelUser,
                                    "ثبت نام موفقیت آمیز بود . به راهنما خوش آمدید"
                            );
                        }
                        else
                        {
                            interfaceRegister.OnResponseRegister(
                                    false,
                                    null,
                                    jsonResponse.getString("message")
                            );

                        }


                    }
                    catch (Exception ex)
                    {
                        interfaceRegister.OnResponseRegister
                                (
                                        false,
                                        null,
                                        controllerError.checkException(TAG, ex)
                                );
                    }

                }
                ,
                error -> interfaceRegister.OnResponseRegister
                        (
                                false,
                                null,
                                controllerError.checkVolleyError(error)
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("phone_number", phoneNumber);
                params.put("first_name", firstName);
                params.put("last_name", lastName);
                params.put("password", password);
                params.put("type_user", typeUser);
                params.put("android_id", androidId);
                return params;
            }

        };

        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);

    }


    // ورود با گذرواژه
    public void Login
    (
            InterfaceAuth.Login interfaceLogin,
            String phoneNumber,
            String password,
            String fcmToken
    )
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.LOGIN,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            ModelUser modelUser = gson.fromJson(jsonResponse.getString("result"), ModelUser.class);
                            interfaceLogin.onResponseLogin(
                                    true,
                                    "  به راهنما خوش آمدید",
                                    modelUser
                            );
                        }
                        else
                        {
                            interfaceLogin.onResponseLogin(
                                    false,
                                    jsonResponse.getString("message"),
                                    null);

                        }


                    }
                    catch (Exception ex)
                    {
                        interfaceLogin.onResponseLogin
                                (
                                        false,
                                        controllerError.checkException(TAG, ex),
                                        null
                                );
                    }

                }
                ,
                error -> interfaceLogin.onResponseLogin
                        (
                                false,
                                controllerError.checkVolleyError(error),
                                null
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {

                Map<String, String> params = new HashMap<>();
                params.put("phone_number", phoneNumber);
                params.put("password", password);
                params.put("android_id", InfoAppAndDevice.getAndroidId());
                params.put("fcm_token", fcmToken);
                return params;
            }

        };

        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);

    }
}
