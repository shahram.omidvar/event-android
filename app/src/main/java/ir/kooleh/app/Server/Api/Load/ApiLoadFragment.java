package ir.kooleh.app.Server.Api.Load;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.Load.InterfaceLoad;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Load.ModelListLoad;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiLoadFragment
{
    private static final String TAG = "ApiLoadFragment";

    private final ControllerError controllerError;
    private final Gson gson;
    private final UserShared userShared;


    public ApiLoadFragment(Context context)
    {
        controllerError = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }


    // دریافت اطلاعات فرگمنت بار
    public void getIndexLoadFragment(final InterfaceLoad.Fragment interfaceLoadFragment, int nextPage, int itemsPerPage, String key)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.LIST_LOAD_FRAGMENT,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {

                            interfaceLoadFragment.onResponseListLoadFragment
                                    (
                                            true,
                                            Arrays.asList(gson.fromJson(jsonResponse.getJSONObject("result")
                                                            .getJSONArray("data").toString(),
                                                    ModelListLoad[].class)),
                                            gson.fromJson(jsonResponse.getJSONObject("result")
                                                            .getJSONObject("paginate").toString(),
                                                    ModelPaginate.class),
                                            jsonResponse.getString("message")
                                    );
                        }
                        else
                        {

                            interfaceLoadFragment.onResponseListLoadFragment
                                    (
                                            false,
                                            null,
                                            null,
                                            jsonResponse.getString("message")
                                    );

                        }


                    }
                    catch (Exception ex)
                    {
                        interfaceLoadFragment.onResponseListLoadFragment
                                (
                                        false,
                                        null,
                                        null,
                                        controllerError.checkException(TAG, ex)
                                );
                    }

                }
                ,
                error ->
                {
                    interfaceLoadFragment.onResponseListLoadFragment
                            (
                                    false,
                                    null,
                                    null,
                                    controllerError.checkVolleyError(error)
                            );
                }
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("page", nextPage + "");
                params.put("per_page", itemsPerPage + "");
                params.put("key", key + "");
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

}
