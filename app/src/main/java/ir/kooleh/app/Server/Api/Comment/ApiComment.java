package ir.kooleh.app.Server.Api.Comment;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.Comment.InterfaceComment;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Comment.ModelComment;
import ir.kooleh.app.Structure.Model.Comment.ModelUpdateVoteComment;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiComment
{

    private static final String TAG = "ApiComment";

    private ControllerError controllerError;
    private Gson gson;
    private UserShared userShared;
    private Context context;

    public ApiComment(Context context)
    {
        this.context = context;

        controllerError = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }


    public void getListCommentAds(final InterfaceComment interfaceComment, String adID, int nextPage, int itemsPerPage)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_LIST_COMMENT_LOAD,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {

                            List<ModelComment> listComment =
                                    Arrays.asList(gson.fromJson(
                                            jsonResponse.getJSONObject("result").getJSONArray("data").toString(),
                                            ModelComment[].class));

                            ModelPaginate modelPaginate = gson.fromJson
                                    (jsonResponse.getJSONObject("result").getJSONObject("paginate").toString(), ModelPaginate.class);


                            interfaceComment.onResponseListComment(
                                    true,
                                    listComment,
                                    modelPaginate,
                                    jsonResponse.getString("message")

                            );
                        }
                        else
                        {
                            interfaceComment.onResponseListComment(
                                    false,
                                    null,
                                    null,
                                    jsonResponse.getString("message")
                            );

                        }


                    }
                    catch (Exception ex)
                    {
                        interfaceComment.onResponseListComment
                                (
                                        false,
                                        null,
                                        null,
                                        controllerError.checkException(TAG, ex)
                                );
                    }
                }
                ,
                error -> interfaceComment.onResponseListComment
                        (
                                false,
                                null,
                                null,
                                controllerError.checkVolleyError(error)
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {

                Map<String, String> params = new HashMap<>();
                params.put("ad_id", adID);
                params.put("page", nextPage + "");
                params.put("per_page", itemsPerPage + "");
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


    public void setVoteComment(final InterfaceComment interfaceComment, String commentId, String _vote, int position)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.VOTE_COMMENT,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            ModelUpdateVoteComment updateVoteComment = gson.fromJson(response, ModelUpdateVoteComment.class);


                            interfaceComment.onResponseVoteComment
                                    (
                                            true,
                                            updateVoteComment.getResult(),
                                            updateVoteComment.getResult().getMessage(),
                                            position
                                    );
                        }
                        else
                        {
                            interfaceComment.onResponseVoteComment
                                    (
                                            false,
                                            null,
                                            jsonResponse.getString("message"),
                                            position
                                    );

                        }


                    }
                    catch (Exception ex)
                    {
                        interfaceComment.onResponseVoteComment
                                (
                                        false,
                                        null,
                                        controllerError.checkException(TAG, ex),
                                        position
                                );

                    }

                }
                ,
                error -> interfaceComment.onResponseVoteComment
                        (
                                false,
                                null,
                                controllerError.checkVolleyError(error),
                                position
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {

                Map<String, String> params = new HashMap<>();
                params.put("comment_id", commentId);
                params.put("vote", _vote);
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

    public void setPointAndComment(final InterfaceComment interfaceComment, String comment, float point, int adID)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.SAVE_POINT_COMMENT,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);

                        interfaceComment.onResponseSavePointAndComment(
                                jsonResponse.getBoolean("ok"),
                                jsonResponse.getString("message")
                        );


                    }
                    catch (Exception ex)
                    {
                        interfaceComment.onResponseSavePointAndComment
                                (
                                        false,
                                        controllerError.checkException(TAG, ex)
                                );

                    }

                }
                ,
                error -> interfaceComment.onResponseSavePointAndComment
                        (
                                false,
                                controllerError.checkVolleyError(error)
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {

                Map<String, String> params = new HashMap<>();
                params.put("text", comment);
                params.put("point", String.valueOf(point));
                params.put("ad_id", adID + "");
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }

}
