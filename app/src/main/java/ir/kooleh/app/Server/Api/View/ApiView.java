package ir.kooleh.app.Server.Api.View;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.ListView.InterfaceView;
import ir.kooleh.app.Structure.Model.View.ModelView;
import ir.kooleh.app.Structure.Model.View.ModelViewValue;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiView
{

    private static final String TAG = "ApiView";
    private final Context context;
    private final ControllerError controllerError;
    private final Gson gson;
    private UserShared userShared;

    public ApiView(Context context)
    {
        this.context = context;
        controllerError = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }


    // دریافت اطلاعات پروفایل
    public void getListViewCategoryAds(final InterfaceView.Server interfaceViewServer, final String idCategoryAds)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_ATTRIBUTE_CATEGORY_AD,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {

                            ModelView modelView = gson.fromJson
                                    (
                                            jsonResponse.getJSONObject("result").toString(),
                                            ModelView.class
                                    );


                            interfaceViewServer.onResponseListView
                                    (
                                            true,
                                            modelView,
                                            jsonResponse.getString("message")

                                    );
                        }
                        else
                        {
                            interfaceViewServer.onResponseListView(
                                    false,
                                    null,
                                    jsonResponse.getString("message")
                            );

                        }


                    }
                    catch (Exception ex)
                    {
                        interfaceViewServer.onResponseListView
                                (
                                        false,
                                        null,
                                        controllerError.checkException(TAG, ex)
                                );
                    }

                }
                ,
                error -> interfaceViewServer.onResponseListView
                        (
                                false,
                                null,
                                controllerError.checkVolleyError(error)
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {

                Map<String, String> params = new HashMap<>();
                params.put("category_ad_id", idCategoryAds);
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }



    public void getViewValueList (final InterfaceView.Server interfaceViewValue, final String modelView)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_LIST_VALUE_VIEW,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            JSONObject jsonResult = new JSONObject(jsonResponse.getString("result"));


                            String model = jsonResult.getString("model");
                            String title = jsonResult.getString("title");
                            List<ModelViewValue> modelViewValueList =
                                    Arrays.asList(gson.fromJson(
                                            jsonResult.getJSONArray("value").toString(),
                                            ModelViewValue[].class));



                            interfaceViewValue.onResponseListViewValue
                                    (
                                    true,
                                            modelViewValueList,
                                            jsonResponse.getString("message"),
                                            model,
                                            title
                                    );
                        }
                        else
                        {
                            interfaceViewValue.onResponseListViewValue(
                                    false,
                                    null,
                                    jsonResponse.getString("message"),
                                    "",
                                    ""
                            );

                        }


                    }
                    catch (Exception ex)
                    {
                        interfaceViewValue.onResponseListViewValue
                                (
                                        false,
                                        null,
                                        controllerError.checkException(TAG, ex),
                                        "",
                                        ""
                                );
                    }

                }
                ,
                error -> interfaceViewValue.onResponseListViewValue
                        (
                                false,
                                null,
                                controllerError.checkVolleyError(error),
                                "",
                                ""
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {

                Map<String, String> params = new HashMap<>();
                params.put("model", modelView);
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


}
