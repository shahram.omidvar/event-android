package ir.kooleh.app.Server.Api.Dashboard;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;
import ir.kooleh.app.Structure.Interface.Dashboard.InterfaceDashboard;
import ir.kooleh.app.Structure.Model.App.ModelPaginate;
import ir.kooleh.app.Structure.Model.Dashboard.ModelDashboard;
import ir.kooleh.app.Structure.Model.Dashboard.ModelMyAd;
import ir.kooleh.app.Structure.Model.Dashboard.ModelOffersMyAd;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiDashboard
{
    private final ControllerError controlEroAndExp;
    private final Gson gson;
    private final UserShared userShared;
    private static final String TAG = "ApiDashboard";

    public ApiDashboard(Context context)
    {
        controlEroAndExp = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }


    public void getProfile(InterfaceDashboard.Apis.IntGetProfile intGetProfile)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_PROFILE,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intGetProfile.onResponseGetProfile(
                                    true,
                                    gson.fromJson(
                                            jsonResponse.getJSONObject("result").toString(),
                                            ModelDashboard.class),
                                    jsonResponse.getString("message")
                            );
                        }
                        else
                        {
                            intGetProfile.onResponseGetProfile(
                                    true,
                                    null,
                                    jsonResponse.getString("message")
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intGetProfile.onResponseGetProfile(
                                false,
                                null,
                                controlEroAndExp.checkException(TAG, ex)
                        );
                    }

                },
                error ->
                        intGetProfile.onResponseGetProfile(
                                false,
                                null,
                                controlEroAndExp.checkVolleyError(error)
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


    public void getMyAds(InterfaceDashboard.Apis.IntGetMyAds intGetMyAds, int typeAdID, int stepAdID, int archive,
                         int nextPage, int itemsPerPage)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_MY_ADS,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intGetMyAds.onResponseGetMyAds(
                                    true,
                                    Arrays.asList(gson.fromJson(
                                            jsonResponse.getJSONObject("result").getJSONArray("data").toString(),
                                            ModelMyAd[].class)),
                                    gson.fromJson(jsonResponse.getJSONObject("result").getJSONObject("paginate").toString(),
                                            ModelPaginate.class),
                                    jsonResponse.getString("message")
                            );
                        }
                        else
                        {
                            intGetMyAds.onResponseGetMyAds(
                                    true,
                                    null,
                                    null,
                                    jsonResponse.getString("message")
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intGetMyAds.onResponseGetMyAds(
                                false,
                                null,
                                null,
                                controlEroAndExp.checkException(TAG, ex)
                        );
                    }

                },
                error ->
                        intGetMyAds.onResponseGetMyAds(
                                false,
                                null,
                                null,
                                controlEroAndExp.checkVolleyError(error)
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("step_ad_id", stepAdID + "");
                params.put("type_ad_id", typeAdID + "");
                params.put("archive", archive + "");
                params.put("page", nextPage + "");
                params.put("per_page", itemsPerPage + "");
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }




    public void getOffersMyAd(InterfaceDashboard.Apis.IntGetOffersMyAd intGetOffersMyAd, String adID)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_OFFERS_MY_AD,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intGetOffersMyAd.onResponseGetOffersMyAd(
                                    true,
                                    gson.fromJson(
                                            jsonResponse.getJSONObject("result").toString(),
                                            ModelOffersMyAd.class),
                                    jsonResponse.getString("message")
                            );
                        }
                        else
                        {
                            intGetOffersMyAd.onResponseGetOffersMyAd(
                                    true,
                                    null,
                                    jsonResponse.getString("message")
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intGetOffersMyAd.onResponseGetOffersMyAd(
                                false,
                                null,
                                controlEroAndExp.checkException(TAG, ex)
                        );
                    }

                },
                error ->
                        intGetOffersMyAd.onResponseGetOffersMyAd(
                                false,
                                null,
                                controlEroAndExp.checkVolleyError(error)
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("ad_id", adID);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }



    public void chooseWinner(InterfaceDashboard.Apis.IntChooseWinner intChooseWinner, String auctionID, String userWinnerID)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.WINNER_AUCTION,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intChooseWinner.onResponseChooseWinner(
                                    true,
                                    jsonResponse.getString("message")
                            );
                        }
                        else
                        {
                            intChooseWinner.onResponseChooseWinner(
                                    true,
                                    jsonResponse.getString("message")
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intChooseWinner.onResponseChooseWinner(
                                false,
                                controlEroAndExp.checkException(TAG, ex)
                        );
                    }

                },
                error ->
                        intChooseWinner.onResponseChooseWinner(
                                false,
                                controlEroAndExp.checkVolleyError(error)
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("auction_id", auctionID);
                params.put("user_winner_id", userWinnerID);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }



    public void getMyOffers(InterfaceDashboard.Apis.IntGetMyOffers intGetMyOffers, int archive, int stepAdID, int nextPage, int itemsPerPage)
    {
        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_MY_OFFERS,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {
                            intGetMyOffers.onResponseGetMyOffers(
                                    true,
                                    Arrays.asList(gson.fromJson(
                                            jsonResponse.getJSONObject("result").getJSONArray("data").toString(),
                                            ModelMyAd[].class)),
                                    gson.fromJson(jsonResponse.getJSONObject("result").getJSONObject("paginate").toString(),
                                            ModelPaginate.class),
                                    jsonResponse.getString("message")
                            );
                        }
                        else
                        {
                            intGetMyOffers.onResponseGetMyOffers(
                                    true,
                                    null,
                                    null,
                                    jsonResponse.getString("message")
                            );
                        }
                    }
                    catch (Exception ex)
                    {
                        intGetMyOffers.onResponseGetMyOffers(
                                false,
                                null,
                                null,
                                controlEroAndExp.checkException(TAG, ex)
                        );
                    }

                },
                error ->
                        intGetMyOffers.onResponseGetMyOffers(
                                false,
                                null,
                                null,
                                controlEroAndExp.checkVolleyError(error)
                        )
        )
        {
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<>();
                params.put("archive", archive + "");
                params.put("step_ad_id", stepAdID  + "");
                params.put("page", nextPage + "");
                params.put("per_page", itemsPerPage + "");
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }
}
