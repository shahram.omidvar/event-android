package ir.kooleh.app.Server.Api.MultiSelection;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ir.kooleh.app.Server.Base.UrlAPI;

import ir.kooleh.app.Structure.Interface.MultiSelection.InterfaceMultiSelection;

import ir.kooleh.app.Structure.Model.MultiSelection.ModelMultiSelection;
import ir.kooleh.app.Utility.Class.ControllerError;
import ir.kooleh.app.Utility.SharedPreferences.UserShared;
import ir.kooleh.app.Utility.Volley.Controller;

public class ApiMultiSelectionActivity
{
    private static final String TAG = "ApiMultiSelectionActivity";

    private final ControllerError controllerError;
    private Gson gson;
    private UserShared userShared;
    private Context context;

    public ApiMultiSelectionActivity(Context context)
    {
        this.context = context;
        controllerError = new ControllerError(context);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        userShared = new UserShared(context);
    }

    public void getListMultiSelection(final InterfaceMultiSelection.Server interfaceMultiSelectionServer, String parentId)
    {

        StringRequest request = new StringRequest(Request.Method.POST,
                UrlAPI.GET_LIST_CATEGORY_ADS,
                response ->
                {
                    try
                    {
                        JSONObject jsonResponse = new JSONObject(response);
                        if (jsonResponse.getBoolean("ok"))
                        {

                            List<ModelMultiSelection> listMultiSelection =
                                    Arrays.asList(gson.fromJson(
                                            jsonResponse.getJSONObject("result").getJSONArray("data").toString(),
                                            ModelMultiSelection[].class));

                            interfaceMultiSelectionServer.onResponseListMultiSelection(
                                    true,
                                    listMultiSelection,
                                    "",
                                    jsonResponse.getJSONObject("result").getString("back_image")
                            );


                        }
                        else
                        {
                            interfaceMultiSelectionServer.onResponseListMultiSelection(
                                    false,
                                    null,
                                    jsonResponse.getString("message"),
                                    ""
                            );

                        }


                    }
                    catch (Exception ex)
                    {
                        interfaceMultiSelectionServer.onResponseListMultiSelection
                                (
                                        false,
                                        null,
                                        controllerError.checkException(TAG, ex),
                                        ""
                                );

                    }

                }
                ,
                error -> interfaceMultiSelectionServer.onResponseListMultiSelection
                        (
                                false,
                                null,
                                controllerError.checkVolleyError(error),
                                ""
                        )
        )
        {

            public Map<String, String> getHeaders() throws AuthFailureError
            {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + userShared.getApiToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {

                Map<String, String> params = new HashMap<>();
                params.put("parent_id", parentId);
                return params;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Controller.getPermission().addToRequestQueue(request);
    }


}
